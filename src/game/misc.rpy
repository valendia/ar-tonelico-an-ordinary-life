# Miscelleneous declarations

init:
    # Misc widgets
    image bottom_engraving = "misc/screen_bottom_engraving.png"
    image chat_border = "misc/chat_border.png"

    # Positions for chatting (when there is more than one NPC onscreen)
    transform chatleft:
        xalign 0.0 yalign 0.5

    transform chatright:
        xalign 1.0 yalign 0.5

    transform chatoffscreenleft:
        xpos 0.0 xanchor 1.0 yalign 0.5

    transform chatoffscreenright:
        xpos 1.0 xanchor 0.0 yalign 0.5

    # Position for displaying interesting items (where hero head usually goes in AT)
    transform itempos:
        xpos 0.11 ypos 0.86 xanchor 0.5 yanchor 0.5

    # A portrait for clair
    image clair_portrait = "misc/clair_portrait.png"

    # Lyner's face
    image lyner_face = "misc/lyner_face.png"

    # Sleipnir's face
    image sleipnir_face = "misc/sleipnir_face.png"

    # Setup extra sound channels
    $ renpy.music.register_channel("sound2", "sfx", loop=False)
    $ renpy.music.register_channel("soundloop", "sfx", loop=True)
