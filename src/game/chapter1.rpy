
label chapter1:

    # Reset volume levels, in case the previous chapter left crazy levels
    call reset_volumes

    stop music fadeout 1
    scene black with dissolve

    centered "You never know what life will throw at you."
    centered "You never know what you'll do next."
    centered "Life just works that way."
    centered "You'll wonder when it will end."
    centered "You'll wonder if you never want it to end."
    centered "Sometimes, we find out what it is we cherish after we lose it...."

    # Play Misha's theme
    play music "music/126 Expressive Girl.ogg"

    scene bg home
    with fade
    call show_chat_border

    me "Wake up. Hey, it's time for school."
    $ mish_name = "Girl"
    mish "Mmm. Five more minutes."

    me "Misha, wake up!"
    n "I sigh. This is a daily routine between me and my childhood friend, Misha."
    $ mish_name = "Misha"

    me "Sheesh. Guess I have no choice."

    n "I turn around and walk to her dresser."
    show panties at itempos with dissolve
    n "I open it and take out a pair of panties."
    hide panties with dissolve

    me "You leave me no choice."
    n "I lift them up and act like I'm smelling them."

    $renpy.sound.set_volume(4.0)
    play sound "sounds/punch.ogg"
    with vpunch
    show pictureframe at itempos with dissolve
    n "{b}BANG!{/b}"
    me "Ow!"
    n "A picture frame hits my head."
    $renpy.sound.set_volume(1.0)

    show misha_ws angry behind bottom_engraving at truecenter
    hide pictureframe
    with dissolve
    mish "What do you think you're doing!"

    n "Misha is finally awake."

    me "It worked, didn't it?"
    show misha_ws upset at truecenter
    mish "Leave my panties alone!"
    me "How else would I get you to wake up?"
    show misha_ws surprised1 at truecenter
    mish "You could just wait five minutes, you know..."
    me "That never works."
    me "Anyway, can't you sleep like a normal girl and put on pajamas instead of just a white shirt?"
    show misha_ws sadsmile at truecenter
    mish "But it's so much comfier. What do you care?"
    me "I do care."
    n "I sigh."
    me "I have to come here every day and see you like this."
    show misha_ws sadblush at truecenter
    mish "{size=-6}I wish you could figure out why...{/size}"

    me "Man. Isn't it usually the other way around? The childhood female friend wakes up the guy?"
    show misha_ws happyblush at truecenter
    mish "{size=-6}But I like it when you pay attention to me every morning...{/size}"
    me "What was that?"
    show  misha_ws surprised2 at truecenter
    mish "Nothing, nothing! I'm gonna get changed now."

    me "Okay, I'll wait."
    show misha_ws thoughtful1 at truecenter
    mish "Hey."
    me "Yeah?"
    show misha_ws surprised2 at truecenter
    mish "I'm changing."
    me "I know."
    show misha_ws upset at truecenter
    mish "So..."
    me "So?"

    show misha_ws angry at truecenter
    mish "Get out!"

    $renpy.sound.set_volume(1.5)
    play sound "sounds/attack.ogg"
    with hpunch
    n "*Sound of items being thrown*"
    me "Okay, okay I'll go!"
    $renpy.sound.set_volume(1)
    play sound "sounds/running3.ogg"

    scene bg town
    with fade
    call show_chat_border

    # Play town music
    play music "music/118 Airport Paradise.ogg" fadeout 1

    n "I run out the room and out the house."
    n "I sigh."
    n "Good thing there's thirty minutes until the first class. Misha'll probably take a long time to get ready."
    n "I don't want to be late though... Incho Shurelia'll get mad again."

    play sound "sounds/stomache.ogg"
    n "My stomach growls."
    me "Man I'm hungry. What to do..."

    # Reset volume levels, to be polite to the next chapter
    call reset_volumes

    return
