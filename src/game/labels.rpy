# Contains character labels, used throughout the various chapters

init:

    # Narration
    $ n = Character(" ") # Naration, so that text stays inside the border

    # Main character
    $ me = Character(" ", what_prefix='"', what_suffix='"')

    # And a few styles for "me", so that the surrounding quotes get changed too
    $ me_muffled = Character(" ", what_prefix='{size=-6}"', what_suffix='"{/size}')
    $ me_soft = Character(" ", what_prefix='{size=-4}"', what_suffix='"{/size}')
    $ me_writing = Character(" ", what_prefix='{i}{u}"', what_suffix='"{/u}{/i}')

    # Other characters, in alphabetical order

    # Aurica
    $ au = Character("au_name", color= "#e0af81", who_outlines=[(1, "#73878f")], dynamic=True)

    # Bourd
    $ bo = Character("bo_name", color="#fecf96", who_outlines=[(1, "#8d5c3b")], dynamic=True)

    # Flute
    $ fl = Character("Flute", color= "#6772c1", who_outlines=[(1, "#8d6d78")])

    # Jack
    $ ja = Character("ja_name", color="#abb9c3", who_outlines=[(1, "#885839")], dynamic=True)

    # Krusche
    $ kr = Character("kr_name", color="#dd6d5c", who_outlines=[(1, "#53795c")], dynamic=True)

    # Leard
    $ le = Character("Leard", color= "#ece1da", who_outlines=[(1, "#a38d89")])

    # Meimei
    $ mei = Character("Meimei", color="#b63f36", who_outlines=[(1, "#6b4a34")])

    # Nir
    $ mir = Character("mir_name", color="#ecebe4", who_outlines=[(1, "#9c5466")], dynamic=True)

    # Misha
    $ mish = Character("mish_name", color= "#6fab7b", who_outlines=[(1, "#3860a9")], dynamic=True)

    # Radolf
    $ ra = Character("ra_name", color="#d08c63", who_outlines=[(1, "#906a54")], dynamic=True)

    # Shurelia
    $ sh = Character("sh_name", color="#e2ebf0", who_outlines=[(1, "#72a3d1")], dynamic=True)

    # Spica
    $ sp = Character("sp_name", color="#eae6eb", who_outlines=[(1, "#6193aa")], dynamic=True)
