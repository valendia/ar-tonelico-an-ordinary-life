label chapter3:

    # Reset volume levels, in case the previous chapter left crazy levels
    call reset_volumes

    # Play town music
    play music "music/118 Airport Paradise.ogg"

    # Some variables to keep track of which music channels are used for which
    # background effects
    $ wc = 6 # Wind channel
    $ sc = 5 # Steps channel

    # Start Song Stone Park wind
    $ renpy.music.set_volume(0.2, channel=wc)
    $ renpy.music.play("sounds/wind2.ogg", channel=wc, fadein=2)

    # Show the last background, from chapter 2
    scene bg sspark
    with fade
    call show_chat_border
    n "I look around for a shortcut to get to school."
    me "I can't miss another class, or I'll be expelled."
    me "Aaaargh! I'm so far away!"
    play sound "sounds/rattle.ogg"
    n "I bang at the fence with my arms."
    n "They start to hurt, but I don't care."
    n "I look forward."
    play music "music/112 Overcoming the Trials.ogg" fadeout 1
    me "Fine! I like a good challenge!"

    # Abuse a music channel to get sound queuing, because Ren'Py sound queues
    # are broken. Hopefully I can fix this soon with help from the Ren'Py
    # forums
    $ renpy.music.set_volume(0.5, channel=sc)
    $ renpy.music.play(["sounds/capeflap.ogg", "sounds/thud.ogg", "sounds/running.ogg"], channel=5, loop=False)
    n "I jump over the fence and run down the cliff."
    show bg sspark at Zoom((640, 480), (0, 0, 640, 480), (276, 412, 70, 50), 2.0)
    with Pause(2.0)

    # Stop the Song Stone Park wind
    stop music channel wc

    $ renpy.music.queue("sounds/running.ogg", channel=sc)
    scene bg forrest
    with dissolve
    call show_chat_border

    me "Let's make this fun!"
    play sound "sounds/motorcycle.ogg"

    "{size=-5}Guy with bike{/size}" "Hey kid! What do you think you're doing!?"
    n "Too late. I am already running, practically falling, down the cliff."
    "Guy" "Crazy kid..."
    stop sound

    $ renpy.sound.set_volume(0.5)
    play sound "sounds/folliage.ogg"
    n "I run as fast as I can, dodging trees, stray branches, and avoiding rocks."
    n "It's like playing a videogame, only I won't have a second chance."
    stop sound
    $ renpy.sound.set_volume(1)
    $ renpy.music.play("sounds/running2.ogg", channel=sc)
    n "I keep running down until I finally hit solid ground."
    $ renpy.music.set_volume(0.4, channel=wc)
    $ renpy.music.play("sounds/wind.ogg", channel=wc)
    n "The thrill of living my life on the edge, the feeling of constant wind in my face,"
    n "my adrenaline rush keeping me going. It's an awesome feeling!"
    n "I don't stop there."

    n "It's a good thing I was the best on the track team before I quit."
    n "I keep running and running,"
    n "afraid my bones will give in or the ground will catch on fire if I go any faster."
    n "Actually, that would be kinda cool."
    n "But I don't bother to check behind me; I might slow down or hit something."
    n "The wind in my eyes is more of a bother. I can barely keep my eyes open."
    play sound "sounds/bees.ogg"
    me "Blah!"
    n "A bug flies into my mouth. {nw}"
    extend "I spit it out to the side and {nw}"
    extend "keep running."

    show bg forrest at Zoom((640, 480), (0, 0, 640, 480), (300, 228, 103, 70), 1.0)
    with Pause(1.0)

    play sound "sounds/carbraking.ogg"
    scene bg street
    with dissolve
    call show_chat_border

    show bg street  at Zoom((640, 480), (0, 0, 640, 480), (153, 189, 288, 221), 0.3)
    with Pause(0.3)
    $ renpy.sound.set_volume(1.5)
    play sound "sounds/crash.ogg"
    show bg street at Zoom((640, 480), (153, 189, 288, 221), (0, 0, 640, 480), 0.3)
    with vpunch

    stop music
    stop music channel 5
    stop music channel 6

    n "{b}{size=+10}Crash!{/size}{/b}"
    $ renpy.music.set_volume(1, channel=5)
    $ renpy.music.play(["sounds/capeflap.ogg", "sounds/wait0.5.ogg", "sounds/thud2.ogg"], channel=5, loop=False)
    n "I fly across the air and land on the {nw}"
    extend "ground."
    show bg street
    with redflash
    n "The pain from the impact courses though my entire body."
    show bg street
    with blackflash
    n "I nearly black out, but I keep strong. I'm surprised I'm not in a hospital yet."
    me "Owowowowow. What'd I hit?"
    n "I hesitate to look up."

    play music "music/124 Wriggling Premonition.ogg"
    $ bo_name = "Man"

    show bourd frown behind bottom_engraving at truecenter
    with dissolve
    bo "Hey you! Look what you did to my car!"
    show bourd smile1 at truecenter with dissolve
    bo "You better hope you can pay for the damage."
    me "Sorry. I think I broke a rib. Think you can help me?"
    show bourd frown at truecenter with dissolve
    bo "Help you? I'm the one that needs help."
    bo "Look at the dent in my car!"
    n "I look at his car. The license plate catches my eye."
    me "Bourd?"
    $ bo_name = "Bourd"
    show bourd smile1 behind bottom_engraving at truecenter
    with dissolve

    bo "That's my name."
    bo "Now what are you going to do about this?"
    show bourd smile2 behind bottom_engraving at truecenter
    with dissolve
    bo "I know!"
    bo "How about you be my slave for the rest of your life?"
    me "Well I- uh..."
    n "I glance at his watch."
    n "There are three minutes left until the gates close."
    me "Crap! Gotta go, sorry!"
    $ renpy.music.set_volume(0.5, channel=sc)
    $ renpy.music.play("sounds/running2.ogg", channel=sc)
    hide bourd with moveoutright
    n "I get back on my feet and run off as fast as I can."
    n "I hear the guy yelling behind me."
    bo "Get back here!"

    play music "music/112 Overcoming the Trials.ogg" fadeout 1
    n "I swear, he's twice my size."

    n "I don't care. Just run, I keep telling myself."
    me "I'm gonna make it, I'm gonna make it, I'm gonna make it!"
    show bg street at Zoom((640, 480), (0, 0, 640, 480), (252, 257, 172, 117), 1)
    with Pause(1)

    scene bg outsideschool
    with dissolve
    call show_chat_border

    n "The school gates are finally in sight!"
    n "They're closing fast."
    show flute behind bottom_engraving at truecenter with dissolve

    n "I spot Flute at the scene."
    hide flute with dissolve
    me "Hey Flute! Hold that gate!"
    n "My adrenaline won't let me slow down."

    show flute behind bottom_engraving at truecenter
    with moveinleft
    fl "Hmm?"
    hide flute
    with moveoutright
    me "Come on, make it make it, make it!"
    n "The gate is just about closed."
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/gates.ogg"
    $ renpy.music.set_volume(1, channel=sc)
    $ renpy.music.play(["sounds/running3.ogg", "sounds/capeflap.ogg"], channel=5, loop=False)
    n "I take a dive and jump through the small opening in between the gates{nw}"
    extend "."
    me "Wragh!"

    $ renpy.music.set_volume(1, channel=sc)
    $ renpy.music.play(["sounds/thud.ogg","sounds/capeflap.ogg", "sounds/capeflap.ogg", "sounds/capeflap.ogg"], channel=sc, loop=False)

    n "I hit the ground and roll, and keep rolling until I crash into the building."

    $ renpy.sound.set_volume(1.5)
    play sound "sounds/thud2.ogg"
    show bg outsideschool
    with vpunch

    $ renpy.sound.set_volume(0.5)
    play sound "sounds/splintering.ogg"
    n "I think I hear another one of my ribs cracking."
    play music "music/107 Salute!.ogg" fadeout 1
    n "Despite that, I stand and strike a gymnastic victory pose."

    show flute behind bottom_engraving at truecenter
    with moveinright
    fl "That was quite the stunt you pulled."
    me "Thanks. What do I get?"
    play music "music/111 Tick-Tock Man.ogg" fadeout 1
    fl "Detention."
    me "Eeeeeeh!?"

    # Fade to black, to make transition of story time between scenes more
    # evident
    scene black with fade

    #---------------------
    $ renpy.music.set_volume(0.5, channel=6)
    play music "sounds/crowd.ogg" channel 6 fadein 1

    # Play a suitable-sounding bonus cosmosphere song for the classroom
    play music "music/stream.afs_00093.adx.ogg" fadeout 1
    scene bg classroom
    with fade
    call show_chat_border

    n "And so I enter my class."
    n "It's as noisy as ever."
    n "I assume the teacher is late for class... again."
    $ renpy.music.set_volume(1, channel=5)
    $ renpy.music.play(["sounds/woodsqueek.ogg", "sounds/wait0.5.ogg", "sounds/woodknock.ogg"], channel=5, loop=False)
    n "I take my seat and bang my head onto the desk, ready to sleep."
    n "My legs are so {nw}"
    extend "sore... I begin to wish I will never have to run ever again."
    n "Just as I'm about to doze off, I hear her voice."

    stop music fadeout 1 channel 6 # Make the kiddies shut up so we can hear Shurelia's theme better :-)
    play music "music/134 Silver.ogg" fadeout 1
    $ sh_name = "Girl"
    show shurelia_sg thoughtful2 behind bottom_engraving at shurcenter
    with dissolve
    sh "You're finally here."
    show shurelia_sg frown at shurcenter
    sh "What do you think you're doing, coming so late?"
    me "Save me the lecture. My legs feel like lead."
    me "You ever had the feeling, Shurelia?"
    $ sh_name = "Shurelia"
    show shurelia_sg thoughtful1 at shurcenter
    sh "No, I can't say I have."
    show shurelia_sg thoughtful2 at shurcenter
    sh "But that's no excuse for you to be late."
    show shurelia_sg thoughtful1 at shurcenter
    sh "You're just lucky that sensei's not here yet."
    me "I'm sorry already..."
    show shurelia_sg frown at shurcenter
    sh "You are a student of Ar Tonelico High School."
    show shurelia_sg thoughtful1 at shurcenter
    sh "And as such, you must honor your uniform by being on time and maintaining your grades."
    me "I'm sorry."
    show shurelia_sg shout at shurcenter
    sh "Sorry isn't good enough."
    show shurelia_sg thoughtful2 at shurcenter
    sh "As the president of the student council and class president, I cannot allow you to be late."
    show shurelia_sg thoughtful1 at shurcenter
    sh "I care about your future and I must-"

    play music "music/117 The Sunset Glow Is Bright Red.ogg" fadeout 1
    $ renpy.sound.set_volume(0.8)
    play sound "sounds/crash.ogg"
    show shurelia_sg shout at shurcenter
    show bg classroom with vpunch
    n "I bang my hands onto the desk and stand up to regard her evenly from eye level."

    # Show a CG for a closeup on Shurelia's face
    show bg cg_hold_shurelia_face
    hide shurelia_sg
    with dissolve

    n "I hold my face up to Shurelia's while caressing her chin with my hand and gazing deeply into her eyes."
    n "She blushes."
    me "Lady Shurelia. You must not hold an agitated look on your face."
    me "You are much more beautiful when you're smiling."
    sh "Wha-wha...."
    me "Please. Bring light to my dark day with your bright smile."
    n "I have a way with words, I admit."
    sh "I-I-I-..."

    n "I guess I picked up that skill when I was on the debate team before I quit."
    n "It didn't really bother me to talk to her like that. She really is cute."
    n "Or should that be beautiful? She's kind of a mix between the two."
    n "It's fun teasing that face."
    n "Oh, well."
    play sound "sounds/woodsqueek.ogg"

    # Dissolve shurelia back to her normal size
    show bg classroom
    show shurelia_sg thoughtful2 behind bottom_engraving:
        shurcenter
        zoom 1.0
    with dissolve

    n "I sit back down, seeing as how my legs still feel like lead."
    n "It makes sense. I did run a marathon's worth in 10 minutes, after all."

    show shurelia_sg thoughtful1 at shurcenter
    sh "J-just don't let it happen again, okay?"
    show shurelia_sg thoughtful2 at shurcenter
    sh "You're lucky sensei's late."
    hide shurelia_sg with dissolve
    n "She turns to look away. To hide her blushing, I presume."
    me "If only Flute was the one being late."
    show shurelia_sg thoughtful1 behind bottom_engraving at shurcenter
    with dissolve
    sh "Speaking of late, have you seen Jack?"

    me "No. I can't say I..."
    me "Wait..."
    me "I might ha-"
    stop music
    play sound "sounds/thud.ogg"
    show bg classroom with vpunch
    n "I feel a smack on my back."

    play music "music/122 Filthy Shop Work.ogg"
    show shurelia_sg thoughtful1 at shurright
    show jackh smile behind bottom_engraving at jackleft
    with moveinleft

    $ ja_name = "Guy"
    ja "That was quite the stunt you pulled there, kid!"
    play sound "sounds/splintering.ogg"
    n "If my cracked ribs weren't on the brink of breaking before, they certainly are now."
    me "I have a name, Jack..."
    n "Luckily, I don't get mad at my friends."
    n "Otherwise, I would have given him a one way ticket to the underworld."

    $ ja_name = "Jack"
    show jackh thoughtful at jackleft
    ja "Don't sweat the small stuff. I could've given you a ride on my bike if you asked."
    show jackh shock at jackleft
    ja "You didn't have to jump off that cliff."
    ja "All the way from Song Stone Park, too."
    show jackh smile at jackleft
    ja "I never knew it, but you're quite the athlete."
    show shurelia_sg shout at shurright
    sh "I-Is that true?"
    me "Yeah, that's right."
    me "So? How did you get past the gate?"
    show shurelia_sg smile1 at shurright
    ja "Me and Flute go way back."
    ja "I'm sorry to hear that you got detention though. Better luck next time."
    me "Save me the pity."
    show jackh frown at jackleft
    ja "Huh?"
    me "I said it's a big city."
    show jackh smile at jackleft
    ja "Uh... Yeah, it is."

    play music "music/138 Pulse ~Elemia Arrange~.ogg" fadeout 1
    show shurelia_sg thoughtful2 at shurright
    sh "Barsett-sensei is coming."
    show shurelia_sg shout at shurright
    sh "Return to your seat!"
    hide jackh
    with moveoutleft
    play sound "sounds/woodsqueek.ogg"
    n "Jack takes his seat to the left of mine."
    hide shurelia_sg
    with moveoutright
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/woodsqueek.ogg"
    n "Shurelia sits down about three seats from mine."
    n "I would have sat down anyway. I'm sore everywhere."
    $ renpy.sound.set_volume(1)
    n "I got hit by a frame,"
    n "other miscellaneous items from Misha's room,"
    n "a falling girl,"

    n "a car,"
    n "the ground,"
    n "the building,"
    n "and Jack's slap on my back to top it all off."
    n "I have a high threshold for pain."
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/door.ogg"

    show leardh smile at truecenter behind bottom_engraving
    with dissolve
    n "Leard Barsett-sensei enters the room."

    hide leardh
    show shurelia_sg shout at shurcenter behind bottom_engraving
    with dissolve

    sh "Stand!"
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/desks.ogg"
    show shurelia_sg smile1 at shurcenter
    n "We stand up."
    show shurelia_sg shout at shurcenter
    sh "Bow!"
    show shurelia_sg smile1 at shurcenter
    n "We bow."
    $ renpy.sound.set_volume(1.0)

    hide shurelia_sg
    show leardh smile at truecenter behind bottom_engraving
    with dissolve
    "Class" "Good morning, sensei."
    le "Good morning, class."

    n "The class says good morning."
    n "I say, \"I'm hungry. Feed me funbuns.\""
    n "I'm sure he couldn't have heard me."

    hide leardh
    show shurelia_sg shout at shurcenter behind bottom_engraving
    with dissolve
    sh "Sit."

    hide shurelia_sg
    show leardh smile at truecenter behind bottom_engraving
    with dissolve

    $ renpy.sound.set_volume(0.5)
    play sound "sounds/desks.ogg"
    $ renpy.music.set_volume(1.0, channel=6)
    $ renpy.music.play(["sounds/wait1.ogg", "sounds/woodsqueek.ogg"], channel=6, loop=False)

    n "We sit."
    n "I yawn and think about sleeping through the rest of the class."
    $ renpy.sound.set_volume(1.0)
    le "I apologize for being late."
    le "I had to guide our new student."

    $ renpy.sound.set_volume(0.3)
    play sound "sounds/crowd2.ogg"
    n "The class whispers about the new student."
    me "New student?"
    n "That catches my interest. I do enjoy new things happening."

    stop sound fadeout 2
    le "That's right, we have a new student with us today."

    hide leardh
    show jackh smile at truecenter behind bottom_engraving
    with dissolve
    ja "Hey, do you think it's a hot girl? I hope so."
    me "Knowing you, I wouldn't doubt it."

    hide jackh leardh
    show leardh smile at truecenter behind bottom_engraving
    with dissolve

    le "Come in, child."
    play music "music/110 Song For Counting the Memories.ogg" fadeout 1
    show leardh smile at leardleft

    # 'with moveinright' doesn't seem to work with dynamic displayables
    show aurica_sg scared at auricaright behind bottom_engraving
    with moveinright

    $ au_name = "Aurica"
    au "Thank you."
    n "My jaw nearly hits the ground as the girl walks in."
    n "I think Jack's does too, but I don't want to check."
    n "My eyes are fixed on the girl whose uniform isn't from this country."

    show aurica_sg smile2 at auricaright behind bottom_engraving
    au "Uhm... My name is Aurica Nestmile."
    show aurica_sg sadsmile at auricaright
    au "I hope I get along with everyone."
    $ renpy.music.set_volume(0.5, channel=6)
    $ renpy.music.set_volume(0.5, channel=5)
    $ renpy.music.play(["sounds/wait1.ogg", "sounds/applause.ogg"], channel=6, loop=False)
    $ renpy.music.play(["sounds/wait1.ogg", "sounds/crowd.ogg"], channel=5, loop=False)
    n "The class is silent for a moment, then erupts into applause."

    hide aurica_sg
    hide leardh
    show jackh smile at truecenter behind bottom_engraving
    with dissolve
    n "Jack forces my {nw}"
    $ renpy.sound.set_volume(1.0)
    play sound "sounds/click.ogg"
    extend "jaw shut."
    ja "Pull yourself together, kid."
    ja "You already have Misha, remember?"
    $ renpy.sound.set_volume(1.5)
    play sound "sounds/thud3.ogg"
    show jackh frown at truecenter
    n "I smack Jack on the back of his head without looking away from Aurica."

    hide jackh
    show leardh smile at leardleft behind bottom_engraving
    show aurica_sg thoughtful1 at auricaright behind bottom_engraving
    with dissolve
    le "Let's see. You can sit in..."

    n "I lift my text book to my face."
    n "I hope she doesn't see me."

    n "I don't know why. I just don't want to say hi to her right now!"
    n "Please don't sit anywhere near me!"
    n "My heart can't handle this!"
    n "Please please please plea-"

    le "Sit in row three, seat five."
    n "Row three, seat five!?"
    n "I look to my right. There's an empty seat."
    n "Crap... Crap... CRAP! I'm in row three seat six!"
    show leardh smile at offscreenleft
    show aurica_sg thoughtful1 at auricacenter
    with moveinright

    $ renpy.sound.set_volume(0.5)
    play sound "sounds/heartbeat.ogg"
    n "My heart pounds as she walks over."
    hide leardh
    n "All the guys in class are giving her ogling looks, and the girls are admiring her."
    n "She's gonna see me anyway, why am I hiding?"
    n "Maybe it's because of today's incident."
    n "Or maybe it's because I... I..."
    n "Argh! She's almost here!"
    n "What am I gonna do!? What!?"

    show aurica_sg laugh at auricacenter
    with dissolve
    au "Ah! It's you! Remember me?"
    me "Ahahah... Hah... Hi... Aurica."

    # Fade to black, to make transition of story time between scenes more
    # evident
    scene black with fade

    #---------------

    scene bg classroom
    with fade
    call show_chat_border

    show leardh smile at truecenter behind bottom_engraving
    with dissolve
    me "... Here."
    n "Leard sensei is calling roll."
    n "I always tried to figure out what the order is on the attendance sheet, but I never quite managed to."
    n "Eventually, I just gave up. It got boring."

    le "Jack Hamilton."

    hide leardh
    show aurica_sg smile1 at auricacenter behind bottom_engraving
    show notebook at itempos
    with dissolve

    n "Aurica nudges me."
    n "I look over and see that she has written something onto her notebook."
    n "I guess she wants to talk without getting in trouble."
    n "I go along with it."

    hide aurica_sg
    hide notebook
    show jackh smile at truecenter behind bottom_engraving
    with dissolve
    ja "Present."
    hide jackh
    show aurica_sg smile2 at auricacenter behind bottom_engraving
    show notebook at itempos
    with dissolve
    au "{i}{u}I didn't think we were going to be studying together.{/u}{/i}"
    n "I smirk and jot a response in my own notebook."
    me_writing "I'm just as surprised as you are."
    me_writing "Why didn't you tell me you were going to Ar Tonelico High?"

    hide aurica_sg
    hide notebook
    show leardh smile at truecenter behind bottom_engraving
    with dissolve

    le "Shurelia."
    hide leardh
    show shurelia_sg smile1 at shurcenter behind bottom_engraving
    with dissolve
    sh "Here."

    hide shurelia_sg
    show aurica_sg thoughtful2 at auricacenter behind bottom_engraving
    show notebook at itempos
    with dissolve
    au "{i}{u}You never asked.{/u}{/i}"
    me_writing "Darn it. I need more time to prepare myself for this."
    me_writing "You nearly gave me a heart attack!"

    hide aurica_sg
    hide notebook
    show leardh smile at truecenter behind bottom_engraving
    with dissolve
    le "Mir."

    hide leardh
    show aurica_sg thoughtful1 at auricacenter behind bottom_engraving
    show notebook at itempos
    with dissolve

    au "{i}{u}Sorry...{/u}{/i}"

    hide aurica_sg
    hide notebook
    show leardh frown at truecenter behind bottom_engraving
    with dissolve
    le "Mir?"

    hide leardh
    show aurica_sg thoughtful1 at auricacenter behind bottom_engraving
    show notebook at itempos
    with dissolve
    me_writing "No, don't apologize."
    me_writing "Still, what a coincidence. This doesn't happen every day."

    hide aurica_sg
    hide notebook
    hide leardh
    show jackh smile at truecenter behind bottom_engraving
    with dissolve
    ja "Sensei, she's absent again."
    ja "Maybe she's still sick."

    hide jackh
    show leardh frown at truecenter behind bottom_engraving
    with dissolve
    le "Again? That girl never comes to school."

    hide leardh
    show aurica_sg smile1 at auricacenter behind bottom_engraving
    with dissolve

    au "{i}{u}I told you so. Blackie's a lucky cat.{/u}{/i}"
    me_writing "Speaking of which, where is he-"
    stop music fadeout 1

    $ renpy.sound.set_volume(1)
    play sound "sounds/meow.ogg"
    show aurica_sg dismay at auricacenter
    n "Before I can finish writing my sentence, a loud meow echoes through the room."

    hide aurica_sg
    show leardh angry at truecenter behind bottom_engraving
    with dissolve
    le "What was that? Don't you kids know the rules!?"
    le "No pets in school!"
    $ renpy.sound.set_volume(1)
    queue music ["music/stream.afs_00100.adx.ogg"]
    n "I yawn as loudly as I can and try to make it sound close to meowing."
    me "Sorry, sensei. I didn't get much sleep last night."
    show leardh frown at truecenter
    le "Keep it down."
    le "Unlike you, I'm trying to make the other kids here have a good future."
    le "After all, the pen's-"
    me "-mightier than the sword."
    n "I mouth his last sentence as he says it."
    n "Too predictable."

    hide leardh
    show aurica_sg dismay at auricacenter behind bottom_engraving
    with dissolve

    play music "music/110 Song For Counting the Memories.ogg" fadeout 1
    au "{i}{u}I'm sorry!{/u}{/i}"
    me_writing "You brought it with you!?"
    show aurica_sg scared at auricacenter
    au "{i}{u}I couldn't just leave him outside.{/u}{/i}"
    show aurica_sg dismay at auricacenter
    au "{i}{u}He'd get lonely and hungry.{/u}{/i}"
    me_writing "Sheesh...."

    hide aurica_sg
    show shurelia_sg smile1 at shurcenter behind bottom_engraving
    with dissolve
    n "I can't tell who, but I can feel someone staring at my back."

    #-------------

    # Fade to black, to make transition of story time between scenes more
    # evident
    scene black with fade

    $ renpy.music.set_volume(0.5, channel=6)
    play music "sounds/crowd.ogg" channel 6 fadein 1
    play music "music/stream.afs_00093.adx.ogg" fadeout 1

    scene bg classroom
    with fade
    call show_chat_border

    n "Break time arrives, giving me a much needed rest from learning."
    me "Finally, I can sleep without being bothered by sensei..."
    show jack smile at truecenter behind bottom_engraving
    with dissolve
    ja "You sleep anyway."

    show jack smile at jackright
    show aurica_sgh smile1 at auricaleft behind bottom_engraving
    with moveinleft

    au "Thank you for covering for Blackie."
    show aurica_sgh sadsmile at auricaleft
    au "I'm sorry."
    me "It's no problem. I'm serio-"
    stop music fadeout 1
    n "I don't get to finish my sentence before..."

    hide jack
    show misha_sg happy at chatright behind bottom_engraving
    with dissolve

    play sound "sounds/door.ogg"
    $ mish_name = "Misha"
    mish "Out of my way!"
    play music "music/126 Expressive Girl.ogg"

    show aurica_sgh smile1 at auricaoffscreenleft
    show misha_sg happy at truecenter
    with moveoutright

    $ renpy.music.set_volume(0.5, channel=6)
    $ renpy.music.play(["sounds/running3.ogg", "sounds/capeflap.ogg", "sounds/wait0.5.ogg"], channel=6, loop=False)
    n "Misha bursts through the door and tackles me to the ground."
    play sound "sounds/thud.ogg"
    show bg classroom with vpunch

    play sound "sounds/splintering.ogg"
    show bg classroom with blackflash
    n "She hugs me tightly. Tightly enough that I begin to suffocate."
    $ renpy.sound.set_volume(0.2)
    play sound "sounds/cloth.ogg"
    n "She talks while moving up and down on my body,"
    play sound "sounds/cloth.ogg"
    n "her breasts rubbing against my face while her legs cling too high up to my right thigh."
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/heartbeat.ogg"
    n "It's impossible for a normal, healthy, young male to not get excited in this kind of situation."
    show misha_sg happyblush at truecenter
    mish "I missed you sooo much!"
    show misha_sg smile at truecenter
    mish "It's so hard being so far from you for so long!"
    me_muffled "You're only in the class next door!"
    n "My voice comes out sounding muffled."
    me_muffled "I can't breathe!"

    show misha_sg happyblush at chatright
    show jackh frown at jackleft behind bottom_engraving
    with moveinleft
    ja "Misha, get a hold of yourself."

    hide jackh
    show aurica_sgh scared at auricaleft
    with dissolve
    au "Uhm..."

    show misha_sg thoughtful1 at chatright
    mish "Huh? Is she new?"

    hide aurica_sgh
    show jackh smile at jackleft behind bottom_engraving
    with dissolve
    ja "She's Aurica. New student."
    ja "And very lovely addition to our wonderful, beautiful, asset-filled school."

    show misha_sg thoughtful2 at chatright
    mish "Aurica, huh?"

    hide jackh
    show aurica_sgh dismay at auricaleft behind bottom_engraving
    with dissolve
    au "Uhm. I don't think he can take any mo-"

    hide misha_sg
    show shurelia_sg smile1 at shurright behind bottom_engraving
    with dissolve
    sh "Aurica, come with me."

    show aurica_sgh scared at auricaleft
    au "Eeeh?"

    play sound "sounds/running3.ogg"
    show shurelia_sg thoughtful1 at shuroffscreenright
    hide aurica_sgh
    with moveoutleft
    n "Shurelia grabs her hand and drags her off, out of the classroom."

    show jackh smile at truecenter behind bottom_engraving with dissolve
    ja "That was weird."

    show jackh smile at jackleft
    show misha_sg thoughtful1 at chatright behind bottom_engraving
    with moveinright
    mish "Yeah. What do you think they're going to do?"
    show misha_sg surprised1 at chatright
    mish "What do you thin-huh?"
    scene black with fade
    centered "I lie, unconscious, between Misha's breasts. This is marshmallow hell."

    #----------------

    play music "music/135 A Secret Meeting at Midnight.ogg" fadeout 1
    scene bg schooltrack
    with fade
    call show_chat_border

    show shurelia_sgh smile1 at shurleft behind bottom_engraving
    show aurica_sg smile1 at auricaright behind bottom_engraving
    with dissolve

    n "Shurelia brings Aurica outside the building."
    show aurica_sg thoughtful2 at auricaright
    au "Shurelia? What's wrong?"
    show shurelia_sgh thoughtful1 at shurleft
    sh "I don't think you should get too close to him."
    show aurica_sg thoughtful1 at auricaright
    au "Him?"
    show shurelia_sgh thoughtful2 at shurleft
    sh "You know who I mean."
    show aurica_sg thoughtful2 at auricaright
    au "Why?"
    show shurelia_sgh shout at shurleft
    sh "Well, he's a complete idiot."
    show shurelia_sgh frown at shurleft
    sh "He's always skipping school, he's mean, sarcastic,"
    show shurelia_sgh thoughtful2 at shurleft
    sh "always joining clubs then quitting them, and he's a pain in the butt."
    show aurica_sg cross at auricaright
    au "You mean he hurts people?"
    show shurelia_sgh smile1 at shurleft
    sh "It's not like that. I guess he does have his strong points, but he's just..."
    show shurelia_sgh thoughtful2 at shurleft
    sh "Out of the ordinary."
    show shurelia_sgh frown at shurleft
    sh "And he's a pervert."
    show aurica_sg smile1 at auricaright
    au "He can't be that bad."
    show shurelia_sgh shout at shurleft
    sh "I just told you he's-"
    show aurica_sg smile2 at auricaright
    au "He can't be a bad person."
    show aurica_sg laugh at auricaright
    au "After all, if a kind person like you likes him so much, he can't be a bad person."

    $ renpy.sound.set_volume(0.8, channel=6)
    play sound "sounds/schoolbell.ogg" channel 6
    n "The bell rings."
    show aurica_sg shout at auricaright
    au "Ah. We should be getting back."
    show aurica_sg smile1 at auricaright
    au "I'll see you in class, Shurelia!"
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/running3.ogg"
    hide aurica_sg with moveoutright
    n "She runs off."
    stop sound fadeout 2

    show shurelia_sgh thoughtful1 at shurleft
    sh "Wha-... I... I like him?"
    show shurelia_sgh cryshout at shurleft
    sh "That can't be!"
    show shurelia_sgh thoughtful1 at shurleft
    sh "That's impossible."
    $ renpy.sound.set_volume(0.5)
    show shurelia_sgh cryshout at shurleft
    sh "Only a fool would like someone like him."
    play sound "sounds/heartbeat.ogg"
    hide shurelia_sgh with moveoutleft
    n "Shurelia's heart beats faster as she continues to consider those words on her way to class."

    #--------------

    # Fade to black, to make transition of story time between scenes more
    # evident
    scene black with fade

    play music "music/138 Pulse ~Elemia Arrange~.ogg" fadeout 1
    scene bg classroom
    with fade
    call show_chat_border

    show leardh smile at truecenter behind bottom_engraving
    with dissolve
    n "Class has started up again."
    n "Leard is saying something about how viruses can control machines."
    n "I don't care. I already learned about that some time ago."
    n "It's obvious if you think about it a little."

    hide leardh
    show aurica_sg smile1 at auricacenter behind bottom_engraving
    with dissolve

    n "I'm about to doze to sleep when Aurica pokes me awake."
    me "What?"
    play sound "sounds/papers.ogg"
    show aurica_sg smile2 at auricacenter behind bottom_engraving
    show notebook at itempos with dissolve
    n "Aurica shoves her notebook toward me."
    show aurica_sg sadsmile at auricacenter behind bottom_engraving
    au "{i}{u}During lunch, can you meet me at the-{/u}{/i}"

    show aurica_sg scared at auricaright
    show leardh frown at leardleft behind bottom_engraving
    with moveinleft

    play music "music/106 Fenrir.ogg"
    le "What do we have here?"
    show aurica_sg shout at auricaright
    play sound "sounds/papers.ogg"
    hide notebook with dissolve
    n "Sensei takes Aurica's notebook."
    le "No notes in class. You can pick this up after school."
    me "Hey! Sensei! Give that ba-"
    show leardh angry at leardleft
    le "{color=#f00}{b}I hope you don't have any problems with that.{/b}{/color}"
    $ renpy.music.play(["music/139 Try Again!.ogg"], loop=False)
    me "None...."

    #-------------

    # Fade to black, to make transition of story time between scenes more
    # evident
    scene black with fade

    play music "music/138 Pulse ~Elemia Arrange~.ogg" fadeout 1
    scene bg classroom
    with fade
    call show_chat_border

    $ renpy.music.set_volume(0.5, channel=6)
    play music "sounds/crowd.ogg" channel 6 fadein 1

    n "Lunch break finally arrives."
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/desks.ogg"
    stop music fadeout 6 channel 6
    n "The majority of the class greets it by racing out to order their bread."
    me "Hey Aurica, where did you want to meet again?"
    $ renpy.sound.set_volume(1)
    n "No response."
    n "I take a look around the class, but she's nowhere to be found."
    me "She already left..."
    me "Where could she have gone?"
    me "I can't leave her hanging like this..."
    n "I gather my thoughts for a moment, then sigh."
    me "Guess I'll check the-"

    # Reset volume levels, to be polite to the next chapter
    call reset_volumes

    return
