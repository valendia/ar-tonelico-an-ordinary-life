label chapter6:

    # Reset volume levels, in case the previous chapter left crazy levels
    call reset_volumes

    # And set the music level appropriately for this part of the game:
    $renpy.music.set_volume(1) # Loud and dramatic.

    # If not continued from the previous chapter, then startup music etc
    # that should be playing at the start of the scene:
    if not continued:
        play music "music/223 Sleipnir.ogg" fadein 0.5
        scene bg dark_room with fade
        call show_chat_border

        show krusche dismay behind bottom_engraving at Position(xalign=1.2, yalign=0.55)
        show abr normal behind bottom_engraving at Position(xalign=0.4, yalign=0.5)
        with dissolve

    me "Watch out!"

    $renpy.sound.set_volume(1)
    play sound "sounds/running3.ogg"

    show krusche dismay:
        xalign 1.2 yalign 0.55
        linear 0.5 zoom 1.5 xalign 0.5 yalign 0.55

    show abr normal:
        xalign 0.4 yalign 0.5
        linear 0.5 xalign 0.0 yalign 0.5 xanchor 1.0 zoom 1.5 # Move off side of screen

    n "I run for her as quickly as I can."

    $renpy.sound.set_volume(1.5)
    play sound "sounds/thud2.ogg"
    show krusche dismay:
        xalign 0.5 yalign 0.55
        linear 0.5 xalign 2.5

    show bg dark_room at Zoom((640, 480), (0, 0, 640, 480), (180, 330, 250, 145), 0.5)
    pause 0.5

    n "The A.B.R is mere inches from her face before I tackle her to the ground,"
    n "shifting my weight and positioning myself to break her fall."

    play sound "sounds/explosion3.ogg"
    show bg dark_room at Zoom((640, 480), (0, 0, 640, 480), (180, 330, 250, 145), 0) with vpunch
    n "We land on a pile of discarded pipes."

    $ kr_name = "Krusche"
    show krusche shout:
        xalign 0.5
    with dissolve

    kr "Ow. What do you think you're doing!? You're going to get yourself killed!"
    me "You have no right to talk! Look at yourself!"
    n "Krusche is covered in bruises and blood."
    kr "This is nothing. Anyway, don't interfere!"

    hide krusche
    hide abr
    show bg dark_room
    show krusche thoughtful1 behind bottom_engraving at Position(xalign=1.2, yalign=0.55)
    show abr normal behind bottom_engraving at Position(xalign=-0.2, yalign=0.5)
    with dissolve

    $renpy.sound.set_volume(1.5)
    show chainsaw at itempos with dissolve
    queue sound "sounds/chainsaw_start.ogg"
    queue sound "sounds/chainsaw_start.ogg"
    queue sound "sounds/chainsaw_start.ogg"

    n "She stands up and attempts to start her chainsaw."
    n "She revs it again and again, but it doesn't start."
    hide chainsaw with dissolve
    show krusche shout at Position(xalign=1.2, yalign=0.55)
    kr "Come on, come on... I didn't pay for those upgrades for nothing!"

    $renpy.sound.set_volume(2)
    play sound "sounds/robot_roar.ogg"
    show abr normal at Move((0.2, 0.5), (0.4, 0.5), 0.2, xanchor="center", yanchor="center", repeat=False)
    pause 0.2
    # Switch ABR out for version with mobile arms:
    hide abr
    show abr_arm2 behind bottom_engraving at Position(xalign=0.49, yalign=0.26)
    show abr noarms behind bottom_engraving at Position(xalign=0.2, yalign=0.5)
    show abr_arm1 behind bottom_engraving at Position(xalign=0.23, yalign=0.257)
    # Move the back arm towards Krusche
    show abr_arm2 at Move((0.335, 0.16), (0.4, 0.25), 0.1)

    n "The A.B.R swings its arm down at Krusche."
    n "She's too distracted to notice."
    me "Idiot!"
    show metalpipe at itempos with dissolve
    n "I grab the nearest thing I can use as a weapon — a long pipe."
    hide metalpipe with dissolve
    $ renpy.sound.set_volume(1.0)
    play  sound "sounds/running3.ogg"
    n "I run up to the A.B.R. and {nw}"
    $renpy.sound.set_volume(2)
    play sound "sounds/thud_metal.ogg"
    show metalpipe at itempos
    show slash2 behind bottom_engraving
    extend "block its arm."

    hide metalpipe
    hide slash2
    with dissolve

    show abr_arm2 at Move((0.4, 0.25), (0.335, 0.16), 0.1)
    me "Pay attention!"

    # Move the other arm towards Krusche
    $renpy.sound.set_volume(2)
    play sound "sounds/robot_beep.ogg"
    show abr_arm1 at Move((0.16, 0.158), (0.5, 0.4), 0.2)
    n "Another robotic arm nearly grazes my face as it passes by."
    show krusche dismay at Move((1.2, 0.55, 1.2, 0.55), (1.9, 0.8, 1.9, 0.8), 0.2)

    $renpy.sound.set_volume(2)
    play sound "sounds/thud2.ogg"
    n "It strikes Krusche directly in the stomach."
    kr "Oof!"
    $renpy.sound.set_volume(1)
    play sound "sounds/capeflap.ogg"
    queue sound "sounds/thud3.ogg"
    n "She hits the wall hard, and passes out."

    me "Hey!"
    $renpy.sound.set_volume(2)
    play sound "sounds/robot_beep2.ogg"
    show abr_arm1 at Move((0.5, 0.4), (0.4, 0.3), 0.5)

    n "The same arm that knocked Krusche out turns its aim at me."
    play sound "sounds/robot_beep.ogg"
    show abr_arm1 behind bottom_engraving:
        xalign 0.6
        yalign 0.5
        linear 0.1 zoom 2

    $renpy.sound.set_volume(3, channel="sound2")
    play sound2 "sounds/punch.ogg"

    show bg dark_room
    with hpunch
    with redflash
    n "Bam!"

    show abr_arm1 behind bottom_engraving:
        linear 0.1 zoom 1 xalign 0.23 yalign 0.257

    $renpy.sound.set_volume(1)
    play sound "sounds/capeflap.ogg"
    pause 1
    $renpy.sound.set_volume(1)
    play sound "sounds/thud6.ogg"
    show bg dark_room with hpunch

    play music "music/210 Vidohunir.ogg" fadeout 0.5
    n "The arm makes contact with my head and sends me crashing into another wall."

    me "Wha... What the hell...?"

    show metalpipe at itempos with dissolve
    n "I try to stand up straight and hold a sword stance."
    play sound "sounds/heartbeat.ogg"
    n "My head begins to pound hard."
    n "Then I remember. I never attended the kendo club."

    play sound "sounds/robot_beep.ogg"
    show abr_arm1 behind bottom_engraving:
      xalign 0.23 yalign 0.257
      linear 0.1 zoom 2 yalign 0.8 xalign 0.6

    $ renpy.sound.set_volume(3.0)
    play sound2 "sounds/punch.ogg"
    hide metalpipe
    show bg dark_room
    with vpunch
    with redflash

    n "Crack!"

    play sound2 "sounds/robot_beep2.ogg"
    show abr_arm1 behind bottom_engraving:
        linear 0.1 zoom 1 xalign 0.23 yalign 0.257

    $renpy.sound.set_volume(1, channel="sound2")
    play sound2 "sounds/capeflap.ogg"
    pause 0.5
    play sound2 "sounds/thud6.ogg"
    show bg dark_room with hpunch

    me "Uaah!"
    n "The arm flies into my chest and again knocks me into a wall."
    n "I can taste my blood as it begins to spray from my mouth."

    me "Is this how it ends...?"

    show bg dark_room
    with redflash
    n "My eyes flash red."

    show bg dark_room
    with blackflash
    with redflash
    n "Then black and red."

    # Swap out the multipart ABR for the single ABR
    hide abr_arm1
    hide abr_arm2

    # Move ABR towards main
    $renpy.sound.set_volume(1)
    play sound "sounds/robot_move.ogg"
    show abr normal at Move((0.385, 0.5), (0.5, 0.5), 2.0, xanchor="center", yanchor="center")
    with Pause(2.0)

    n "I can barely keep my eyes open as the A.B.R. slowly approaches me."
    $renpy.sound.set_volume(2, channel="sound2")
    play sound2 "sounds/electric_charge.ogg"
    show pink_orb_anim behind bottom_engraving:
        xalign 0.61 yalign 0.23
    with dissolve

    show black behind bottom_engraving with Dissolve(2.0)

    n "A small, bright light forms in the A.B.R.'s mouth, charging, as I black out."
    n "I no longer have any hope."

    play music "music/05 Memories of a Gentle Breeze.ogg" fadeout 0.5
    show lyner_face at itempos with dissolve
    "Voice" "{color=#fff4c2}Live!{/color}"

    me "Lyner?"

    n "Lyner's voice in the midst of the darkness."
    "Lyner" "{color=#fff4c2}Live! For the both of us!{/color}"
    hide lyner_face with dissolve

    stop music fadeout 0.5
    show black with whiteflash
    n "My dark world flashes a bright white."

    # Go even louder for this part:
    $renpy.music.set_volume(1.5)
    play music "music/C1L3_Catacombs_Fight.ogg"
    $renpy.sound.set_volume(2)
    play sound "sounds/click.ogg"
    n "Click."

    show black with redflash
    show black with redflash
    n "His eyes begin to flash red, over and over again."
    $renpy.sound.set_volume(1)
    play sound "sounds/heartbeat.ogg"
    show black with redflash
    show black with redflash
    n "His head pulsates in a violent melody."
    show black with redflash
    show black with redflash
    n "The stress is too much to bear, crying for release, until..."

    show black with redflash
    n "{color=#ff0000}{b}{size=+5}\"AAAAAAAAAAAAAAAAHH!\"{/size}{/b}{/color}"

    image red_haze = Solid((255, 0, 0, 80))
    show red_haze behind black

    hide black with dissolve
    n "{color=#e00000}He can see again.{/color}"
    n "{color=#e00000}Blood covers his eyes, his vision dyed crimson by his own injuries.{/color}"
    show metalpipe at itempos with dissolve
    n "{color=#e00000}Blood trickles from his hand as he grips the pipe with all of his strength.{/color}"
    hide metalpipe with dissolve

    $renpy.sound.set_volume(2)
    play sound "sounds/capeflap.ogg"
    n "{color=#e00000}He jumps at the A.B.R. and {nw}{/color}"
    $renpy.sound.set_volume(2)
    play sound "sounds/thud3.ogg"
    extend "{color=#e00000}lands on its head.{/color}"

    play sound "sounds/attack3.ogg"
    show slash2 behind bottom_engraving
    show metalpipe at itempos
    show bg dark_room with vpunch

    hide slash2
    hide metalpipe
    with dissolve

    n "{color=#e00000}He positions the pipe downward and stabs it right into the laser charger.{/color}"

    n "{color=#ff0000}{b}\"AAAAAAAH!\"{/b}{/color}"
    $renpy.sound.set_volume(2)
    play sound "sounds/robot_motor.ogg"
    "{color=#009000}A.B.R.{/color}" "{color=#009000}{b}BWEEEEEE!{/b}{/color}"

    play sound "sounds/electric_charge.ogg"
    n "{color=#e00000}The A.B.R. screeches as its metallic components fly through the air,{/color}"
    $renpy.sound.set_volume(1)
    play sound "sounds/electricity.ogg"
    n "{color=#e00000}their sparking wires scarring the bloody attacker's face as they shoot past.{/color}"

    $ renpy.sound.set_volume(2.0)
    play sound "sounds/explosion4.ogg"
    show abr nohead:
        xanchor 0.5 yanchor 0.5 xpos 0.5 ypos 0.638
    hide pink_orb_anim
    with whiteflash
    n "{color=#e00000}He jumps to the ground as the head blows up.{/color}"
    n "{color=#e00000}Two parts remain, but he doesn't care.{/color}"
    n "{color=#e00000}His adrenaline level is too high to allow him to care.{/color}"
    n "{color=#e00000}He sees only blood, and craves more blood.{/color}"
    n "{color=#e00000}A single thought whirls through his head.{/color}"

    n "Die. Die! DIE! {b}DIE! {color=#ff0000}DIE! {size=+6}DIE!{/size}{/color}{/b}"

    play sound "sounds/attack3.ogg"
    show slash2 behind bottom_engraving
    show metalpipe at itempos
    show bg dark_room with vpunch

    hide slash2
    hide metalpipe
    with dissolve

    n "{color=#e00000}He charges again, stabbing the metal pipe into the A.B.R's lower body.{/color}"

    play sound "sounds/explosion3.ogg"
    n "{color=#e00000}The sickening sound of breaking metal greets the pipe's entry.{/color}"
    play sound "sounds/attack3.ogg"
    show slash4 behind bottom_engraving
    show metalpipe at itempos
    show bg dark_room with vpunch

    hide slash4
    hide metalpipe
    with dissolve

    n "{color=#e00000}The crazed teenager then withdraws the pipe and reaches into the hole with an arm.{/color}"
    n "{color=#ff0000}\"WAAAAAAAAH!\"{/color}"
    n "{color=#e00000}He grabs hold a handful of wires.{/color}"
    $ renpy.sound.set_volume(2.0)
    $ renpy.sound.set_volume(1.0, channel="sound2")
    play sound "sounds/explosion3.ogg"
    play sound2 "sounds/electricity.ogg"
    show slash3 behind bottom_engraving with Pause(0.2)
    hide slash3 with dissolve
    n "{color=#e00000}With a shriek of consuming strength, he tears the wires free from their enclosure.{/color}"

    $ renpy.sound.set_volume(2.0)
    play sound "sounds/explosion4.ogg"
    hide abr nohead
    show abr_arms behind red_haze at Position(xalign=0.544, yalign=0.262)
    with whiteflash
    n "{color=#e00000}He jumps back as the lower component of the A.B.R. explodes behind him.{/color}"
    n "{color=#e00000}One left.{/color}"

    $ renpy.sound.set_volume(1.5)
    play sound "sounds/robot_move.ogg"
    show abr_arms behind bottom_engraving:
      xalign 0.544 yalign 0.262
      linear 1.0 zoom 1.5
    n "{color=#e00000}The midsection of the A.B.R flies at its target from behind.{/color}"
    show metalpipe at itempos with dissolve
    n "{color=#e00000}He picks up the pipe again, tainted crimson with his own blood.{/color}"
    hide metalpipe with dissolve
    n "{color=#e00000}Turning around at just the right moment, he nails the midsection straight on.{/color}"
    n "{color=#ff0000}\"AAAAAAAAAAAAAAAH!\"{/color}"

    $renpy.sound.set_volume(2)
    play sound "sounds/thud_metal.ogg"
    show slash2_transparent behind bottom_engraving
    show metalpipe at itempos

    show abr_arms behind bottom_engraving:
      xalign 0.544 yalign 0.262
      linear 0.2 zoom 0.5 yalign 0.8

    pause 0.2
    $renpy.sound.set_volume(1.0, channel="sound2")
    play sound2 "sounds/explosion2.ogg"
    hide slash2_transparent
    hide metalpipe
    with dissolve

    n "{color=#e00000}He swings the midsection, batting it into the wall.{/color}"
    n "{color=#e00000}It crashes loudly.{/color}"
    n "{color=#e00000}Relentless, he doesn't let up.{/color}"

    show abr_arms behind bottom_engraving:
      xalign 0.544 yalign 0.8
      linear 0.5 zoom 1.0
    $renpy.sound.set_volume(1)
    play sound "sounds/capeflap.ogg"
    pause 0.4
    $renpy.sound.set_volume(2, channel="sound2")

    play sound2 "sounds/attack.ogg"
    show slash2 behind bottom_engraving
    show metalpipe at itempos
    show bg dark_room with hpunch
    hide slash2
    hide metalpipe
    with dissolve

    n "{color=#e00000}The boy jumps up to it and begins beating at it mercilessly.{/color}"

    $renpy.sound.set_volume(2)
    play sound2 "sounds/attack.ogg"
    show slash4 behind bottom_engraving
    show metalpipe at itempos
    show bg dark_room with hpunch
    show slash2 behind bottom_engraving
    show bg dark_room with hpunch

    hide slash2
    hide slash4
    hide metalpipe
    with dissolve
    n "{color=#e00000}Again and again, he beats it.{/color}"

    play sound "sounds/attack.ogg"
    $renpy.sound.set_volume(1, channel="sound2")
    play sound2 "sounds/electricity.ogg"

    show slash4 behind bottom_engraving
    show metalpipe at itempos
    show bg dark_room with hpunch
    show slash2 behind bottom_engraving
    show bg dark_room with hpunch
    hide slash2
    hide slash4
    hide metalpipe
    with dissolve
    n "{color=#e00000}Screws, wires, bolts, and sparks. They fly everywhere.{/color}"

    play sound "sounds/attack.ogg"
    play sound2 "sounds/electricity.ogg"

    show slash4 behind bottom_engraving
    show metalpipe at itempos
    show bg dark_room with hpunch
    hide slash4
    hide metalpipe
    with dissolve
    n "{color=#e00000}The boy's rage is fueled by a storm of emotions and desires.{/color}"

    play sound "sounds/attack.ogg"
    play sound2 "sounds/electricity.ogg"

    show slash2 behind bottom_engraving
    show metalpipe at itempos
    show bg dark_room with hpunch
    hide slash2
    hide metalpipe
    with dissolve
    n "{color=#e00000}Fear. Anger. Blood lust.{/color}"

    play sound "sounds/attack.ogg"
    play sound2 "sounds/electricity.ogg"

    show slash2 behind bottom_engraving
    show metalpipe at itempos
    show bg dark_room with hpunch
    show slash4 behind bottom_engraving
    show bg dark_room with hpunch
    hide slash4
    show bg dark_room with hpunch

    hide slash2
    hide slash4
    hide metalpipe
    with dissolve
    n "{color=#e00000}But most importantly, survival.{/color}"

    show metalpipe at itempos
    play sound "sounds/thud_metal.ogg"
    show bg dark_room:
      xalign 0.5 yalign 0.5
      linear 0.2 zoom 2.0 xalign 0.5 yalign 0.5
    pause 0.2

    $renpy.sound.set_volume(2, channel="sound2")
    play sound2 "sounds/explosion4.ogg"

    show bg dark_room with hpunch

    hide abr_arms
    hide metalpipe
    with whiteflash

    n "{color=#e00000}He finishes off the central core by pushing a pipe against the last remaining part of the A.B.R.,{/color}"
    n "{color=#e00000}ramming it into the wall.{/color}"

    stop music fadeout 5.0
    n "{color=#e00000}He begins to breathe heavily.{/color}"
    n "{color=#ff0000}\"Haaaaa.... haaa... haaa..\"{/color}"
    n "{color=#e00000}His dripping sweat mixes with puddles of blood.{/color}"
    n "{color=#e00000}The teen rests his head on the wall next to the A.B.R. core stuck to the wall, impaled by his pipe.{/color}"

    # Dramatic scene is done, so change back to the regular music volume.
    $renpy.music.set_volume(MUSIC_VOL)
    play music "music/125 Dimly Lit.ogg"
    kr "Hey..."
    n "Krusche had long regained consciousness. She was scared. Cautious."
    kr "Are... you okay?"

    show bg dark_room:
      zoom 1.0
    show krusche thoughtful1 behind bottom_engraving at kruschecenter
    with dissolve

    n "{color=#e00000}The finisher turns his head her way.{/color}"
    n "{color=#e00000}Everything is red.{/color}"

    $renpy.sound.set_volume(0.4)
    play sound "sounds/cloth2.ogg"
    hide red_haze with dissolve
    n "{color=#e00000}He stands up straight and wipes the blood from his eyes with his arm.{/color}"
    me "Yeah... I'm alright."

    n "I look upon the destruction."
    n "The alley does not look anything like it did a few moments ago."
    n "I never want to experience that again."

    #-----------

    $renpy.sound.set_volume(0.4, channel="soundloop")
    play soundloop "sounds/footsteps3.ogg"
    scene black with fade
    scene bg street3 with dissolve
    call show_chat_border

    n "I walk home by myself."
    n "Krusche helped bandage me up before sprinting off when she heard police sirens approaching."
    n "She said she didn't want to pay for repairs to the alleyway."
    n "It seems that she works for an agency called Tenba."
    n "She's in charge of terminating rampant machines."
    n "Seems like this wasn't the first time this had happened."

    stop soundloop fadeout 1
    scene black with fade
    scene bg house_night with fade
    call show_chat_border

    n "I arrive at my house."
    n "I can't wait to take a shower and change out of these blood soaked clothes."

    $renpy.sound.set_volume(1)
    play sound "sounds/door2.ogg"
    n "I open the door."

    play music "music/124 Wriggling Premonition.ogg" fadeout 0.5
    me "Wait..."
    n "Didn't I lock up before I left?"

    $renpy.sound.set_volume(0.5)
    play sound "sounds/anime_sample.ogg"
    n "I ease myself into the house."
    n "The TV is on."
    show broom at itempos with dissolve
    n "I grab a broom and tiptoe in."

    scene bg living_room_lights with dissolve
    call show_chat_border
    $renpy.sound.set_volume(0.5)
    play sound "sounds/capeflap.ogg"
    show broom at itempos with dissolve
    n "I jump out from behind the couch and point the broom end at the trespasser."
    stop music fadeout 0.1
    me "Misha?"

    play music "music/232 Warmth.ogg"
    hide broom with dissolve
    n "Misha is sleeping on my couch."
    n "Such a cute little thing."
    show bento2 at itempos with dissolve
    n "I look around, and find food on my table."
    play sound "sounds/footsteps2.ogg"

    hide bento2
    show paper at itempos
    with dissolve

    n "I walk up to the table and see the note left there."

    $ mish_name = "Misha"
    mish "{i}{u}Sorry for being selfish today.{/u}{/i}"
    mish "{i}{u}I wanted to make it up to you, so I made this.{/u}{/i}"
    mish "{i}{u}I hope you're not mad at me...{/u}{/i}"
    mish "{i}{u}<3 Misha.{/u}{/i}"
    hide paper with dissolve

    n "I smirk."
    n "Typical Misha."
    n "Guess she got tired of waiting for me to come home."
    play sound "sounds/footsteps2.ogg"

    show bg misha_sleeping behind background_engraving with dissolve

    n "I walk over to her and put a blanket over her. She looks so cute."
    n "So peaceful. So... vulnerable."
    me "Misha..."

    n "To be honest, after what happened today, I'm in the need of comfort from anybody."
    n "Anything would do. I'm in a vulnerable state of mind."
    n "I ease my face toward Misha's. Her lips look soft."
    n "I am so close, I can almost feel her breath."

    mish "Welcome... back...."
    n "I stop. Misha was talking in her sleep. I can't do this."
    n "I've never kissed anyone before, and I'm sure Misha hasn't either,"
    n "for as long as I've known her."
    n "I just can't take advantage of anyone like that."
    n "I sigh and smile at Misha."
    me "She must have been thinking of saying that the entire time..."

    play sound "sounds/footsteps2.ogg"

    show bg living_room_lights behind background_engraving with dissolve

    n "I walk over to the front door to close it."
    stop sound
    n "Before closing it, I notice some scratch marks on the doorknob."
    me "Well, at least now I know how she got in. She always was a good lock picker."
    play sound "sounds/door2.ogg"
    n "I close the door."
    play sound "sounds/footsteps2.ogg"
    show bento2 at itempos with dissolve
    n "I walk over to the table and eat some of Misha's food."
    n "It's the greatest thing I've eaten all week."

    #-----

    $ renpy.music.play("music/222 Good night!.ogg", loop=False, fadeout=0.5)
    scene black with fade
    $renpy.pause(4)
    play music "music/126 Expressive Girl.ogg"
    call show_chat_border
    mish "Hey!"
    me "Mmmm."
    mish "Wake up!"
    me "I don't wanna."
    mish "Wake up please?"
    me "Five more minutes."
    mish "Booo... Heehee."
    $renpy.sound.set_volume(0.2)
    play sound "sounds/cloth.ogg"
    n "Misha crawls into bed with me and hugs me tightly under the sheets."
    me "Misha..."
    mish "Yes?"
    me "What are you doing!?"

    transform misha_cling:
        truecenter
        zoom 1.8 yanchor 0.5 ypos 0.7

    $renpy.sound.set_volume(0.5)
    play sound "sounds/capeflap.ogg"
    show bg bedroom behind bottom_engraving
    show misha_sg smile behind bottom_engraving at misha_cling
    with dissolve

    n "I jump out of the sheets and stand up."
    n "Misha is still clinging to me."

    show misha_sg sadsmile behind bottom_engraving at misha_cling with dissolve
    mish "I'm trying to wake you up. Now let's go to school."
    me "Misha."
    mish "Yes?"
    me "It's Saturday."
    show misha_sg surprised2 behind bottom_engraving at misha_cling with dissolve
    mish "...... Oh yeah."
    me "That's the wrong response!"

    $renpy.sound.set_volume(1.0)
    play sound "sounds/heartbeat.ogg"
    n "My head hurts like crazy."

    show red_haze behind bottom_engraving with dissolve
    hide red_haze with dissolve
    n "Every now and then, my vision flashes a slight red."
    n "Yesterday was a blur. I remember only bits and pieces of it."

    play sound "sounds/footsteps2.ogg"
    hide misha_sg with dissolve
    n "It hurts even more when I try to recall those events as I walk down the stairs."

    scene bg living_room_opendoor with dissolve
    call show_chat_border
    show misha_sg smile behind bottom_engraving at truecenter with dissolve

    n "We arrive downstairs and Misha offers to cook breakfast for me."
    n "I don't want to make her feel sad by turning her down, so I accept her offer."

    play sound "sounds/heartbeat.ogg"
    show red_haze behind bottom_engraving with dissolve
    hide red_haze with dissolve
    n "I sit at the table and wait, holding my throbbing head."
    show food at itempos with dissolve
    n "She finishes, and we both eat."
    n "Misha eats rather quietly, staring at me."
    hide food with dissolve
    n "Once we finish eating (it was delicious, by the way), Misha asks me a question."

    show misha_sg thoughtful2
    mish "Hey, what happened to your arm?"
    me "This? Nothing."
    me "I uh... I fell down the stairs and landed on a box of knives!"
    me "Yeah, that's it."
    n "Ugh, there's not very much blood going to my brain this morning."
    n "I was so sure that I covered myself with enough bandages, but the bleeding wouldn't stop."

    show misha_sg thoughtful1
    mish "Liar."
    n "She's sharp."
    mish "Well, even if you won't tell me what happened, I still care."
    me "Sorry..."
    show misha_sg smile with dissolve
    n "Misha places her hands on the table and closes her eyes."
    stop music fadeout 0.5
    n "She tilts her head back and begins singing."

    $ hymmnos = "Was ki ra hymme re enclone xava yor"
    $ english = "I will sing to you who are covered in wounds"

    $renpy.music.set_volume(1.5)
    play music "music/chronicle_key_edit.ogg"

    image misha_sprite_magic = Particles(AuSpriteMagicFactory("aurica/sprite_particle.png", numParticles=80, pos=(0.7, 0.15)))

    image misha_blue = im.Scale(im.MatrixColor(im.MatrixColor(ImageReference("misha_sg smile"), im.matrix.brightness(.9)), im.matrix.saturation(.5) * im.matrix.tint(.035, .894, 0.906)), 227+4, 317+4)
    show misha_blue behind misha_sg at truecenter
    show hymmnos_song at truecenter
    show misha_sprite behind bottom_engraving:
        xalign 0.8 yalign 0.05
    show misha_sprite_magic behind bottom_engraving
    with dissolve

    mish "{i}{font=fonts/hymmnos.ttf}%(hymmnos)s{/font}{/i}"

    hide hymmnos_song with dissolve
    $ hymmnos = "lusye wassa chs vonn beng iem"
    $ english = "Right now, before this light of praising\nturns into darkness"
    show hymmnos_song at truecenter with dissolve
    mish "{i}{font=fonts/hymmnos.ttf}%(hymmnos)s{/font}{/i}"

    hide hymmnos_song with dissolve
    $ hymmnos = "jyel lof wis dornpica dsier mea"
    $ english = "This lonely place shall be the\nseed of my desire"
    show hymmnos_song at truecenter with dissolve
    mish "{i}{font=fonts/hymmnos.ttf}%(hymmnos)s{/font}{/i}"

    hide hymmnos_song with dissolve
    $ hymmnos = "Wee yea ra fwal fane sos yorr cexm"
    $ english = "I will spread kindness so that you shall\ncome to me"
    show hymmnos_song at truecenter with dissolve
    mish "{i}{font=fonts/hymmnos.ttf}%(hymmnos)s{/font}{/i}"
    hide hymmnos_song with dissolve

    me "Hey, what're you doing?"
    n "As I say that, my wounds begin to feel a lot better."
    stop music fadeout 2

    hide misha_sprite
    hide misha_sprite_magic
    hide misha_blue
    with dissolve
    $renpy.pause(1.5)
    $renpy.music.set_volume(MUSIC_VOL)
    play music "music/117 The Sunset Glow Is Bright Red.ogg"
    me "Misha... You're a Reyvateil?"
    n "She stops singing once I'm all fixed up."

    $renpy.sound.set_volume(0.2)
    play sound "sounds/cloth2.ogg"
    n "I remove my bandages, hardly able to believe that all my injuries were gone."
    mish "Yeah. I had a dream last night."
    mish "It told me to sing for you. The dream even told me I was a Reyvateil. Cool, huh?"
    me "Yeah, that's awesome!"

    show misha_sg surprised2
    mish "Are you praising me?"
    n "Yeah! I should be honoring you."

    show misha_sg happy with dissolve
    mish "Yay!"
    $renpy.sound.set_volume(0.5)
    play sound "sounds/running3.ogg"
    hide misha_sg with dissolve
    show misha_sg happy behind bottom_engraving at misha_cling with dissolve

    n "She runs around the table and {nw}"
    play sound "sounds/capeflap.ogg"
    extend "jumps onto me for a hug."
    mish "You praised me... I'm so happy. I thought Aurica stole you..."
    me "Aurica? What about her?"

    show misha_sg sadsmile with dissolve
    mish "Nothing."
    show misha_sg happyblush with dissolve
    mish "Now, then. You have to pay me back."
    me "Alright. What do you want? Anything. Just ask."
    me "Money? To be your slave for a day? Cook for you? Just name it."
    show misha_sg smile with dissolve
    mish "Go on a date with me."
    me "... That's it?"
    mish "That's it."

    #-------

    $ renpy.music.play("music/222 Good night!.ogg", loop=False, fadeout=0.5)
    scene black with fade
    $renpy.pause(4)
    play music "music/118 Airport Paradise.ogg" fadeout 0.5
    scene bg town with dissolve
    call show_chat_border

    n "Sunday arrives."
    n "I'm wearing my most casual clothes."
    n "If I had dressed up too fancily, Misha would probably have thought I was trying too hard."

    $renpy.sound.set_volume(0.5)
    play sound "sounds/footsteps3.ogg"
    n "I make my way to the Nemo Shopping Center."

    scene black with fade
    scene bg mall with dissolve
    call show_chat_border

    show misha_map behind bottom_engraving:
        zoom 0.5
        xalign 0.53
        yalign 0.68

    with dissolve
    n "As I arrive, I see Misha in the distance, waving to me."

    play sound "sounds/running3.ogg"

    hide misha_map
    hide bg mall
    with dissolve

    show bg mall behind bottom_engraving
    show misha_st happy behind bottom_engraving at truecenter
    with dissolve

    n "I run up to her and stop."
    n "She's wearing her standard casual clothing."
    n "It's nice to see her in something other than a school uniform."
    me "You look nice."

    show misha_st happyblush with dissolve
    n "Misha blushes."
    mish "Thanks. You too."
    me "Really now?"

    show misha_st happy
    mish "Heehee."

    $renpy.sound.set_volume(0.5)
    play sound "sounds/capeflap.ogg"
    n "She steps back and spins around."
    mish "So, then, I'll leave it to you."
    me "What do you mean?"
    mish "Where do you want to go?"
    me "Hmm."

    # Reset volume levels, to be polite to the next chapter
    call reset_volumes

    return
