label chapter7:

    # Reset volume levels, in case the previous chapter left crazy levels
    call reset_volumes

    # Hax: Stop music:
    stop music

    # And set the music level appropriately for this part of the game:
    $renpy.music.set_volume(1) # Loud and dramatic.

    # If not continued from the previous chapter, then startup music etc
    # that should be playing at the start of the scene:
    if not continued:
        #play music "music/223 Sleipnir.ogg" fadein 0.5
        #scene bg dark_room with fade
        scene black with dissolve
        call show_chat_border

        #show krusche dismay behind bottom_engraving at Position(xalign=1.2, yalign=0.55)
        #show abr normal behind bottom_engraving at Position(xalign=0.4, yalign=0.5)
        #with dissolve

    #me "Watch out!"

    # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

    $ mish_name = "Misha"
#    jump dev

    n "[Chapter 7 (under development)]"

    play music "music/109 A Single Breath.ogg" fadeout 1

    mish "How's this?"

    n "We decided to go shopping."
    n "It was a big shopping center, so there were endless possibilities."
    n "I wouldn't want Misha to get bored with me. It'd ruin my pride."

    me "You look really pretty."

    n "Misha came out of the changing room in a China dress. She looks stunning."

    mish "Thank you."

    n "Misha blushes. She goes back into the changing room to try another piece."

    n "I stand outside and look around. I have an uneasy feeling."

    me "I keep getting the feeling that I'm forgetting something."
    me "Damn, how could I forget..."

    n "I stand there, trying to remember."
    n "I can't shake the feeling out of my head."

    mish "What are you thinking about?"

    n "Misha bends forward to meet my eyes that are looking toward the ground."
    n "She surprised me."

    me "Nothing. Are you finished looking?"

    mish "Yup."

    n "We go to the the cash register. I pay for her clothes."
    n "She just chose whatever I said looked good. I guess she wanted to impress me."

    n "I laugh to myself."

    me "If this was Lyner, he'd be oblivious the entire time."

    play music "music/118 Airport Paradise.ogg" fadeout 1

    n "We walk outside and check out some more stores."

    n "We take turns choosing which one to go to."
    n "A certain store catches my eye."
    n "I remember what it was that I forgot."

    me "Hey Misha, I'll go check out a store."
    me "Can you stay here for a while?"

    mish "Okay."
    mish "Actually, I think I'll go buy something to eat. I'm getting hungry."

    me "Alright. I'll meet you at the food court."

    mish "Okay."

    n "We part ways for a while."

    play music "music/109 A Single Breath.ogg" fadeout 1

    n "I go inside the store and shuffle around."

    me "What would be good..."

    n "I keep thinking to myself. I need to buy something especially special."

    me "Perfect!"

    n "I pick it out and go to the cashier."

    $ sp_name = "Cashier"
    sp "That will be 100,000,000 lea- ah! Hello again."

    me "Spica? What are you doing here?"

    $ sp_name = "Spica"
    sp "My convenience store closes early on Sundays."
    sp "I work here part time to help out family."
    sp "It's so nice to see you again. Yet, strange."
    sp "Let me guess, shopping with Misha?"

    me "Yeah. That obvious?"

    sp "If you were buying this for anyone else, you'd be in big trouble."
    sp "I don't know what I might do to you. <3."

    me "Hah... hah."

    n "I was getting kind of nervous."

    sp "After all, Misha is very precious to me."

    me "I know. By the way, did you happen to teach her how to pick locks?"

    sp "You found out?"

    me "She did a number on mine last night."

    sp "That's nice to know."
    sp "By the way, there's a special on kitty candy today. Only 200 leaf."
    sp "Would you like some?"

    me "I don't care about the price. I just want a bucket's worth."

    sp "I thought so. <3"

    n "She rings me up my usual 50%%-off discount. I got lucky today."
    n "I leave the store in happy spirits with my box in hand."
    n "I hear the customer behind me asking Spica what a kitty candy is."

    play music "music/118 Airport Paradise.ogg" fadeout 1

    n "I walk to the food court and finally spot Misha."
    n "It was kinda hard to see for a while, but I saw a big man harassing her."
    n "I speed up to meet them."

    play music "music/124 Wriggling Premonition.ogg" fadeout 1

    $ bo_name = "Man"
    bo "Misha, long time no see. Come with me, you."

    mish "No! I don't want to! Let go of me!"

    bo "Stop struggling, you tool."

    play music "music/stream.afs_00001.adx.ogg" fadeout 1

    n "I throw a ketchup bottle at his head."
    n "It hits and explodes on contact."

    me "Hey! Who are you calling a tool?"

    bo "You're going to pay for that!"
    bo "Wait a minute... It's you!"

    me "Ah! It's.... Who are you?"

    bo "Are you messing with me?"

    me "Hmmm..... Bork?"

    $ bo_name = "Bourd"
    bo "It's Bourd!"

    mish "I said let go!"

    n "Misha punches Bourd in the gut."
    n "Bourd lets go and Misha runs behind me."
    n "I hold my free arm out to shield her from Bourd."

    bo "Move it, kid. She belongs to me."

    me "Misha doesn't belong to anyone. Who are you anyway?"

    bo "Hahahaha. Why are you treating her like a human? She's a Reyvateil."
    bo "Nothing but a tool to be used for war. Don't you understand that?"

    me "Shut up! Who the hell do you think you are?"
    me "Misha is Misha, and she's a person!"
    me "If there's anyone who can't be considered a human, it's you, Bourt!"

    bo "I said move it, or I'll move you!"

    me "I'd like to see you try."

    n "I put the box on a nearby table and grab a nearby broom."
    n "Bourd and I are ready to fight for honor."

    stop music fadeout 1

    $ ra_name = "Guy"
    ra "Alright, alright. That's enough you two."

    bo "Who are you?"

    me "Radolf?"

    $ ra_name = "Radolf"
    play music "music/138 Pulse ~Elemia Arrange~.ogg" fadeout 1
    ra "This is a public area."
    ra "I suggest you two stop making a ruckus and go your separate ways right now."

    bo "Pfft. I don't have time for the both of you."
    bo "I'll be back to get you, Misha."

    n "Misha sticks her tongue out at Baurf as he leaves."
    n "I sigh."
    n "I don't have enough confidence in my swordsmanship skills yet,"
    n "since I don't have anyone to compare myself with."

    me "That was close. Thanks, Radolf."

    n "Radolf knocks on my head."

    ra "Don't be so quick to resort to violence. If at all possible, just talk your way out of the situation."

    mish "But he was just trying to protect me, Radolf."

    ra "Hahaha, I know. He hasn't changed a bit."
    ra "And neither have you, Misha. It's been a while."

    mish "Yeah, it has."

    me "Really though, your timing was perfect. What were you doing here?"

    ra "Just meeting some friends and taking care of some business."
    ra "Which reminds me, I need to go."
    ra "It's nice to see you two again. I'll see you later."

    mish "Bye. And thanks again."

    me "Yeah, I'll see you tomorrow at school."

    ra "I hope so."

    n "Radolf leaves."
    n "I pick up my box and walk away with Misha."

    mish "So what did you buy?"

    me "It's a secret."

    play music "music/104 Doubt.ogg" fadeout 1

    n "Meanwhile, Radolf walks into an office."
    n "The door has a sign, \"Foster help center.\""

    ra "Sorry I'm late. I had to take care of some business."

    $ au_name = "Aurica"
    au "It's no problem. We were just talking about little things."

    "Old man" "That's right. Just little things."

    ra "Thank you."

    au "I'm sorry I asked you to come with me."
    au "My foster parents wouldn't want to come with me."

    ra "It's alright. I'm happy I could help."
    ra "Are you feeling alright about this, Aurica?"

    au "I can't help it..."

    ra "...Okay let's get this over with."
    ra "Shall we get down to the paperwork, Falss?"

    "Falss" "Yes. Here is the paperwork for Aurica's transfer."

    play music "music/125 Dimly Lit.ogg" fadeout 1

    n "It was getting late."
    n "It's around nine o'clock as we head home."
    n "We arrive at a nearby park."

    mish "Thanks for coming with me today."

    me "No problem. Sorry it had to be with me, though."

    mish "No! I wanted to come because it was you."
    mish "I could have a day with you all to myself. Just you and me."

    me "I'm flattered."

    n "I look at her."
    n "Guess I will never get used to Misha saying that to me."
    n "We did grow up together after all."

    stop music fadeout 1
    n "I hear a voice in the background behind the clutter of crickets."

    play music "music/at3/15 Yudantaiteki.ogg"
    "Voice" "Oooh, where is it?"

    n "I walk over to the voice."
    n "I guess my curiosity always gets the better of me."
    n "I see it is a girl. It seems like she is looking for something."

    "Voice" "Not here..."

    mish "What's wrong?"

    "Girl" "Aaah! Don't scare me like that. That's mean."

    me "Sorry about that. We were just worried about you."
    me "Are you looking for something?"

    "Girl" "Yeah. I'm supposed to make a delivery, but I dropped it."
    "Girl" "Ooh, stupid stupid me, heehee."

    mish "How sad."

    me "Yeah."

    "Girl" "If I don't find it, I'll be in super big trouble."

    me "How about we help you find it?"

    "Girl" "Really, mister?"

    mish "That's a good idea."
    mish "After all, it's better with three people, right?"

    me "We'll find whatever it is you're looking for in a flash."

    "Girl" "Oh thank you, thank you, thank you."

    mish "My name's Misha. What's yours?"

    "Girl" "You can call me Lyra!"
    "Girl" "I'm a treasure hunter."
    "Girl" "In return for a Grathnode crystal, I'll get you an item from wherever you want!"

    mish "That sounds dangerous."

    me "Well, let's get to work."

    n "We start searching for the item. Turns out we need to find a rare trading card."
    n "It could be anywhere."
    n "We thought of a lot of possibilities while looking for it."
    n "It might have been blown away, so we look in the trees."
    n "Misha climbed a tree to see if it got stuck on a branch."
    n "She forgot she was afraid of heights."

    me "Just jump!"

    mish "I can't! It's too scary!"

    me "Don't worry! I'll catch you!"

    mish "I can't!"

    me "Trust me!"

    mish "Oooo..."

    n "Misha closes her eyes and jumps."
    n "She crash lands on me."

    n "She lies on top of me."
    n "By reflex I had caught her. My hands are crossed, holding her."

    me "Soft?"

    n "I find that my hands gripping on both her breasts."

    me "Aah! It was an accident!"

    n "I jump back."

    n "Misha sits there, blushing."

    me "That's the wrong reaction!"

    n "Honestly, I was expecting a slap to the face."

    n "The search continues."

    n "We think it might have flown into the fountain."
    n "So Misha and I both jump in and search around."
    n "The bottom is slippery. Misha slips and falls backwards."

    mish "Eek!"

    me "Misha!"

    n "I grab for her hand and loose my footing."
    n "We both fall into the fountain."

    n "Misha is on the ground and I am on my knees above her."
    n "We are both soaking wet. This is an awkward position."

    "Lyra" "Hey you two! Stop fooling around!"

    n "The search continues until night."

    play music "music/125 Dimly Lit.ogg" fadeout 1

    n "It's at around midnight when we begin to give up hope."

    mish "Sorry we couldn't find it, Lyra."

    "Lyra" "It's okay. I had a feeling it wasn't here anymore."

    n "I keep searching. It just has to be here somewhere."
    n "I don't want to have wasted all this time, just to end up empty-handed."

    "Lyra" "To repay you, how about I give you two some kitty candy?"

    n "Lyra grabs her bag and looks through it."

    mish "That's okay."

    stop music fadeout 1
    "Lyra" "Ah-..........."

    mish "What's wrong?"

    "Lyra" "Don't get mad... but I found the card."

    mish "What!?"

    play music "music/104 Doubt.ogg" fadeout 1

    n "The card was in her bag this entire time. I heard this and stand up slowly."
    n "Misha and Lyra look my way."

    "Lyra" "Uhm... I'm sorry! Please don't get mad!"

    mish "It was just a mista-"

    stop music fadeout 1
    me "Isn't that great?"

    "Lyra" "Eh?"

    play music "music/232 Warmth.ogg"
    me "You found the card. Looks like you won't get yelled at after all."

    "Lyra" "Ah... Thank you!"

    n "Lyra gave her thanks and appologies before leaving to go home."

    n "Misha spins my way."

    me "What?"

    mish "You might seem mean, but you're always so nice."

    me "So?"

    mish "Hehe. I just remembered... why I always feel right whenever I'm with you."

    me "That's not a good reason."

    mish "It is to me."

    n "A bell rings."
    n "I turn to the park clock and it's midnight."

    mish "It's Monday."

    me "Yeah, it is."

    n "I take out the box and hand it to Misha."

    me "Here."

    mish "What is it?"

    me "Just take it."

    n "Misha takes it and shakes it. It makes a fluffy sound."

    mish "Can I open it?"

    me "Yeah, that's why I gave it to you."

    n "Misha opens the box."

    n "White fluff sticks out from the box."
    n "As Misha unravels it, her eyes glitter from the dress's reflection."
    n "It's a beautiful white kimono."

    mish "I-I-It's a..."

    me "Happy birthday."

    n "Misha lowers the kimono."
    n "As she looks straight at me, I see her eyes tearing up"

    me "He-hey! Why are you crying?"

    mish "Waaaaaah..."

    n "Misha hugs me tightly."
    n "I can barely breathe as she continues to hug and cry on my shoulder."
    n "She continues to cry."

    me "Hey. Stop crying... You know I don't like it when women cry."

    n "She doesn't stop."
    n "I scratch my head and hug her back. She is the warmth in this cold night."

    play music "music/stream.afs_00093.adx.ogg" fadeout 1

    n "The school bell rings."

    n "I look around, but it seems like Aurica is absent."

    n "I asked Shurelia and Jack, but they didn't know where she is, either."

    n "Maybe she's sick."

    n "I guess Reyvateils can get sick, too."

    n "Break comes and I go to see Radolf."

    play music "music/138 Pulse ~Elemia Arrange~.ogg" fadeout 1

    ra "What's wrong?"

    me "Did Aurica come here today to do any council work?"

    ra "No. She's been... at home."

    me "Oh."

    n "I guess she really is sick."

    me "By the way, Radolf, you're captain of the Kendo club, right?"

    ra "Yeah, I am. Why do you ask?"

    me "I was wondering if I could join."

    ra "I don't know. Knowing you, you'd just quit once you got bored of it."

    me "I won't quit this time. I swear it."

    ra "Prove it."
    ra "Why do you want to join the Kendo club?"
    ra "Depending on your answer, I might not ever let you join."

    me "I... I want to be able to protect."

    ra "....Protect what?"

    me "I want to protect that what's important to me."

    ra "...Good answer. I'll give you the sign up sheets tomorrow."

    n "The bell rings and I go back to class."
    n "Lunch finally comes."

    n "Aurica's not here."

    n "Misha's cooking club meets during lunch. I wonder what I should do today."

    # Reset volume levels, to be polite to the next chapter
    call reset_volumes

    return
