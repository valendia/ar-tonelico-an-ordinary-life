# Declarations for background images
init:
    image bg bedroom = "backgrounds/bedroom.png"
    image bg classroom = "backgrounds/classroom.png"
    image bg classroom2 = "backgrounds/classroom2.png"
    image bg dark_room = "backgrounds/dark_room.png"
    image bg forrest = "backgrounds/forrest.png"
    image bg home = "backgrounds/home.png"
    image bg house_evening = "backgrounds/house_evening.png"
    image bg house_night = "backgrounds/house_night.png"
    image bg inn_room = "backgrounds/inn_room.png"
    image bg living_room_lights = "backgrounds/living_room_lights.png"
    image bg living_room_opendoor = "backgrounds/living_room_opendoor.png"
    image bg mall = "backgrounds/mall.png"
    image bg outsideschool = "backgrounds/outsideschool.png"
    image bg restaurant = "backgrounds/restaurant.png"
    image bg park = "backgrounds/park.png"
    image bg school_corridoor = "backgrounds/school_corridoor.png"
    image bg school_rooftop = "backgrounds/school_rooftop.png"
    image bg school_stairs = "backgrounds/school_stairs.png"
    image bg schooltrack = "backgrounds/schooltrack.png"
    image bg shed = "backgrounds/shed.png"
    image bg street = "backgrounds/street.png"
    image bg street2 = "backgrounds/street2.png"
    image bg street3 = "backgrounds/street3.png"
    image bg sun_and_clouds = "backgrounds/sun_and_clouds.png"
    image bg sspark = "backgrounds/sspark.png" # Song Stone Park
    image bg town = "backgrounds/town.png"

    # Modified backgrounds, used for special effects
    image school_corridoor_front = "backgrounds/school_corridoor_front.png"

    # CGs by Nironeru
    image bg cg_hold_shurelia_face = "backgrounds/cg/hold_shurelia_face.png"
    image bg cg_aurica_and_blackie = "backgrounds/cg/aurica_and_blackie.png"
    image bg misha_sleeping = 'backgrounds/cg/misha_sleeping.jpg'