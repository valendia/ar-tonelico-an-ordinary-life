# Declarations for item images
init:
    image bbqsoda = "items/bbqsoda.png" # Not actually AT BBQ soda, but it matches Dexa's description best
    image bento1 = "items/bento1.png"
    image bento2 = "items/bento2.png"
    image broom = "items/broom.png"
    image bucket = "items/bucket.png"
    image chainsaw = "items/chainsaw.png"
    image flags = "items/flags.png"
    image food = "items/food.png"
    image metalpipe = "items/metalpipe.png"
    image notebook = "items/notebook.png"
    image panties = "items/panties.png"
    image paper = "items/paper.png"
    image pictureframe = "items/pictureframe.png"
