# Declarations for character art, and things related to displaying it

# Characters are in alphabetical order

init:

    # Aurica: Schoolgirl outfit

    image aurica_sg smile1 = "aurica/schoolgirl/1.png"
    image aurica_sg scared = "aurica/schoolgirl/2.png"
    image aurica_sg haunted = "aurica/schoolgirl/3.png"
    image aurica_sg thoughtful1 = "aurica/schoolgirl/4.png"
    image aurica_sg shout = "aurica/schoolgirl/5.png"
    image aurica_sg notimpressed = "aurica/schoolgirl/6.png"
    image aurica_sg meansmile = "aurica/schoolgirl/7.png"
    image aurica_sg sadsmile = "aurica/schoolgirl/8.png"
    image aurica_sg smile2 = "aurica/schoolgirl/9.png"
    image aurica_sg cross = "aurica/schoolgirl/10.png"
    image aurica_sg laugh = "aurica/schoolgirl/11.png"
    image aurica_sg sad = "aurica/schoolgirl/12.png"
    image aurica_sg dismay = "aurica/schoolgirl/13.png"
    image aurica_sg thoughtful2 = "aurica/schoolgirl/14.png"
    # And some horizontally flipped pics
    image aurica_sgh smile1 = im.Flip("aurica/schoolgirl/1.png", horizontal=True)
    image aurica_sgh scared = im.Flip("aurica/schoolgirl/2.png", horizontal=True)
    image aurica_sgh haunted = im.Flip("aurica/schoolgirl/3.png", horizontal=True)
    image aurica_sgh thoughtful1 = im.Flip("aurica/schoolgirl/4.png", horizontal=True)
    image aurica_sgh shout = im.Flip("aurica/schoolgirl/5.png", horizontal=True)
    image aurica_sgh notimpressed = im.Flip("aurica/schoolgirl/6.png", horizontal=True)
    image aurica_sgh meansmile = im.Flip("aurica/schoolgirl/7.png", horizontal=True)
    image aurica_sgh sadsmile = im.Flip("aurica/schoolgirl/8.png", horizontal=True)
    image aurica_sgh smile2 = im.Flip("aurica/schoolgirl/9.png", horizontal=True)
    image aurica_sgh cross = im.Flip("aurica/schoolgirl/10.png", horizontal=True)
    image aurica_sgh laugh = im.Flip("aurica/schoolgirl/11.png", horizontal=True)
    image aurica_sgh sad = im.Flip("aurica/schoolgirl/12.png", horizontal=True)
    image aurica_sgh dismay = im.Flip("aurica/schoolgirl/13.png", horizontal=True)
    image aurica_sgh thoughtful2 = im.Flip("aurica/schoolgirl/14.png", horizontal=True)

    # Aurica's images are a bit short, so they float above the text area.
    # Note: Misha is 164 cm tall, and Aurica is 160 cm tall, so it's fine for
    #       Aurica's image to show a bit lower

    transform auricacenter:
        xalign 0.5 yalign 0.56

    transform auricaleft:
        xalign 0.0 yalign 0.56

    transform auricaright:
        xalign 1.0 yalign 0.56

    transform auricaoffscreenleft:
        xpos 0.0 xanchor 1.0 yalign 0.56

    transform auricaoffscreenright:
        xpos 1.0 xanchor 0.0 yalign 0.56

    # Bourd

    image bourd smile1 = "bourd/bourd_1.png"
    image bourd frown = "bourd/bourd_2.png"
    image bourd smile2 = "bourd/bourd_3.png"

    # Chester

    image chester neutral1 = "chester/chester_1.png"
    image chester frown = "chester/chester_2.png"
    image chester nervous1 = "chester/chester_3.png"
    image chester shout = "chester/chester_4.png"
    image chester neutral2 = "chester/chester_5.png"
    image chester nervous2 = "chester/chester_6.png"
    image chester smile = "chester/chester_7.png"
    image chester nervous3 = "chester/chester_8.png"
    # Horizontally-flipped versions:
    image chesterh neutral1 = im.Flip("chester/chester_1.png", horizontal=True)
    image chesterh frown = im.Flip("chester/chester_2.png", horizontal=True)
    image chesterh nervous1 = im.Flip("chester/chester_3.png", horizontal=True)
    image chesterh shout = im.Flip("chester/chester_4.png", horizontal=True)
    image chesterh neutral2 = im.Flip("chester/chester_5.png", horizontal=True)
    image chesterh nervous2 = im.Flip("chester/chester_6.png", horizontal=True)
    image chesterh smile = im.Flip("chester/chester_7.png", horizontal=True)
    image chesterh nervous3 = im.Flip("chester/chester_8.png", horizontal=True)

    # Flute

    image flute = "flute/flute.png"

    # Flute's wide image is wide
    transform fluteright:
        xalign 1.5 yalign 0.5

    # Jack

    image jack smile = "jack/jack_1.png"
    image jack frown = "jack/jack_2.png"
    image jack thoughtful = "jack/jack_3.png"
    image jack shock = "jack/jack_4.png"
    # And some horizontally flipped pics of Jack
    image jackh smile = im.Flip("jack/jack_1.png", horizontal=True)
    image jackh frown = im.Flip("jack/jack_2.png", horizontal=True)
    image jackh thoughtful = im.Flip("jack/jack_3.png", horizontal=True)
    image jackh shock = im.Flip("jack/jack_4.png", horizontal=True)

    # Jack's wide image is wide
    transform jackleft:
        xalign -0.4  yalign 0.5
    transform jackright:
        xalign 1.6, yalign 0.5

    # Krusche
    image krusche thoughtful1 = "krusche/krusche_1.png"
    image krusche shout = "krusche/krusche_2.png"
    image krusche smile = "krusche/krusche_3.png"
    image krusche dismay = "krusche/krusche_4.png"
    image krusche thoughtful2 = "krusche/krusche_5.png"
    image krusche thoughtful3 = "krusche/krusche_6.png"
    # Horizontally-flipped versions
    image kruscheh thoughtful1 = im.Flip("krusche/krusche_1.png", horizontal=True)
    image kruscheh shout = im.Flip("krusche/krusche_2.png", horizontal=True)
    image kruscheh smile = im.Flip("krusche/krusche_3.png", horizontal=True)
    image kruscheh dismay = im.Flip("krusche/krusche_4.png", horizontal=True)
    image kruscheh thoughtful2 = im.Flip("krusche/krusche_5.png", horizontal=True)
    image kruscheh thoughtful3 = im.Flip("krusche/krusche_6.png", horizontal=True)

    # Krusche's image is a bit short
    $ kruscheoffscreenleft = Position(xpos=0.0, xanchor=1.0, yalign=0.55)
    $ kruschecenter = Position(xalign=0.5, yalign=0.55)

    # Leard

    image leard smile = "leard/leard_1.png"
    image leard frown = "leard/leard_2.png"
    image leard angry = "leard/leard_3.png"
    # And some horizontally flipped pics
    image leardh smile = im.Flip("leard/leard_1.png", horizontal=True)
    image leardh frown = im.Flip("leard/leard_2.png", horizontal=True)
    image leardh angry = im.Flip("leard/leard_3.png", horizontal=True)

    # Leard's wide image is wide
    transform leardleft:
        xalign -0.4 yalign 0.5

    # Meimei

    # Do not want "hoooge" Meimei
    $ mei_scale=0.90
    $ mei_w = int(279 * mei_scale)
    $ mei_h = int(303 * mei_scale)

    image meimei tired_hand = im.Scale("meimei/meimei_1.png", mei_w, mei_h)
    image meimei smile_hand = im.Scale("meimei/meimei_2.png", mei_w, mei_h)
    image meimei yawn_hand = im.Scale("meimei/meimei_3.png", mei_w, mei_h)
    image meimei tired = im.Scale("meimei/meimei_4.png", mei_w, mei_h)
    image meimei smile = im.Scale("meimei/meimei_5.png", mei_w, mei_h)
    image meimei yawn = im.Scale("meimei/meimei_6.png", mei_w, mei_h)

    # Meimei's images aren't tall enough, so she floats slightly above the chat
    # box...
    $ mei_yalign = 0.6
    $ meileft = Position(xalign=0.0, yalign=mei_yalign)
    $ meiright = Position(xalign=1.0, yalign=mei_yalign)
    $ meicenter = Position(xalign=0.5, yalign=mei_yalign)
    $ meioffscreenleft = Position(xpos=0.0, xanchor=1.0, yalign=mei_yalign)
    $ meioffscreenright = Position(xpos=1.0, xanchor=0.0, yalign=mei_yalign)

    # Mir: Ar Tonelico 1

    image mir_at1 angry = "mir/at1/mir_1.png"
    image mir_at1 sad = "mir/at1/mir_2.png"
    image mir_at1 tears = "mir/at1/mir_3.png"
    image mir_at1 smile = "mir/at1/mir_4.png"

    # Mir's at1 images have the same problem as Meimei
    $ mir_at1_yalign = 0.58
    $ mir_at1_left = Position(xalign=0.0, yalign=mir_at1_yalign)
    $ mir_at1_right = Position(xalign=1.0, yalign=mir_at1_yalign)
    $ mir_at1_center = Position(xalign=0.5, yalign=mir_at1_yalign)
    $ mir_at1_offscreenleft = Position(xpos=0.0, xanchor=1.0, yalign=mir_at1_yalign)
    $ mir_at1_offscreenright = Position(xpos=1.0, xanchor=0.0, yalign=mir_at1_yalign)

    # Mir: Ar Tonelico 2: Schoolgirl outfit

    # Large AT2 Mir is large
    $ scale = 0.83 # This makes her the same height as AT1 Aurica
    $ w = int(292 * scale)
    $ h = int(380 * scale)

    image mir_sg neutral = im.Scale("mir/schoolgirl/mir_1.png", w, h)
    image mir_sg faintsmile = im.Scale("mir/schoolgirl/mir_2.png", w, h)
    image mir_sg frown = im.Scale("mir/schoolgirl/mir_3.png", w, h)
    image mir_sg shock = im.Scale("mir/schoolgirl/mir_4.png", w, h)
    image mir_sg surprisedshout = im.Scale("mir/schoolgirl/mir_5.png", w, h)
    image mir_sg angryshout = im.Scale("mir/schoolgirl/mir_6.png", w, h)
    image mir_sg embarassedshout = im.Scale("mir/schoolgirl/mir_7.png", w, h)
    image mir_sg embarassedsweat = im.Scale("mir/schoolgirl/mir_8.png", w, h)
    image mir_sg sadsweat = im.Scale("mir/schoolgirl/mir_9.png", w, h)
    image mir_sg happyblush1 = im.Scale("mir/schoolgirl/mir_10.png", w, h)
    image mir_sg happyblush2 = im.Scale("mir/schoolgirl/mir_11.png", w, h)
    image mir_sg smile1 = im.Scale("mir/schoolgirl/mir_12.png", w, h)
    image mir_sg smile2 = im.Scale("mir/schoolgirl/mir_13.png", w, h)
    image mir_sg laugh = im.Scale("mir/schoolgirl/mir_14.png", w, h)

    # Misha: Schoolgirl outfit

    image misha_sg smile = "misha/schoolgirl/misha_1.png"
    image misha_sg thoughtful1 = "misha/schoolgirl/misha_2.png"
    image misha_sg surprised1 = "misha/schoolgirl/misha_3.png"
    image misha_sg happy = "misha/schoolgirl/misha_4.png"
    image misha_sg thoughtful2 = "misha/schoolgirl/misha_5.png"
    image misha_sg badmemories = "misha/schoolgirl/misha_6.png"
    image misha_sg surprised2 = "misha/schoolgirl/misha_7.png"
    image misha_sg happyblush = "misha/schoolgirl/misha_8.png"
    image misha_sg angry = "misha/schoolgirl/misha_9.png"
    image misha_sg tears = "misha/schoolgirl/misha_10.png"
    image misha_sg sadblush = "misha/schoolgirl/misha_11.png"
    image misha_sg sadsmile = "misha/schoolgirl/misha_12.png"
    image misha_sg upset = "misha/schoolgirl/misha_13.png"
    # And, horizontally-flipped pictures:
    image misha_sgh smile = im.Flip("misha/schoolgirl/misha_1.png", horizontal=True)
    image misha_sgh thoughtful1 = im.Flip("misha/schoolgirl/misha_2.png", horizontal=True)
    image misha_sgh surprised1 = im.Flip("misha/schoolgirl/misha_3.png", horizontal=True)
    image misha_sgh happy = im.Flip("misha/schoolgirl/misha_4.png", horizontal=True)
    image misha_sgh thoughtful2 = im.Flip("misha/schoolgirl/misha_5.png", horizontal=True)
    image misha_sgh badmemories = im.Flip("misha/schoolgirl/misha_6.png", horizontal=True)
    image misha_sgh surprised2 = im.Flip("misha/schoolgirl/misha_7.png", horizontal=True)
    image misha_sgh happyblush = im.Flip("misha/schoolgirl/misha_8.png", horizontal=True)
    image misha_sgh angry = im.Flip("misha/schoolgirl/misha_9.png", horizontal=True)
    image misha_sgh tears = im.Flip("misha/schoolgirl/misha_10.png", horizontal=True)
    image misha_sgh sadblush = im.Flip("misha/schoolgirl/misha_11.png", horizontal=True)
    image misha_sgh sadsmile = im.Flip("misha/schoolgirl/misha_12.png", horizontal=True)
    image misha_sgh upset = im.Flip("misha/schoolgirl/misha_13.png", horizontal=True)

    # Misha: White shirt outfit

    image misha_ws smile = "misha/white_shirt/white_shirt_1.png"
    image misha_ws thoughtful1 = "misha/white_shirt/white_shirt_2.png"
    image misha_ws surprised1 = "misha/white_shirt/white_shirt_3.png"
    image misha_ws happy = "misha/white_shirt/white_shirt_4.png"
    image misha_ws thoughtful2 = "misha/white_shirt/white_shirt_5.png"
    image misha_ws badmemories = "misha/white_shirt/white_shirt_6.png"
    image misha_ws surprised2 = "misha/white_shirt/white_shirt_7.png"
    image misha_ws happyblush = "misha/white_shirt/white_shirt_8.png"
    image misha_ws angry = "misha/white_shirt/white_shirt_9.png"
    image misha_ws tears = "misha/white_shirt/white_shirt_10.png"
    image misha_ws sadblush = "misha/white_shirt/white_shirt_11.png"
    image misha_ws sadsmile = "misha/white_shirt/white_shirt_12.png"
    image misha_ws upset = "misha/white_shirt/white_shirt_13.png"
    image misha_ws thoughtful3 = "misha/white_shirt/white_shirt_14.png"

    # Misha: Standard outfit

    image misha_st smile = "misha/standard/misha_1.png"
    image misha_st thoughtful1 = "misha/standard/misha_2.png"
    image misha_st surprised1 = "misha/standard/misha_3.png"
    image misha_st happy = "misha/standard/misha_4.png"
    image misha_st thoughtful2 = "misha/standard/misha_5.png"
    image misha_st badmemories = "misha/standard/misha_6.png"
    image misha_st surprised2 = "misha/standard/misha_7.png"
    image misha_st happyblush = "misha/standard/misha_8.png"
    image misha_st angry = "misha/standard/misha_9.png"
    image misha_st tears = "misha/standard/misha_10.png"
    image misha_st sadblush = "misha/standard/misha_11.png"
    image misha_st sadsmile = "misha/standard/misha_12.png"
    image misha_st upset = "misha/standard/misha_13.png"
    # And, horizontally-flipped pictures:
    image misha_sth smile = im.Flip("misha/standard/misha_1.png", horizontal=True)
    image misha_sth thoughtful1 = im.Flip("misha/standard/misha_2.png", horizontal=True)
    image misha_sth surprised1 = im.Flip("misha/standard/misha_3.png", horizontal=True)
    image misha_sth happy = im.Flip("misha/standard/misha_4.png", horizontal=True)
    image misha_sth thoughtful2 = im.Flip("misha/standard/misha_5.png", horizontal=True)
    image misha_sth badmemories = im.Flip("misha/standard/misha_6.png", horizontal=True)
    image misha_sth surprised2 = im.Flip("misha/standard/misha_7.png", horizontal=True)
    image misha_sth happyblush = im.Flip("misha/standard/misha_8.png", horizontal=True)
    image misha_sth angry = im.Flip("misha/standard/misha_9.png", horizontal=True)
    image misha_sth tears = im.Flip("misha/standard/misha_10.png", horizontal=True)
    image misha_sth sadblush = im.Flip("misha/standard/misha_11.png", horizontal=True)
    image misha_sth sadsmile = im.Flip("misha/standard/misha_12.png", horizontal=True)
    image misha_sth upset = im.Flip("misha/standard/misha_13.png", horizontal=True)

    # Misha's healing sprite
    image misha_sprite = "misha/cinna.png"

    # Map sprite of misha facing forwards
    image misha_map = "misha/misha_map.png"

    # Radolf

    image radolf smile = "radolf/radolf_1.png"
    image radolf dismay = "radolf/radolf_2.png"
    image radolf shout = "radolf/radolf_3.png"
    image radolf frown = "radolf/radolf_4.png"
    image radolf thoughtful1 = "radolf/radolf_5.png"
    image radolf laugh = "radolf/radolf_6.png"
    image radolf thoughtful2 = "radolf/radolf_7.png"
    # Horizontally-flipped:
    image radolfh smile = im.Flip("radolf/radolf_1.png", horizontal=True)
    image radolfh dismay = im.Flip("radolf/radolf_2.png", horizontal=True)
    image radolfh shout = im.Flip("radolf/radolf_3.png", horizontal=True)
    image radolfh frown = im.Flip("radolf/radolf_4.png", horizontal=True)
    image radolfh thoughtful1 = im.Flip("radolf/radolf_5.png", horizontal=True)
    image radolfh laugh = im.Flip("radolf/radolf_6.png", horizontal=True)
    image radolfh thoughtful2 = im.Flip("radolf/radolf_7.png", horizontal=True)

    # Radolf's wide image is wide
    $ radolfleft = Position(xalign=-0.5, yalign=0.5)

    # Shurelia

    # Small Shurelia is small
    $ sh_scale=0.85
    $ sh_w = int(246 * sh_scale)
    $ sh_h = int(327 * sh_scale)

    # Shurelia: Schoolgirl outfit

    image shurelia_sg smile1 = im.Scale("shurelia/schoolgirl/shurelia_1.png", sh_w, sh_h)
    image shurelia_sg frown = im.Scale("shurelia/schoolgirl/shurelia_2.png", sh_w, sh_h)
    image shurelia_sg smile2  = im.Scale("shurelia/schoolgirl/shurelia_3.png", sh_w, sh_h)
    image shurelia_sg thoughtful1  = im.Scale("shurelia/schoolgirl/shurelia_4.png", sh_w, sh_h)
    image shurelia_sg tears1 = im.Scale("shurelia/schoolgirl/shurelia_5.png", sh_w, sh_h)
    image shurelia_sg cryshout = im.Scale("shurelia/schoolgirl/shurelia_6.png", sh_w, sh_h)
    image shurelia_sg thoughtful2 = im.Scale("shurelia/schoolgirl/shurelia_7.png", sh_w, sh_h)
    image shurelia_sg shout = im.Scale("shurelia/schoolgirl/shurelia_8.png", sh_w, sh_h)
    image shurelia_sg tears2 = im.Scale("shurelia/schoolgirl/shurelia_9.png", sh_w, sh_h)
    # Horizontally-flipped images
    image shurelia_sgh smile1 = im.Scale(im.Flip("shurelia/schoolgirl/shurelia_1.png", horizontal=True), sh_w, sh_h)
    image shurelia_sgh frown = im.Scale(im.Flip("shurelia/schoolgirl/shurelia_2.png", horizontal=True), sh_w, sh_h)
    image shurelia_sgh smile2  = im.Scale(im.Flip("shurelia/schoolgirl/shurelia_3.png", horizontal=True), sh_w, sh_h)
    image shurelia_sgh thoughtful1  = im.Scale(im.Flip("shurelia/schoolgirl/shurelia_4.png", horizontal=True), sh_w, sh_h)
    image shurelia_sgh tears1 = im.Scale(im.Flip("shurelia/schoolgirl/shurelia_5.png", horizontal=True), sh_w, sh_h)
    image shurelia_sgh cryshout = im.Scale(im.Flip("shurelia/schoolgirl/shurelia_6.png", horizontal=True), sh_w, sh_h)
    image shurelia_sgh thoughtful2 = im.Scale(im.Flip("shurelia/schoolgirl/shurelia_7.png", horizontal=True), sh_w, sh_h)
    image shurelia_sgh shout = im.Scale(im.Flip("shurelia/schoolgirl/shurelia_8.png", horizontal=True), sh_w, sh_h)
    image shurelia_sgh tears2 = im.Scale(im.Flip("shurelia/schoolgirl/shurelia_9.png", horizontal=True), sh_w, sh_h)

    # Small Shurelia is small
    $ sh_yalign = 0.6
    transform shurleft:
        xalign 0.0 yalign sh_yalign
    transform shurright:
        xalign 1.0 yalign sh_yalign
    transform shurcenter:
        xalign 0.5 yalign sh_yalign
    transform shuroffscreenleft:
        xpos 0.0 xanchor 1.0 yalign sh_yalign
    transform shuroffscreenright:
        xpos 1.0 xanchor 0.0 yalign sh_yalign

    # Spica

    image spica smile = "spica/Spica_1.png"
    image spica laugh = "spica/Spica_2.png"
    image spica sly = "spica/Spica_3.png"
    image spica blink = "spica/Spica_4.png"

    ####### MISCELLENEOUS #######

    # A.B.R. (enemy)

    # ABR is very very big
    $ scale = 1.8
    image abr normal = im.FactorScale("enemies/abr_1.png", scale)
    image abr flinch = im.FactorScale("enemies/abr_2.png", scale)

    # Individual ABR sections
    image abr noarms = im.FactorScale("enemies/abr_noarms.png", scale) # ABR with no arms
    image abr_arm1 = im.FactorScale("enemies/abr_arm1.png", scale) # Front arm
    image abr_arm2 = im.FactorScale("enemies/abr_arm2.png", scale) # Front arm
    image abr nohead = im.FactorScale("enemies/abr_nohead.png", scale) # ABR with no head
    image abr_arms = im.FactorScale("enemies/abr_arms.png", scale) # Just the ABR arms

    # Aurica's healing sprite
    image aurica_sprite = "aurica/aurica_sprite.png"
