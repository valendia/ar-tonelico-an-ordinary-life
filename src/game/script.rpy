# You can place the script of your game in this file.

init:
    image credits = "backgrounds/credits.jpg"
    image splash1 = "backgrounds/splash/at_aol_splash1.jpg"
    image splash2 = "backgrounds/splash/at_aol_splash2.jpg"
    image splash3 = "backgrounds/splash/at_aol_splash3.jpg"
    image splash4 = "backgrounds/splash/at_aol_splash4.jpg"
    image splash5 = "backgrounds/splash/at_aol_splash5.jpg"
    image splash6 = "backgrounds/splash/at_aol_splash6.jpg"
    image splash7 = "backgrounds/splash/at_aol_splash7.jpg"
    image splash8 = "backgrounds/splash/at_aol_splash8.jpg"
    image splash9 = "backgrounds/splash/at_aol_splash9.jpg"
    image title = "backgrounds/title/at_aol_title_BG.jpg"
    image title_buttons = "backgrounds/title/at_aol_title_0.jpg"

    # Default starting chapter:
    define start_ch = 1 # This can be changed in the chapters menu

label splashscreen:
#    # Chapter development mode:
#    return

    play music "music/06 - EXEC_PHANTASMAGORIA.ogg"

    $ renpy.pause(0.49)

    show splash1
    with dissolve
    with Pause(0.7)

    show splash2
    with dissolve
    with Pause(0.76)

    show splash3
    with dissolve
    with Pause(0.75)

    show splash4
    with dissolve
    with Pause(0.75)

    show splash5
    with dissolve
    with Pause(0.79)

    show splash6
    with dissolve
    with Pause(0.77)

    show splash7
    with dissolve
    with Pause(0.77)

    show splash8
    with dissolve

    show splash9
    with dissolve

    show title_buttons
    with dissolve

    return

label my_help:
    $ _help()
    return

label my_credits:
    show credits with dissolve
    "" # Wait for the user to click or press enter
    hide credits with dissolve
    return

## Skip the main menu
#label main_menu:
#    hide mainmenu
#    $ renpy.jump_out_of_context("start")
#    $ continued = False

label my_chapters:
    # Show title screen background for the chapters screen
    show title

    # Show the chapter menu, and jump to the chapter chosen by the player
    menu:
        "Return":
            play sound "sounds/menu2.ogg"
            return
        "Chapter 1":
            $ start_ch = 1
        "Chapter 2":
            $ start_ch = 2
        "Chapter 3":
            $ start_ch = 3
        "Chapter 4":
            $ start_ch = 4
        "Chapter 5":
            $ start_ch = 5
        "Chapter 6":
            $ start_ch = 6
        "Chapter 7 (under development)":
            $ start_ch = 7

    play sound "sounds/menu2.ogg"

    # Now jump to the game starting logic (but with the start_ch variable set)
    jump start

# The game starts here.
label start:

    # A variable each chapter can check, to see if it is standalone, or
    # running after the previous chapter:
    $ continued = False

    # Call chapters one after another, starting on the chapter selected
    # from the chapter menu.

    # Chapter 1
    if start_ch <= 1:
        call chapter1
        $ continued = True

    # Chapter 2
    if start_ch <= 2:
        call chapter2
        $ continued = True

    # Chapter 3
    if start_ch <= 3:
        call chapter3
        $ continued = True

    # Chapter 4
    if start_ch <= 4:
        call chapter4
        $ continued = True

    # Chapter 5
    if start_ch <= 5:
        call chapter5
        $ continued = True

    # Chapter 6
    if start_ch <= 6:
        call chapter6
        $ continued = True

        # After chapter 6, jump to the end. Chapter 7 is still under development,
        # peopele need to specifically choose that from the chapters menu to play it
        jump end

    # Chapter 7
    if start_ch <= 7:
        call chapter7
        $ continued = True


label end:
    # Fade out (music and display) and show "to be continued" message
    stop music fadeout 1
    scene black
    with fade
    centered "To be continued..."
