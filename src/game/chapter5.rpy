label chapter5:

    # Reset volume levels, in case the previous chapter left crazy levels
    call reset_volumes

    # If continued from the previous chapter, then fade to black first,
    # to indicate the passage of time since the previous scene
    if continued:
        stop music fadeout 1
        scene black with fade

    play music "music/138 Pulse ~Elemia Arrange~.ogg"
    scene bg classroom with fade
    call show_chat_border

    $ renpy.music.play(["sounds/schoolbell.ogg"], channel=6, loop=False)
    n "The recess bell rings. {nw}"
    $ renpy.sound.set_volume(1.2, channel=0)
    play sound "sounds/pencils.ogg"
    extend "The sound of dropping pencils fills the {nw}"
    $ renpy.sound.set_volume(0.8, channel=1)
    play sound "sounds/desks.ogg" channel 1
    extend "air."
    $ renpy.sound.set_volume(1)
    play sound "sounds/woodsqueek.ogg"
    n "I lean back in my chair and sigh."

    me "Man..."
    $ renpy.sound.set_volume(1, channel=1)
    show mir_sg smile1 at truecenter behind bottom_engraving
    with dissolve
    $ mir_name = "Mir"
    mir "What's wrong?"
    n "I can't tell her that her presence here nearly gave me a heart attack."
    me "Nothing. The test was just a killer, is all."
    n "I lied. It was easy as cake."
    show mir_sg smile2 at truecenter
    mir "Oh."
    hide mir_sg with dissolve
    n "I look around the class. Aurica already left."
    n "I sigh. Guess she's still mad at me."
    show jackh smile behind bottom_engraving at truecenter with dissolve
    $ ja_name = "Jack"
    ja "What's wrong, kid?"
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/running3.ogg"
    me "I just said the test was a kill-{nw}"
    stop music
    $ renpy.sound.set_volume(0.8)
    play sound "sounds/thud.ogg"
    extend "ooof!"
    $ renpy.sound.set_volume(1.0)
    play sound "sounds/thud2.ogg"
    hide jackh
    show bg classroom at Zoom((640, 480), (0, 0, 640, 480), (358, 12, 100, 58), 0)
    with dissolve
    n "I get tackled to the ground."
    n "You guessed it, it was Misha."

    play music "music/126 Expressive Girl.ogg"
    show misha_sg happy behind bottom_engraving at truecenter with dissolve
    $ mish_name = "Misha"
    mish "I got you!"
    me "Get off of me, Misha!"
    $ renpy.sound.set_volume(0.3)
    play sound "sounds/rope.ogg"
    n "Her legs are clinging to mine so tightly that they're cutting off my circulation."
    show misha_sg smile at truecenter with dissolve
    mish "No! I'm not letting you go!"
    show bg classroom
    hide misha_sg
    show mir_sg faintsmile behind bottom_engraving at truecenter
    with dissolve
    mir "Who's that?"
    show jackh smile behind bottom_engraving at jackleft
    show mir_sg faintsmile behind bottom_engraving at chatright
    with moveinleft
    ja "Oh yeah, you wouldn't know, huh Mir?"
    ja "That's Misha. This is a daily routine."
    hide jackh
    show misha_sgh thoughtful1 behind bottom_engraving at chatleft
    with dissolve
    mish "Another new student?"

    show misha_sgh thoughtful2 behind bottom_engraving at chatleft
    play sound "sounds/electricity.ogg"
    show lighting at lightning_pos behind mir_sg with Pause(0.5)
    hide lighting

    n "Misha sees Mir look at me jealously."
    show misha_sgh surprised1 behind bottom_engraving at chatleft
    $ renpy.sound.set_volume(1.0)
    play sound "sounds/attack2.ogg"
    n "She puts two and two together and finds the inevitable conclusion to her math."
    play music "music/220 War of the Panties.ogg" fadeout 0.2
    show misha_sgh angry behind bottom_engraving at chatleft
    mish "Not another one. He's mine, I tell you!"
    $ sh_name = "Shurelia"
    show shurelia_sg shout behind mir_sg at shurcenter with dissolve
    sh "Misha! Cease this obscenity immediately!"

    mish "Why? He's mine!"
    show bg classroom at Zoom((640, 480), (0, 0, 640, 480), (358, 12, 100, 58), 0)
    show misha_sgh angry at truecenter
    hide mir_sg
    hide shurelia_sg
    with dissolve
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/rope.ogg"
    n "She hugs me even more tightly."
    play music "music/130 Pocoscon.ogg" fadeout 0.2
    show bg classroom
    show jack frown behind bottom_engraving at truecenter
    hide misha_sgh
    with dissolve
    ja "He's dying."
    show jack frown at jackright
    show misha_sgh surprised1 behind bottom_engraving at chatleft
    with moveinleft
    mish "Huh?"

    show bg classroom at Zoom((640, 480), (0, 0, 640, 480), (358, 12, 100, 58), 0)
    hide jack
    show misha_sgh surprised1 behind bottom_engraving at truecenter
    with dissolve
    n "She looks at me."
    $ renpy.sound.set_volume(1.0)
    play sound "sounds/bell.ogg"
    n "I lie on the floor, unconscious and blue-faced."

    show bg classroom
    show misha_sgh angry behind bottom_engraving at chatleft
    show shurelia_sg shout behind bottom_engraving at shurcenter
    show mir_sg embarassedshout behind shurelia_sg at chatright
    show jack smile behind misha_sgh at truecenter
    with dissolve

    n "Misha, Mir, and Shurelia scream. I'm pretty sure Jack laughed."

    #-------

    stop music fadeout 0.5
    scene black
    with ComposeTransition(Dissolve(1.0), after=ImageDissolve(im.Tile("dissolves/blood3.png"), 1.0, 8))
    play music "music/109 A Single Breath.ogg"
    scene bg inn_room
    with ComposeTransition(Dissolve(1.0), after=ImageDissolve(im.Tile("dissolves/blood3.png"), 1.0, 8))
    call show_chat_border

    me "Huh?"
    n "I wake up to find myself lying in a bed."
    $ renpy.sound.set_volume(1, channel=1)
    me "The nurse's room, I'm guessing."
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/clock.ogg"
    n "I sigh and look at the clock. I've already missed at least half of the next class."
    $ renpy.sound.set_volume(0.2)
    play sound "sounds/cloth2.ogg"
    n "I get up and {nw}"
    $ renpy.sound.set_volume(0.5, channel=6)
    play music "sounds/footsteps2_loop.ogg" channel 6
    extend "start walking out the room."
    n "The nurse is out. I assume she is getting some coffee."

    stop sound
    stop music channel 6
    stop music fadeout 0.5
    scene black
    with ComposeTransition(Dissolve(0.5), after=CropMove(0.5, "wipeleft"))
    $renpy.pause(0.4)
    $ renpy.sound.set_volume(1.0)
    play sound "sounds/door2.ogg"
    play music "music/138 Pulse ~Elemia Arrange~.ogg"
    scene bg classroom
    with ComposeTransition(Dissolve(0.5), before=CropMove(0.5, "wipeleft"))
    call show_chat_border

    play music "sounds/footsteps2_loop.ogg" channel 6
    n "I open the classroom door and walk toward my seat."
    show leardh smile at truecenter behind bottom_engraving with dissolve
    le "Ah, you're back. Are you feeling alright?"
    me "Yeah. Just a little dizzy, but I can still learn."
    le "Okay. Take your seat."
    stop music channel 6
    play sound "sounds/woodsqueek.ogg"
    n "I take my seat."
    play sound "sounds/magicstart.ogg"

    # Spin the classroom
    show bg classroom as c1 behind bottom_engraving at Move((0.0,0.5), (-1.0,0.5), .5, xanchor="left", yanchor="center", repeat=True)
    show bg classroom as c2 behind bottom_engraving at Move((1.0,0.5), (0,0.5), .5, xanchor="left", yanchor="center", repeat=True)
    hide leardh
    with dissolve
    n "The room spins for three seconds."

    # Stop spinning
    hide c1
    hide c2
    with dissolve

    $ renpy.sound.set_volume(0.5)
    play sound "sounds/papers.ogg"
    show notebook at itempos with dissolve
    n "By the time my vision returns, a notebook had appeared before my face."
    me "Are you alright?"
    show leardh frown at truecenter behind bottom_engraving with dissolve
    le "What was that?"
    me "I said I'm alright!"
    show leardh smile at truecenter with dissolve
    le "Okay."

    play music "music/110 Song For Counting the Memories.ogg" fadeout 1
    hide leardh
    show aurica_sg scared at auricacenter behind bottom_engraving
    with dissolve
    n "I regain my focus and see that Aurica had written to me, asking if I was alright."
    n "I am more happy than anything."
    me_writing "I'm okay."
    show aurica_sg smile1 at auricacenter with dissolve
    n "Aurica exhales as if she is relieved."
    $ au_name = "Aurica"
    au "{i}{u}I'm relieved.{/u}{/i}"
    n "I was right."
    me_writing "Look, about yesterday, it was an accident."
    show aurica_sg smile2 at auricacenter
    au "{i}{u}I know.{/u}{/i}"
    me_writing "What?"
    show aurica_sg laugh at auricacenter with dissolve
    au "{i}{u}I just wanted to mess with you.{/u}{/i}"
    play sound "sounds/silly.ogg"
    n "She doodles a cat sticking its tongue at me and winking."
    me_writing "Sheesh. So where did you go during break?"
    show aurica_sg smile1 at auricacenter with dissolve
    au "{i}{u}It's a secret.{/u}{/i}"
    me_writing "Come on. You owe me for that slap."
    show aurica_sg smile2 at auricacenter
    au "{i}{u}Okay. I was signing up for the school council.{/u}{/i}"
    stop music fadeout 0.5
    show aurica_sg scared at auricacenter
    hide notebook
    me "What!?"
    play music "music/stream.afs_00100.adx.ogg" fadeout 1
    n "I didn't realize that I screamed that out loud."
    hide aurica_sg
    show leardh angry at truecenter behind bottom_engraving
    with dissolve
    le "You two, outside, now!"

    # ----

    stop music fadeout 0.5
    scene black
    with ComposeTransition(Dissolve(1.0), after=ImageDissolve(im.Tile("dissolves/id_circlewipe.png"), 1.0, 8, reverse=True))
    play music "music/110 Song For Counting the Memories.ogg"
    scene bg school_corridoor
    show school_corridoor_front
    with ComposeTransition(Dissolve(1.0), before=ImageDissolve(im.Tile("dissolves/id_circlewipe.png"), 1.0, 8, reverse=True))
    call show_chat_border
    show aurica_sg thoughtful1 at auricaright behind bottom_engraving with dissolve

    n "We stood outside the classroom, buckets filled with water as punishment."
    n "I balanced mine on my head."
    n "She held hers with both hands in front of her."
    me "I can hold that if you want. It was my fault in the first place."
    show aurica_sg smile2 at auricaright
    au "No, it's okay."
    me "Come on. It hurts my pride to see a girl being punished in front of me."
    show aurica_sg smile1 at auricaright
    au "... Okay."
    show aurica_sg smile2 at auricacenter with moveinright
    $ renpy.sound.set_volume(0.8)
    play sound "sounds/splash.ogg"
    show bucket at itempos with dissolve
    n "She passes me her bucket."
    hide bucket with dissolve
    $ renpy.sound.set_volume(1)
    n "This is easy. It was basic conditioning back when I was on the baseball team."
    n "Of course, I left that too."
    me "Are you sure you're okay now?"
    show aurica_sg thoughtful2 at auricacenter
    au "What do you mean?"
    me "Am I really forgiven?"
    show aurica_sg laugh at auricacenter with dissolve
    n "Aurica giggles."
    au "You're forgiven."

    me "Thank God..."
    play music "music/117 The Sunset Glow Is Bright Red.ogg" fadeout 1

    # Show sakura petals falling outside the window
    show aurica_sg smile1 at auricacenter with dissolve
    show snowblossom behind school_corridoor_front
    with dissolve

    n "We look out the window. Sakura petals are falling."
    me "Oh yeah. It's sakura season."
    show aurica_sg laugh at auricacenter with dissolve
    au "So pretty."
    n "I smile. Aurica begins to hum something."
    me "What's that?"
    show aurica_sg smile1 at auricacenter with dissolve
    au "It's a song that's been floating around in my head for a while now."
    show aurica_sg smile2 at auricacenter
    au "It comes to mind whenever I think of you."
    me "Really? Can I hear it?"
    show aurica_sg sadsmile at auricacenter
    au "I'm not sure you'll like it."
    me "Don't worry. If anything, it's impossible for me to hate it."
    me "After all, you made it for me."
    show aurica_sg smile1 at auricacenter
    au "Okay."
    show aurica_sg smile2 at auricacenter
    play music "music/10 Reisha's Lullaby.ogg" fadeout 1
    n "She begins to sing. The song is beautiful."
    n "Once again, though, it's being sung in the language that I learned was called Hymmnos."
    me "It's beautiful."
    play music "music/110 Song For Counting the Memories.ogg" fadeout 1
    show aurica_sg smile1 at auricacenter
    au "Thank you."
    show aurica_sg smile2 at auricacenter
    au "I looked it up online last night, and read that it was something only Reyvateils can do."
    me "Reyvateils?"
    show aurica_sg thoughtful2 at auricacenter
    au "Yeah. I'm not sure what that means either."
    me "It sounds pure... It suits you."
    show aurica_sg smile2 at auricacenter
    au "... Thank you."
    show aurica_sg smile1 at auricacenter
    n "She smiles her beautiful smile."
    play sound "sounds/schoolbell.ogg"
    n "The bell rings."

    # ----

    stop music fadeout 0.5
    scene black with squares
    play music "music/138 Pulse ~Elemia Arrange~.ogg"
    scene bg classroom with squares
    call show_chat_border
    play sound "sounds/woodsqueek.ogg" fadeout 0.5
    n "I sit down at my desk, relieved that I can finally throw away those buckets. My neck is sore."
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/stomache.ogg"
    n "My stomach growls."
    n "I never did get anything to eat. My last meal was yesterday morning, I think."
    n "I sigh."
    me "Might as well go buy some bread."
    me "By now, there's probably only plain bread left."
    play music "music/110 Song For Counting the Memories.ogg" fadeout 1
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/papers.ogg"
    show aurica_sg smile1 at auricacenter behind bottom_engraving with dissolve
    au "Umm, here."
    me "Huh?"
    n "She hands me a bento. It looks fancy."
    me "I can't."
    show aurica_sg smile2 at auricacenter
    au "Please?"
    play sound "sounds/click2.ogg"
    n "I open up the bento."
    show bento1 at itempos with dissolve
    play music "music/120 Leave It Be.ogg" fadeout 0.2
    n "I didn't know eggs could be{w=0.2}.{w=0.2}.{w=0.2}.{w=0.2} {nw}"

    $ renpy.sound.set_volume(1)
    play sound "sounds/silly2.ogg"
    extend "Green?"
    me "What is this?"
    show aurica_sg smile1 at auricacenter
    au "They're seaweed eggs."
    show aurica_sg laugh at auricacenter with dissolve
    au "I mixed lots of yummy things together, and this is what came out."
    au "Here."
    play music "sounds/bubbling.ogg" channel 6
    hide bento1
    show bbqsoda at itempos
    with dissolve
    n "She passes me a cup of brown stuff."
    au "I call this BBQ Soda."
    n "I stare at it for at least eight seconds."
    n "I didn't know if this stuff would even stay in my stomach for {i}three{/i} seconds."
    stop music fadeout 1 channel 6
    n "I look back at Aurica."
    show aurica_sg smile2 at auricacenter with dissolve
    au "I stayed up all night making this for you."
    n "I can't refuse that."
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/running3.ogg"
    me "Well, okay. Itadak-"
    stop music fadeout 0.5
    $ renpy.sound.set_volume(2.0)
    play sound "sounds/thud2.ogg"
    hide aurica_sg
    hide bbqsoda
    show bg classroom at Zoom((640, 480), (0, 0, 640, 480), (358, 12, 100, 58), 0)
    with dissolve
    n "{b}Bam!{/b}"
    n "I get tackled again. This is all too familiar."
    play music "music/126 Expressive Girl.ogg"
    show misha_sg happy behind bottom_engraving at truecenter with dissolve
    mish "I'm back!"
    show  misha_sg happyblush at truecenter
    mish "Hey, guess what?"
    me "I'm going to die again?"
    show misha_sg smile at truecenter
    mish "No, silly."

    show bg classroom with dissolve
    show bento2 at itempos with dissolve
    n "She gets off of me and presents me with a bento."
    show misha_sg happy at truecenter with dissolve
    mish "I made this for you! I knew you were hungry, so I made this!"
    show misha_sg thoughtful2 at truecenter with dissolve
    hide bento2
    show bento1 at itempos
    with dissolve
    n "She spots the bento on my desk from the corner of her eye."

    hide misha_sg
    show misha_sgh angry behind bottom_engraving at chatleft
    show aurica_sg smile2 behind bottom_engraving at auricaright
    with dissolve
    play music "music/238 Last Chapter - Illusionary Gene.ogg" fadeout 0.5
    $ renpy.sound.set_volume(1.0)
    play sound "sounds/electricity.ogg"
    show lighting behind misha_sgh at lightning_pos with Pause(0.4)
    hide lighting with Pause(0.2)
    show lighting behind misha_sgh at lightning_pos with Pause(0.1)
    hide lighting with Pause(0.2)
    show lighting behind misha_sgh at lightning_pos with Pause(0.4)
    hide lighting

    n "She then glares at Aurica after putting two and two together."
    mish "Could it be that she made that for you?"
    me "Umm... Yeah."
    hide bento1
    show bento2 at itempos
    with dissolve
    mish "No, no, no! I stayed up all night to make this for you."
    hide bento2 with dissolve
    me "Well..."
    show aurica_sg sad at auricaright with dissolve
    au "Are you going to eat hers?"
    n "She looks disappointed and sad."
    me "No, no! I'll eat yours!"
    show misha_sgh badmemories at chatleft with dissolve
    mish "Bu.. but..."
    n "Misha starts to tear up."
    me "I'll eat yours too!"
    n "I've lost. This is a game that cannot be won. It's like the Special Olympics."
    n "Even if I win, I'll still be a loser in the end."
    play music "music/232 Rumbling.ogg" fadeout 1
    show bento1 at itempos
    play sound "sounds/thud6.ogg"
    $ renpy.pause(0.2)
    hide bento1
    show bento2 at itempos
    play sound "sounds/thud6.ogg"
    $ renpy.pause(0.2)
    hide bento2
    n "I put both bentos on my desk and look at them in turn."
    show misha_sgh angry at chatleft
    show aurica_sg cross at auricaright
    with dissolve
    n "Aurica and Misha both stare at me with anticipation."
    $ renpy.sound.set_volume(2.0)

    hide misha_sgh
    hide aurica_sg
    show jackh smile behind bottom_engraving at jackleft
    show shurelia_sg smile1 behind bottom_engraving at shurright
    with dissolve
    play sound "sounds/feedback.ogg"
    ja "So, Announcer Shurelia, which bento do you think will come out victorious?"
    show shurelia_sg smile2 behind bottom_engraving at shurright
    sh "I don't know, Announcer Jack. He seems to be in quite a bind."
    $ renpy.sound.set_volume(1)
    ja "Indeed, Announcer Shurelia. Whichever he eats first will give a good impression to the first chef,"
    ja "but the second cook will feel left out, and ultimately defeated; disappointed."
    show shurelia_sg smile1 at shurright
    sh "Yes, Announcer Jack."
    sh "What do you think, Judge Mir?"

    hide jackh
    hide shurelia_sg
    show mir_sg smile1 at truecenter behind bottom_engraving
    with dissolve
    show flags at itempos with dissolve
    n "Mir stands behind me holding a blue flag in one hand and a red one in the other."
    show mir_sg faintsmile at truecenter
    hide flags with dissolve
    n "She shrugs."
    me "You guys aren't helping!"
    me "Moreover, how the hell did you get those mics?"
    me "And Mir, when did you make those flags!?"
    show mir_sg smile1 at truecenter
    mir "Two seconds ago."

    hide mir_sg
    show aurica_sg laugh behind bottom_engraving at auricacenter
    show bento1 at itempos
    with dissolve
    au "Please, eat mine!"
    show aurica_sg laugh behind bottom_engraving at auricaright
    show misha_sgh happy behind bottom_engraving at chatleft
    with moveinleft
    hide bento1
    show bento2 at itempos
    with dissolve
    mish "No, eat mine!"
    hide bento2 with dissolve
    $ renpy.sound.set_volume(1.2)
    play sound "sounds/attack.ogg"
    show bg classroom with hpunch
    n "My mind is overflowing with suggestions and confusion."
    $ renpy.sound.set_volume(1)
    play sound "sounds/swish.ogg"

    show aurica_sg laugh at auricaoffscreenright
    show misha_sgh happy at truecenter
    with moveinleft
    n "I look to Misha,"

    play sound "sounds/swish.ogg"
    show misha_sgh happy at chatoffscreenleft
    show aurica_sg laugh at auricacenter
    with moveinright
    n " then Aurica,"

    play sound "sounds/swish.ogg"
    show aurica_sg laugh at auricaoffscreenright
    show misha_sgh happy at truecenter
    with moveinleft
    n "then back to Misha,"

    play sound "sounds/swish.ogg"
    show misha_sgh happy at chatoffscreenleft
    show aurica_sg laugh at auricacenter
    with moveinright
    n "then Aurica."

    play sound "sounds/swish.ogg"
    hide aurica_sg
    show jackh smile behind bottom_engraving at truecenter
    with dissolve
    n "I glare at Jack."

    play sound "sounds/swish.ogg"
    hide jackh
    show mir_sg smile1 at truecenter behind bottom_engraving
    with dissolve
    n "I look behind me at Mir, checking to see whether she can bail me out."
    show mir_sg faintsmile at truecenter
    n "She keeps her mouth shut."

    play sound "sounds/swish.ogg"
    hide mir_sg
    show misha_sgh angry at truecenter
    with dissolve
    n "I look back at Misha."
    n "There is a look of anticipation on her face, as though she knows I will pick hers for sure;"
    n "she will hate me forever if I don't."

    play sound "sounds/swish.ogg"
    hide misha_sgh
    show aurica_sg smile1 behind bottom_engraving at auricacenter
    with dissolve

    n "I look at Aurica."
    n "Hers is a fragile, innocent look that begs me to eat hers first; she will cry if I don't."
    play sound "sounds/swish.ogg"
    hide aurica_sg with dissolve
    n "I look at the bentos."
    stop music fadeout 0.5
    me "I-Itadakimasu!"
    play music "music/130 Pocoscon.ogg"

    show bg classroom with hpunch
    n "I stuff the contents of both bentos into my mouth at the same time and chew as fast as I can."
    show bg classroom with hpunch
    n "Both are so good."
    show bg classroom with hpunch
    show bento1 at itempos with dissolve
    n "Sure Aurica's looks like prison food,"
    n "but this is what they mean when they say you can't judge a book by it's cover."
    $ renpy.sound.set_volume(2.0)
    play sound "sounds/feedback.ogg"
    hide bento1
    show jackh smile behind bottom_engraving at jackleft
    show shurelia_sg smile1 behind bottom_engraving at shurright
    with dissolve
    ja "Oooooh! He goes for both bentos at the same time! What could this mean!?"
    show shurelia_sg smile2 at shurright
    sh "This means that there could be two winners!"
    sh "It's all down to whose he thinks tastes better."
    $ renpy.sound.set_volume(1)

    hide jackh
    hide shurelia_sg
    show misha_sgh angry behind bottom_engraving at chatleft
    show aurica_sg smile1 behind bottom_engraving at auricaright
    with dissolve
    n "The girls eagerly await my answer."
    show bg classroom with hpunch
    n "I finish the last of the contents."
    play music "music/208 Heave-Ho Half-Off Sale!.ogg" fadeout 0.5
    show misha_sgh angry at chatoffscreenleft
    show aurica_sg smile1 at auricaoffscreenright
    with moveoutleft
    show bg classroom at Zoom((640, 480), (0, 0, 640, 480), (237, 342, 211, 130), 0.5)
    $renpy.pause(0.5)
    $renpy.sound.set_volume(2.0)
    play sound "sounds/thud2.ogg"
    show black with fade
    centered "I begin to choke, and then fall to the ground, unconscious... Again."

    show bg classroom
    hide black
    show mir_sg smile1 at truecenter behind bottom_engraving
    show flags at itempos
    with fade
    n "Mir raises both flags into the air."
    $ renpy.sound.set_volume(1.0)
    play sound "sounds/silly3.ogg"
    show mir_sg laugh at truecenter with dissolve
    mir "A tie."
    hide flags with dissolve
    $ renpy.sound.set_volume(2.0)
    play sound "sounds/feedback.ogg"

    show shurelia_sg smile1 behind mir_sg at shuroffscreenright with None
    show jackh smile behind mir_sg at jackleft
    show shurelia_sg smile1 at shurright
    with moveinleft

    ja "That concludes today's match."
    show shurelia_sg smile2 at shurright
    sh "Expect more tomorrow. This is Shurelia and Jack, signing off."
    $ renpy.sound.set_volume(1)
    hide mir_sg
    hide shurelia_sg
    hide jackh
    show misha_sgh angry behind bottom_engraving at chatleft
    show aurica_sg shout behind bottom_engraving at auricaright
    with dissolve
    "{size=-5}Aurica & Misha{/size}" "Are you alright?"

    show black with fade
    play sound "sounds/bell.ogg"
    centered "\"Gochisousama...\""
    centered "I twitch."
    centered "This can't be good for my metabolism."

    # -----

    stop music fadeout 0.5
    scene black with fade
    play music "music/138 Pulse ~Elemia Arrange~.ogg"
    scene bg classroom2
    with ComposeTransition(Dissolve(1.0), after=ImageDissolve(im.Tile("dissolves/id_circlewipe.png"), 1.0, 8, reverse=True))
    call show_chat_border

    n "School ends."
    me "Why am I here, again?"
    show shurelia_sg smile1 behind bottom_engraving at shurcenter with dissolve
    sh "Because you didn't attend detention yesterday."
    me "But why am I doing student council work for you?"
    show shurelia_sg thoughtful1 at shurcenter
    sh "Because if you didn't, then you would be suspended."
    show shurelia_sg smile1 at shurcenter
    sh "Just be happy that I asked the principal to spare you by doing this."
    show radolfh smile behind bottom_engraving at chatleft
    show shurelia_sg smile1 at shurright
    with moveinleft
    $ ra_name = "Radolf"
    ra "Thank you for your assistance."
    me "Yeah yeah."
    n "Radolf is the vice president and Shurelia is the president."

    show flute behind bottom_engraving at truecenter
    hide shurelia_sg
    hide radolfh
    with dissolve
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/papers.ogg"
    n "Flute is behind me, counting the school's profits and expenses."
    n "I guess he's the treasurer."

    hide flute
    show aurica_sg smile2 behind bottom_engraving at auricacenter
    with dissolve
    n "Aurica is the new secretary."

    hide aurica_sg
    show radolfh smile behind bottom_engraving at chatleft
    show shurelia_sg smile1 behind bottom_engraving at shurright
    with dissolve
    me "You mean, we didn't have a school secretary before?"
    show shurelia_sg thoughtful1 at shurright
    sh "We did. She graduated."
    show radolfh thoughtful2 at chatleft
    show clair_portrait at itempos
    with dissolve
    ra "I think her name was Clair. She graduated when I took over as the vice president."
    hide clair_portrait with dissolve
    show shurelia_sg thoughtful2 at shurright
    sh "I wonder what she's doing now?"

    show flute behind bottom_engraving at truecenter
    hide shurelia_sg
    hide radolfh
    with dissolve
    fl "She's a singer at a bar now. She said she's doing fine."

    show flute at chatleft
    show aurica_sg smile2 behind bottom_engraving at auricaright
    with moveinright
    au "I hope I learn to be as good as her someday."

    hide flute
    hide aurica_sg
    show radolfh laugh behind bottom_engraving at chatleft
    show shurelia_sg smile1 behind bottom_engraving at shurright
    with dissolve
    ra "Hahaha. I'm sure you will. Just believe in yourself."
    me "Sounds boring."

    show shurelia_sg shout behind bottom_engraving at shurright
    sh "Govern your tongue!"
    me "Yeah, yeah."

    hide shurelia_sg
    hide radolfh
    show aurica_sg smile2 behind bottom_engraving at auricacenter
    with dissolve
    au "Are you in any clubs right now?"
    me "No. I've done pretty much all the ones that caught my eye. They all got boring."

    hide aurica_sg
    show radolfh smile behind bottom_engraving at truecenter
    with dissolve
    ra "You should choose one and stick to it."
    ra "Who knows, you might like the competition."
    me "What about you, Radolf? What club are you in?"
    ra "Me? I'm in the Lancing and Kendo club."
    ra "I plan to be a knight of the Church once I graduate."

    hide radolfh
    show aurica_sg laugh behind bottom_engraving at auricacenter
    with dissolve
    au "Wow, that's so great."

    hide aurica_sg
    show radolfh laugh behind bottom_engraving at chatleft
    show shurelia_sg smile1 behind bottom_engraving at shurright
    with dissolve
    ra "Hahaha, not really."
    show shurelia_sg thoughtful1 at shurright
    sh "I don't have time for clubs. There's always more paperwork to do."
    play music "music/120 Leave It Be.ogg" fadeout 0.5
    me "You should go find yourself a boyfriend, Shurelia."
    show shurelia_sg thoughtful2 at shurright
    sh "I told you, I don't have time."
    me "If you want, how about we go on a date someday?"

    hide radolfh
    hide shurelia_sg
    show flute behind bottom_engraving at truecenter
    with dissolve
    fl "That sounds like a great idea."

    hide flute
    show shurelia_sg cryshout behind bottom_engraving at shurcenter
    with dissolve
    sh "Wha-wha-"
    me "Hahahaha. Just kidding. It's just so fun to see you flustered."
    show shurelia_sg shout at shurcenter
    sh "Stop teasing me!"

    hide shurelia_sg
    show flute behind bottom_engraving at fluteright
    show radolfh laugh behind bottom_engraving at radolfleft
    with dissolve
    show bg classroom2 with vpunch
    n "Radolf, Flute, and I all laugh."

    hide flute
    hide radolfh
    show shurelia_sg cryshout behind bottom_engraving at shurcenter
    with dissolve
    n "Shurelia continues to blush."
    play music "music/110 Song For Counting the Memories.ogg" fadeout 0.5

    hide shurelia_sg
    show aurica_sg sadsmile behind bottom_engraving at auricaright
    with dissolve
    n "Aurica giggles, but a sad look creeps across her face for about three seconds."

    $ renpy.sound.set_volume(0.5)
    play sound "sounds/desks.ogg"
    show aurica_sg sadsmile at auricacenter
    with moveinright
    n "I notice, and move my seat next to hers, offering to help her with her work."
    $ renpy.sound.set_volume(1)
    show aurica_sg smile2 at auricacenter
    n "She seems better now."
    scene black
    with ComposeTransition(Dissolve(1.0), after=ImageDissolve(im.Tile("dissolves/id_circlewipe.png"), 1.0, 8, reverse=True))
    stop music fadeout 0.5
    $ renpy.pause(0.4)
    play music "music/138 Pulse ~Elemia Arrange~.ogg"
    scene bg classroom2
    with ComposeTransition(Dissolve(1.0), after=ImageDissolve(im.Tile("dissolves/id_circlewipe.png"), 1.0, 8, reverse=True))
    call show_chat_border
    n "Finally, night falls."
    show radolfh smile behind bottom_engraving at truecenter with dissolve
    n "Radolf insists on giving me a ride home,"
    n "but we live on opposite sides of the city, so I kindly decline his offer."

    play sound "sounds/harp1.ogg"

    hide radolfh
    show aurica_sg smile2 behind bottom_engraving at auricacenter
    show aurica_blue behind aurica_sg at auricacenter
    show aurica_sprite behind bottom_engraving at ch4_sprite_pos
    show aurica_sprite_magic behind bottom_engraving at ch4_sprite_pos
    with dissolve
    n "Aurica sings my stomachache away."
    n "I feel kinda bad, but she insists."

    hide aurica_blue
    hide aurica_sprite
    hide aurica_sprite_magic
    with dissolve

    $ renpy.sound.set_volume(0.4)
    play sound "sounds/clock.ogg"
    show aurica_sg dismay at auricacenter with dissolve
    n "Soon after, Aurica looks at the clock and gasps."
    show aurica_sg shout at auricacenter with dissolve
    au "Oh no, I have to get to the convenience store to buy Blackie some food!"
    stop sound
    $ renpy.sound.set_volume(1)
    me "You should get going then."
    show aurica_sg smile1 at auricacenter with dissolve
    au "Yeah. I'll see you next time then."
    me "Oh, Aurica!"
    show aurica_sg smile2 at auricacenter
    au "Yeah?"
    me "Remember. You want a bucket's worth of Kitty Candy."
    show aurica_sg thoughtful2 at auricacenter
    au "I do?"
    me "Just remember that."
    show aurica_sg smile1 at auricacenter with dissolve
    au "... Okay."
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/running3.ogg"
    hide aurica_sg
    with easeoutleft

    n "And so she runs out the door."
    n "I proceed with the paperwork so I can get home earlier."
    n "At this rate though, the sun will be up by the time I finish..."

    show black
    with ComposeTransition(Dissolve(1.0), after=ImageDissolve(im.Tile("dissolves/id_circlewipe.png"), 1.0, 8, reverse=False))
    hide black
    with ComposeTransition(Dissolve(1.0), after=ImageDissolve(im.Tile("dissolves/id_circlewipe.png"), 1.0, 8, reverse=False))

    show shurelia_sg smile1 behind bottom_engraving at shurright with dissolve
    show chesterh neutral2 behind bottom_engraving at chatleft with moveinleft

    n "Time passes, and Shurelia is picked up by her driver."
    show shurelia_sg smile1 at chatoffscreenright
    show chesterh neutral2 at chatoffscreenleft
    with moveoutleft

    n "Guess she's as rich as she looks."

    show flute behind bottom_engraving at fluteright with dissolve
    show jackh smile behind bottom_engraving at jackleft with moveinleft
    n "Jack comes to pick Flute up on his bike."

    n "I ask him if he bothers with this every day."
    ja "It's alright. I finish practice with the rifle club at around this time."

    show flute at chatoffscreenright
    show jackh smile at chatoffscreenleft
    with moveoutleft
    n "I didn't even know this school {i}had{/i} a rifle club. Meh."
    n "I am left alone to walk home."

    stop music fadeout 0.5
    scene black
    with ComposeTransition(Dissolve(1.0), after=ImageDissolve(im.Tile("blindstile.png"), 1.0, 8))
    stop music fadeout 0.5
    $renpy.pause(0.5)
    play music "music/125 Dimly Lit.ogg"
    scene bg street3
    with ComposeTransition(Dissolve(1.0), before=ImageDissolve(im.Tile("blindstile.png"), 1.0, 8))
    call show_chat_border
    n "I didn't realize how dark it was out."
    $ renpy.sound.set_volume(0.4, channel=6)
    play music "sounds/footsteps3.ogg" channel 6
    n "On my way home, I think about what might await me tomorrow."
    n "I just want to go home and watch some samurai flicks."
    n "I sigh."
    me "I wish this day was over..."

    stop music fadeout 0.5
    stop music channel 6
    $ renpy.sound.set_volume(1.0)
    play sound "sounds/crash.ogg"

    n "I hear a crash behind me."
    me "What was that!?"

    $ renpy.music.set_volume(1.0) # Bump up music volume (default level is 0.5)
    play music "music/232 Rumbling.ogg"
    play sound "sounds/robot_roar.ogg"
    $ renpy.sound.set_volume(1.0, channel=1)
    play sound "sounds/running3.ogg" channel 1
    show kruscheh shout behind bottom_engraving at kruscheoffscreenleft
    hide kruscheh with moveoutright
    show abr normal behind bottom_engraving at chatoffscreenleft
    hide abr with moveoutright

    n "I turn around and see a machine chasing after a girl."
    me "An A.B.R.?"
    $ renpy.sound.set_volume(1, channel=1)
    $ renpy.sound.set_volume(1.0)
    play sound "sounds/running3.ogg"
    show bg street3 at Zoom((640, 480), (0, 0, 640, 480), (486, 277, 144, 96), 1.0)
    with Pause(1.0)
    n "I chase after it."
    n "I just can't leave my curiosity and need for excitement alone, can I?"

    scene bg dark_room with dissolve
    call show_chat_border
    play sound "sounds/running3.ogg"

    show kruscheh shout behind bottom_engraving at Position(xalign=1.2, yalign=0.55)
    show abr normal behind bottom_engraving at Position(xalign=-0.5, yalign=0.5)
    with dissolve
    n "I chase and follow them into a dead end."

    play music "music/223 Sleipnir.ogg" fadeout 0.5
    $ renpy.sound.set_volume(0.7)
    play sound "sounds/chainsaw.ogg"

    hide kruscheh
    show krusche shout behind bottom_engraving at Position(xalign=1.2, yalign=0.55)
    show chainsaw at itempos
    with dissolve
    n "The girl turns around and holds out a chainsaw."
    hide chainsaw with dissolve

    $ kr_name = "Girl"
    show krusche smile at Position(xalign=1.2, yalign=0.55)
    kr "Led you right into my trap!"
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/robot_move.ogg"
    show abr normal at Position(xalign=-0.2, yalign=0.5) with moveinleft
    n "The A.B.R. inches forward."
    $ renpy.sound.set_volume(1.5)
    play sound "sounds/chainsaw2.ogg"
    show krusche shout at Position(xalign=1.2, yalign=0.55)
    show slash behind bottom_engraving
    show chainsaw at itempos
    with dissolve
    hide chainsaw
    hide slash
    with dissolve
    n "The girl slices a rope next to her to trigger a trap."
    show falling_crates behind krusche
    hide chainsaw with dissolve
    stop music fadeout 0.5
    $ renpy.sound.set_volume(1.2)
    play sound "sounds/explosion.ogg"
    show abr flinch at Position(xalign=-0.2, yalign=0.5)
    hide abr with moveoutbottom
    n "Lots of boxes and crates fall onto the A.B.R. from above, raising a loud clamor."
    hide falling_crates with dissolve
    n "Guess she didn't need my help after all."
    stop sound fadeout 0.5
    $ renpy.sound.set_volume(1)
    play music "music/107 Salute!.ogg"
    show krusche smile behind bottom_engraving at Position(xalign=1.2, yalign=0.55)
    n "The girl jumps up and down in triumph."
    kr "Yeah! I did it!"
    kr "You don't mess with Krusche!"
    stop music fadeout 1
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/woodrumble.ogg"
    n "The rubble begins to tremble."
    show bg dark_room with vpunch
    $ renpy.sound.set_volume(1.0)
    play sound "sounds/explosion2.ogg"
    show abr normal behind bottom_engraving at Position(xalign=-0.2, yalign=0.55)
    show krusche shout at Position(xalign=1.2, yalign=0.55)
    with moveinbottom
    n "{color=#f00}{b}Boom!{/b}{/color}"
    play music "music/223 Sleipnir.ogg" fadeout 0.5
    play sound "sounds/robot_beep.ogg"
    n "The A.B.R. bursts through the pile."
    show krusche dismay behind bottom_engraving at Position(xalign=1.2, yalign=0.55)
    $ kr_name = "Krusche"
    kr "Oh, crap."
    $ renpy.sound.set_volume(1)
    play sound "sounds/chainsaw_start.ogg"
    show chainsaw at itempos with dissolve
    hide chainsaw with dissolve

    n "She tries to start her chainsaw."
    n "It won't start."
    play sound "sounds/chainsaw_start.ogg"
    show chainsaw at itempos with dissolve
    n "She {nw}"
    queue sound "sounds/chainsaw_start.ogg"
    extend "tugs and tugs on the cord, but it won't start up."
    hide chainsaw with dissolve
    show krusche shout behind bottom_engraving at Position(xalign=1.2, yalign=0.55)
    kr "Dang, out of power!"
    show krusche dismay behind bottom_engraving at Position(xalign=1.2, yalign=0.55)
    kr "Oh no!"
    $ renpy.sound.set_volume(1)
    play sound "sounds/robot_roar.ogg"
    #show abr normal at Position(xalign=0.3, yalign=0.5) with t
    show abr normal at Move((0.2, 0.5), (0.4, 0.5), 0.2, xanchor="center", yanchor="center", repeat=False)
    n "The A.B.R. lunges at her."
    me "Hey!"

    # Reset volume levels, to be polite to the next chapter
    call reset_volumes

    return
