# Declarations for special effects

init:

    # Flashes

    $ blackflash = Fade(0.2, 0.0, 0.8, color='#000')
    $ redflash = Fade(0.2, 0.0, 0.8, color='#f00')
    $ whiteflash = Fade(0.2, 0.0, 0.8, color='#ffffff')

    # Graphics for special effects

    # Sakura blossom animation
    image snowblossom = SnowBlossom(anim.Filmstrip("effects/sakura.png", (20, 20), (2, 1), .15), yspeed=(100, 120), count=20, fast=True)

    # Horizonal lighting effect
    image lighting = "effects/lightning.png"
    $ lightning_pos = Position(xalign=0.0, yalign=0.25) # Head level

    # A few slash effects:
    image slash = "effects/slash.png"
    image slash2 = "effects/slash2.png"
    image slash2_transparent = "effects/slash2_transparent.png"
    image slash3 = "effects/slash3.png"
    image slash4 = "effects/slash4.png"

    # Glowing blue aurica, for when she uses magic in her schoolgirl outfit:
    # Code shamelessly copied & pasted and modified slightly frmom the Ren'Py tutorial...
    # To get a blue outline, we make a slightly larger Aurica pic and show it
    # behind her. The tint RGB values are to get it close to the AT OVA blue shade

    image aurica_blue = im.Scale(im.MatrixColor(im.MatrixColor(ImageReference("aurica_sg smile1"), im.matrix.brightness(.9)), im.matrix.saturation(.5) * im.matrix.tint(.035, .894, 0.906)), 227+4, 293+4)

    # Particle bursts

    # Particle Burst stuff for Aurica's healing sprite
    python:
        class AuSpriteMagicFactory: # the factory that makes the sprite particles
            def __init__(self, theDisplayable, explodeTime=0, numParticles=20, pos=(0.5, 0.5)):
                self.displayable = theDisplayable
                self.time = explodeTime
                self.particleMax = numParticles
                self.pos = pos

            def create(self, partList, timePassed):
                newParticles = None
                if partList == None or len(partList) < self.particleMax:
                    if timePassed < self.time or self.time == 0:
                        newParticles = self.createParticles()
                return newParticles

            def createParticles(self):
                timeDelay = renpy.random.random() * 0.6
                return [AuSpriteMagicParticle(self.displayable, timeDelay, self.pos)]

            def predict(self):
                return [self.displayable]


        class AuSpriteMagicParticle:

                def __init__(self, theDisplayable, timeDelay, pos):
                    self.displayable = theDisplayable
                    self.delay = timeDelay
                    self.xSpeed = (renpy.random.random() - 0.5) * 0.02
                    self.ySpeed = (renpy.random.random() - 0.5) * 0.02
                    self.xPos = pos[0]
                    self.yPos = pos[1]

                def update(self, theTime):

                    if (theTime > self.delay):
                        # Add gravity-like effect
                        self.ySpeed += 0.0002

                        self.xPos += self.xSpeed
                        self.yPos += self.ySpeed

                        if self.xPos > 1.05 or self.xPos < -1.05 or self.yPos > 1.05 or self.yPos < -1.05:
                            return None

                    return (self.xPos, self.yPos, theTime, self.displayable)

    image aurica_sprite_magic = Particles(AuSpriteMagicFactory("aurica/sprite_particle.png", numParticles=80, pos=(0.9, 0.15)))

    # Falling crates animation, used at the end of ch5. Similar to the magic
    # particles, but with falling crates, from offscreen
    python:
        class FallingCratesFactory: # the factory that makes the crate particles
            def __init__(self, theDisplayable, explodeTime=2.0, numParticles=20):
                self.displayable = theDisplayable
                self.time = explodeTime
                self.particleMax = numParticles

            def create(self, partList, timePassed):
                newParticles = None
                if partList == None or len(partList) < self.particleMax:
                    if timePassed < self.time or self.time == 0:
                        newParticles = self.createParticles()
                return newParticles

            def createParticles(self):
                timeDelay = renpy.random.random() * 0.6
                return [CrateParticle(self.displayable, timeDelay)]

            def predict(self):
                return [self.displayable]


        class CrateParticle:

                def __init__(self, theDisplayable, timeDelay):
                    self.displayable = theDisplayable
                    self.delay = timeDelay
                    self.xSpeed = (renpy.random.random() - 0.5) * 0.005
                    self.ySpeed = (renpy.random.random() - 0.5) * 0.1
                    self.xPos = 0.0
                    self.yPos = -0.5

                def update(self, theTime):

                    if (theTime > self.delay):
                        # Add gravity-like effect
                        self.ySpeed += 0.0002

                        self.xPos += self.xSpeed
                        self.yPos += self.ySpeed

                        if self.xPos > 1.05 or self.xPos < -1.05 or self.yPos > 1.05 or self.yPos < -1.05:
                            return None

                    return (self.xPos, self.yPos, theTime, self.displayable)

    $ scale = 2.0 # Crate picture is a bit small
    $ w = int(120 * scale)
    $ h = int(113 * scale)
    image crate = im.Scale("misc/crate.png", w, h)
    image falling_crates = Particles(FallingCratesFactory(ImageReference("crate"), numParticles=5))

    # Thing for showing Hymmnos songs with translations
    python:
        def hymmnos_song_formatter_func(st, at):
            song_fmt = "{font=fonts/hymmnos.ttf}%(hymmnos)s{/font}\n\n{size=-4}{i}{{%(hymmnos)s}\n\n%(english)#s{/i}{/size}"
            song_text = Text(song_fmt % globals(), outlines = [(1, "#000000")])
            return song_text, None
    image hymmnos_song = DynamicDisplayable(hymmnos_song_formatter_func)

    # Glowing pink orb animation:
    image pink_orb_anim = Animation(
        "misc/animations/pink_orb_1.png", 0.1,
        "misc/animations/pink_orb_2.png", 0.1,
        "misc/animations/pink_orb_3.png", 0.1,
        "misc/animations/pink_orb_2.png", 0.1)
