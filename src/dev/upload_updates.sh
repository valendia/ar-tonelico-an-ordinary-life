#!/bin/bash

# This Linux script uploads the game files to wizzardx.isisview.org

set -e

echo "Changing to script directory..."
cd $(dirname $0)

echo "Checking that the game was built for win32"
EXE_FILE="../AnOrdinaryLife.exe"
if [ ! -f $EXE_FILE ]; then
  echo "Windows game executable ($EXE_FILE) not found!"
  exit 1
fi

echo "Making file permissions super permissive, to avoid Windows problems"
chmod 777 .. -R

echo "Uploading files..."
rsync -va --progress --delete ../ wizzardx.isisview.org:/var/www/uploads/at_aol/AnOrdinaryLife/

echo "Creating zip file..."
ssh -C wizzardx.isisview.org "
    set -e
    cd /var/www/uploads/at_aol
    rm -f AnOrdinaryLife.zip
    zip -r AnOrdinaryLife.zip AnOrdinaryLife
    "
