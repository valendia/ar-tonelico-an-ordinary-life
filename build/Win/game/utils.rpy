
# Useful utilitity functions for the visual novel

init:

    # Main music channel, which we play at a lower level so that music stays
    # in the background
    $ MUSIC_VOL = 0.5

# Function for showing the Ar Tonelico 1 "visual novel" chat box
label show_chat_border:

    # Move widgets into place for dialogue
    show bottom_engraving
    with moveinbottom
    show chat_border at left
    with dissolve

    return

# Function for resetting sound levels to their defaults. Useful when a
# chapter has been tweaking them like crazy and wants to set them back to
# sane levels before the next chapter
label reset_volumes:

    # Sound channels
    $ renpy.sound.set_volume(1.0, channel=0)
    $ renpy.sound.set_volume(1.0, channel=1)
    $ renpy.sound.set_volume(1.0, channel=2) # Usually voice audio

    # Music channels. We usually use these for background looping sound
    # effects (footsteps, wind, etc)
    $ renpy.music.set_volume(1.0, channel=3)
    $ renpy.music.set_volume(1.0, channel=4)
    $ renpy.music.set_volume(1.0, channel=5)
    $ renpy.music.set_volume(1.0, channel=6)

    # This is the main music channel; we set the volume lower than
    # the sound effects:
    $ renpy.music.set_volume(MUSIC_VOL, channel=7)

    return
