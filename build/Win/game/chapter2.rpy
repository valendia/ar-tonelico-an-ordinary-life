label chapter2:

    # Reset volume levels, in case the previous chapter left crazy levels
    call reset_volumes

    # Continuing from the previous chapter?
    if not continued:
        # Startup music and show the background scene from the end of
        # the previous chapter
        play music "music/118 Airport Paradise.ogg" fadeout 1
        scene bg town with fade
        call show_chat_border

    me "I'll grab a bite to eat."

    # Now show the mall
    scene black with fade
    scene bg mall with fade
    call show_chat_border

    n "So I walk to the convenience store. It's not far at all. It's practically on the way to school."
    n "Upon arriving, I take look at the sign for a bit. I don't know why. It just catches my eye."
    me "Firefly Corner..."
    n "I never did figure why it was named that. Maybe she sells fireflies or something."

    n " I shrug it off and enter the store."

    # Scene change
    $renpy.sound.set_volume(0.5)
    play sound "sounds/doorchimes.ogg"
    play music "music/109 A Single Breath.ogg" fadeout 1
    scene bg restaurant
    with fade

    # Move widgets into place for dialogue
    call show_chat_border

    show spica laugh behind bottom_engraving at truecenter
    with dissolve
    $ sp_name = "Woman"
    sp "Welcome. Ah, it's you. Good morning~"
    $renpy.sound.set_volume(1.0)
    me "Hey, Spica. Just grabbing a bite to eat."
    $ sp_name = "Spica"
    show spica smile at truecenter
    sp "Go right on ahead. Your business is always welcome."
    me "Thanks."
    # Ugly hack: Play a very long footsteps recording.
    # TODO: Find the "right" way to do loop sounds (embedded Python?)
    $renpy.sound.set_volume(0.5)
    play sound "sounds/manyfootsteps2.ogg"

    show spica sly at truecenter
    sp "You're looking well. Oh? You're not with Misha today?"
    me "She's awake. Just thought I'd come here while she gets dressed."
    show spica blink at truecenter
    sp "You're not skipping school again, are you?"
    me "No, I'm not. Waking up Misha to go to school kinda motivated me to not stay at home."
    me "I don't care about my future, but I won't let her waste hers."

    stop sound
    n "I arrive at the warmer."
    n "The reason I skip school every so often is because usually nothing fun happens even if I go."
    n "Just go, learn, leave."

    show spica laugh at truecenter
    sp "You're a good kid."
    n "I grab one onigiri."

    me "I wouldn't say that... Ah, that's right. I forgot to bring my bento from home... Man..."
    n "I grab two onigiris instead. I wouldn't have enough leaf if I bought an onigiri and a bento."
    $renpy.sound.set_volume(0.5)
    play sound "sounds/footsteps2.ogg"
    n "I walk over to the counter."
    stop sound

    show spica smile at truecenter
    sp "Just two today?"
    me "Yeah, my inheritance from my dead parents is getting low."
    show spica laugh at truecenter
    sp "Oh well. By the way, there's a special on Kitty Candy today. Only 200 leaf. Would you like some?"
    me "I don't care about the price. I just want a bucket's worth."
    show spica blink at truecenter
    sp "I thought so~"

    $ renpy.sound.set_volume(1)
    play sound "sounds/chaching.ogg"
    n "She rings the onigiris up."
    n "To tell you the truth, I don't really like Kitty Candy."
    $ renpy.sound.set_volume(1) # Change sound level back to normal
    n "It's just a secret trade between employees and their friends to get a good employee discount."
    n "We get a free Kitty Candy whenever we ask so the customers behind us don't get suspicious."
    n "I finish paying."
    show spica smile at truecenter
    sp "Have a nice day~ Stay in school, okay?"
    me "Yeah yeah."

    $ renpy.sound.set_volume(0.5)
    play sound "sounds/footsteps2_and_doorchime.ogg"
    n "I walk out. I hear the customer behind me asking for three Kitty Candies."
    stop sound

    # Scene change
    play music "music/108 Peaceful Song.ogg" fadeout 1
    scene bg park
    with fade
    call show_chat_border

    play sound "sounds/woodsqueek.ogg"
    n "I sit on a bench in a nearby park and chomp down my breakfast."
    n "A pretty girl passes by, but I don't really care."
    n "As I finish the last grain of rice, I throw the wrapper at a nearby trashcan underneath a tree."
    # Make swish sound softer
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/swish_drop_and_bounce.ogg"
    n "It bounces off the rim."
    me "Guess I'm too tired today..."
    # And back again:
    $ renpy.sound.set_volume(1)
    n "I stand up to pick up the trash. I mumble to myself as I throw it away properly."
    me "Littering is bad."

    n "I look at the sky. The sun feels good on my skin."
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/nature.ogg"
    n "I close my eyes. I hear the birds chirp, the wind blow, and the grass dance."
    n "The same as every other day. Nothing ever changes."

    me "Guess I'll go to school."
    n "I throw my bag over my shoulder."
    me "I won't be late. It's a ten minute commute to school. It'll probably be this way for the rest of the year."
    n "I sigh."
    me "It always is. Same ol' same o-"

    stop music fadeout 1
    $ au_name = "Voice"
    au "Watch out!"
    me "Huh?"
    n "The voice came from above my head. I look up and see-"
    me "White?"

    $ renpy.sound.set_volume(1.5) # The thud should be pretty loud
    play sound "sounds/thud.ogg"
    with vpunch
    n "Crash!"
    $ renpy.sound.set_volume(1) # Back to original volume
    play music "music/111 Tick-Tock Man.ogg"
    me "Ite-te-te-te. What is it with hitting me today!?"
    n "Then I remembered."
    me "Hey, you alright?"

    play music "music/110 Song For Counting the Memories.ogg" fadeout 1
    show aurica_sg dismay behind bottom_engraving at auricacenter
    with dissolve
    $ au_name = "Girl"
    au "Sorry, sorry. It's all my fault."
    me "What were you doing on that tree?"
    show aurica_sg sadsmile at auricacenter
    au "I was saving him."
    $ renpy.sound.set_volume(0.4) # Make cat a bit quieter
    play sound "sounds/meow.ogg"
    n "She holds out a black cat. It meows."
    me "A cat?"
    $ renpy.sound.set_volume(1) # Back to normal
    show aurica_sg smile1 at auricacenter

    au "Blackie was stuck, so I saved him."
    me "You already named it?"
    show aurica_sg smile2 at auricacenter
    au "Yeah."
    me "You named it Blackie?"
    show aurica_sg smile1 at auricacenter
    au "Yup."

    n "What a unique naming sense."
    me "Does that mean you're going to keep it?"
    show aurica_sg thoughtful1 at auricacenter
    au "I am."
    me "You might not know, but around here, some people consider black cats to be bad luck."
    show aurica_sg thoughtful2 at auricacenter
    au "Bad luck?"
    me "Bad luck."

    $ renpy.sound.set_volume(0.2)
    play sound "sounds/cloth.ogg"
    n "The girl reaches behind her head and pulls out her red ribbon."
    n "I watch her tie a bow around {nw}"
    extend "the cat's neck like a bow tie."

    show aurica_sg smile1 at auricacenter

    n "I stare for a second."
    au "Is it still bad luck?"
    me "Heh. I guess not."

    n "I stand up and hold out my hand. She takes it and I help her up."
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/stomache2.ogg"
    n "Suddenly, I hear her stomach growl."
    show aurica_sg scared at auricacenter
    au "Ah... Sorry."
    $ renpy.sound.set_volume(1)
    n "I laugh. I'm not sure if it was at her or with her."
    me "Here."
    n "I take out my onigiri."
    me "Take it."

    show aurica_sg dismay at auricacenter
    au "That's okay. I can't take something from you without giving ba-"
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/stomache2.ogg"
    n "Her stomach growls again."
    me "Take it. You'll hurt my pride if you don't."
    show aurica_sg smile1 at auricacenter
    au "Then... Thanks for the meal."
    $ renpy.sound.set_volume(1)

    n "So we sit on the bench. The girl eats the onigiri, while I give Blackie the Kitty Candy."
    n "It should be okay. It is named Kitty Candy after all."

    me "Well we know Blackie. So what's your name?"
    show aurica_sg smile2 at auricacenter
    $ au_name = "Aurica"
    au "My name's Aurica. Aurica Nestmile. I'm new here."
    me "Aurica, huh? Nice name. And I guessed you're new. I've never seen that uniform before."

    show aurica_sg sadsmile at auricacenter
    au "Yeah. I just moved in. I was just going out to get familiar with the area."
    me "That so?"
    show aurica_sg thoughtful1 at auricacenter
    au "Uhm..."
    me "What is it?"

    show aurica_sg scared at auricacenter
    au "Can you do me a favor?"
    me "Yeah?"
    show aurica_sg sadsmile at auricacenter
    au "Can you take me somewhere where I can see the city better?"
    me "A place where you can see the city better... I know just the place."

    stop music fadeout 1
    scene black
    with fade
    $ renpy.sound.set_volume(0.4)
    play sound "sounds/footsteps3.ogg"
    centered "So, I take her to the best place I know."
    stop sound

    # Scene change
    play music "music/117 The Sunset Glow Is Bright Red.ogg" fadeout 1
    scene bg sspark
    with fade

    # Move widgets into place for dialogue
    call show_chat_border

    # Make the wind sound loop for a while, with this ugly hack
    $renpy.music.set_volume(0.4, channel=6)
    $renpy.music.play("sounds/wind3.ogg", channel=6)

    me "This is Song Stone Park. It's really high. I never could take Misha here."
    au "Wow! It's so far! The view is great! I can see my house from here!"
    me "The city's name is Sol Ciel. My house is over there, in Platina Avenue."
    me "That's the school I go to, Ar Tonelico High. It's not much, but it does the job."
    me "It's a breathtaking view though."

    au "Yeah, it is. I wonder... What awaits me here. Say..."
    me "Yeah?"
    show bg cg_aurica_and_blackie with dissolve

    play music "music/220 Cirrocumulus Clouds Flowing.ogg" fadeout 1
    $renpy.music.set_volume(0.2, channel=6)
    $renpy.music.play("sounds/wind2.ogg", channel=6, fadein=1, fadeout=1) # Another looping sound hack
    n "She turns around."
    n "Her hair sways in the air as she turns, glittering in the sunlight behind her as she holds Blackie."
    n "Her smile is beautiful."
    au "Thank you."
    me "Uh... No problem."
    n "But her eyes seem... What was the word..."

    show bg sspark
    show aurica_sg shout at auricacenter behind bottom_engraving
    with dissolve
    au "Oh no! I'm late for school!"
    me "School?"
    show aurica_sg smile1 at auricacenter
    au "I'll see you later!"
    me "See me later? But you don't even know where I hang out!"
    show aurica_sg thoughtful2 at auricacenter
    au "Oh yeah."
    me "Sheesh."
    show aurica_sg smile2 at auricacenter
    au "Don't worry! Blackie's a lucky cat! We'll see each other again."
    me "Lucky cat? You just put a ribbon on him."
    show aurica_sg smile1 at auricacenter
    au "He is so a lucky cat. After all... It's because of him that I met you!"
    me "What?"

    $ renpy.sound.set_volume(0.5)
    play sound "sounds/running3.ogg"
    hide aurica_sg
    with dissolve

    stop music fadeout 2
    n "Then she leaves."
    n "I scratch my head and think to myself for a while."
    $ renpy.sound.set_volume(1) # Back to normal
    me "What did she mean by that? Well... Luck, huh? I don't think it works that way."
    me "Still, that uniform... I haven't seen that one before."
    me "What school does that belong t- oh crap! I'm gonna be late!"

    $renpy.music.stop(channel=6, fadeout=3) # Stop now
    scene black
    with fade
    centered "Ah, now I remember. That word I was looking for... Her eyes seemed... Sad."

    # Reset volume levels, to be polite to the next chapter
    call reset_volumes

    return
