init:

    # Additional misc positions used in this chapter:
    $ ch4_floorz = Zoom((640, 480), (0, 0, 640, 480), (88, 332, 204, 145), 0)
    $ ch4_sprite_pos = Position(xalign=0.9, yalign=0.05)

label chapter4:

    # Reset volume levels, in case the previous chapter left crazy levels
    call reset_volumes

    # If not continued from the previous chapter, then startup music etc
    # that should be playing at the start of the scene:
    if not continued:
        play music "music/138 Pulse ~Elemia Arrange~.ogg"
        scene bg classroom
        with fade
        call show_chat_border

    n "In my head I run through the list of places that Aurica could possibly be."
    me "I think at the end of her note there was an 'S'... Hmm."
    me "School... Sing... Student council?"
    me "Student council room! I hope so."

    $ renpy.sound.set_volume(0.5)
    play sound "sounds/running3.ogg"
    play music "music/201 The Sound of the Bell, the Voice of Prayer.ogg" fadeout 1
    scene bg school_corridoor
    with ComposeTransition(Dissolve(0.5), after=CropMove(0.5, "wipeleft"))
    call show_chat_border
    n "So I run to the student council room."
    n "I've never been there before, but there's a first time for everything."

    show bg school_corridoor at Zoom((640, 480), (0, 0, 640, 480), (499, 184, 139, 153), 0)
    with ComposeTransition(Dissolve(0.4), after=ImageDissolve(im.Tile("blindstile.png"), 0.4, 8))
    n "I come to the student council door."
    n "As I reach for the door, I begin to wonder."
    me "Why would she want to come here?"
    n "Oh well. I'm here, so I'll go all the way."
    $ renpy.sound.set_volume(1)
    play sound "sounds/door2.ogg"

    play music "music/138 Pulse ~Elemia Arrange~.ogg" fadeout 1
    scene bg classroom2
    with ComposeTransition(Dissolve(0.5), after=ImageDissolve(im.Tile("dissolves/ltorsmooth.png"), 0.5, 8, reverse=True))
    call show_chat_border
    n "I open the door and enter the room."
    me "Hello?"
    $ ra_name = "Man"
    show radolf smile behind bottom_engraving at truecenter with dissolve
    ra "Good morning. Ah, long time no see."
    me "Radolf-senpai. It's been a while."
    $ ra_name = "Radolf"
    show radolf laugh at truecenter with dissolve
    ra "I told you, please, just call me Radolf. I'm not used to being called senpai."
    me "Okay."
    show radolf smile at truecenter with dissolve
    ra "What's the matter?"
    me "I was just looking for somebody. It looks like she's not here."
    n "I sigh."

    me "I was so sure, too. Now I have no idea where she is."
    ra "Hahaha. Come, sit. Tell me about it."
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/woodsqueek.ogg"

    hide radolf with dissolve
    show bg classroom2 at Zoom((640, 480), (0, 0, 640, 480), (22, 96, 595, 375), 0)
    with ComposeTransition(Dissolve(0.5), after=CropMove(0.5, "irisout"))
    show radolf smile behind bottom_engraving at truecenter with dissolve
    n "I sit down on a nearby chair."
    me "Hope she doesn't get mad at me."
    ra "Who might this be?"
    me "A girl named Aurica."
    me "She said to meet her at a place that starts with S. I couldn't read the whole letter."
    show radolf thoughtful1 at truecenter
    ra "Passing notes in class, are we?"
    me "So sue me."
    show radolf laugh at truecenter with dissolve
    ra "Hahaha. You haven't changed at all, I see. Still the smart mouth that we all know and love."
    me "We?"
    show radolf thoughtful2 at truecenter with dissolve
    ra "You remember. Our group that we did everything with. Adventures, mischief, and the miscellaneous."
    me "Ah. That was two years ago."
    me "Since coming to high school, we've been split apart."
    show radolf smile at truecenter with dissolve
    ra "Good times."
    show radolf dismay at truecenter with dissolve
    ra "You, myself, Misha, Jack..."
    me "You can say his name."
    show lyner_face at itempos with dissolve
    me "Lyner."

    hide lyner_face
    show radolf frown at truecenter
    with dissolve
    ra "You had it the hardest. You and Lyner have grown up together since you were babies."
    me "Those days are over. They've been over since a year ago."

    play sound "sounds/sleipnir_roar.ogg"
    show radolf thoughtful2 at truecenter
    show sleipnir_face at itempos
    with dissolve
    ra "The Sleipnir incident. It took a toll on us all."
    hide sleipnir_face with dissolve
    me "Still, Lyner didn't have to sacrifice himself. We all could have made it out together."
    me "That idiot..."
    show radolf dismay at truecenter with dissolve
    ra "You can cry if you want."
    me "I cried all my tears on that day. The rest is just boring."
    show radolf smile at truecenter with dissolve
    ra "The rest?"
    me "Lyner was a great guy to be with. He was unpredictible. There was never a dull moment with him."
    me "Now that he's gone, every day's just one boring day after another."
    show radolf laugh at truecenter with dissolve
    ra "Hahaha. Look at us. Grieving over the past. This is not like us."
    me "Haha. You're right. We need more subjects to talk about, rather than limiting ourselves to the past."
    show radolf smile at truecenter with dissolve
    ra "The sky's the limit."
    stop music fadeout 2
    me "Yeah, the sk-"
    $ renpy.music.play("music/204 Go for It!.ogg", loop=False)
    me "That's it!"
    show radolf dismay at truecenter
    ra "What?"
    me "Thanks, Radolf! I gotta get going!"

    $ renpy.sound.set_volume(0.5, channel=6)
    play music "sounds/running2.ogg" channel 6
    play music "music/201 The Sound of the Bell, the Voice of Prayer.ogg"
    scene bg school_corridoor
    with ComposeTransition(Dissolve(0.5), after=ImageDissolve(im.Tile("dissolves/ltorsmooth.png"), 0.5, 8))
    call show_chat_border
    n "I run out the door."

    show bg classroom2
    show radolf smile behind bottom_engraving at truecenter
    with dissolve
    ra "Glad to be of assistance."
    show bg school_corridoor
    hide radolf
    with dissolve
    n "I run toward the school staircase."
    n "Why didn't I think of this before? The sky! 'S'! The school rooftop!"

    show bg school_corridoor at Zoom((640, 480), (0, 0, 640, 480), (477, 191, 82, 56), 2.0)
    with Pause(2.0)
    scene bg school_stairs
    with ComposeTransition(Dissolve(0.4), before=ImageDissolve(im.Tile("blindstile.png"), 0.4, 8, reverse=True))
    call show_chat_border
    n "I run towards the stairs leading to the roof."
    play music "music/111 Tick-Tock Man.ogg" fadeout 2
    n "After about ten steps, my legs give out on me{nw}"
    stop music channel 6
    play sound "sounds/running_trip.ogg"
    extend " and I trip."
    me "Shoot!"
    play sound "sounds/swish.ogg"
    n "I try to grab onto anything I can."
    $ renpy.sound.set_volume(1.2)
    play sound "sounds/thud.ogg"

    # Show part of meimei on the screen
    image mm_big = im.Rotozoom(im.Scale("meimei/meimei_6.png", 480, 640), 270, 1.0)
    show bg school_stairs at Zoom((640, 480), (0, 0, 640, 480), (286, 232, 352, 247), 0)
    show mm_big behind bottom_engraving at Position(xpos=-2, ypos=200, xanchor=0, yanchor=0)
    with dissolve
    n "{b}Crash!{/b}"
    $ renpy.sound.set_volume(1)

    n "I land on something soft."
    me "Not again... Ouch, what did I hi-"
    me "AAAAAH!"
    n "I look forward while on the ground."
    n "I stare into red and white stripes."
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/capeflap.ogg"
    hide mm_big
    show bg school_stairs at Zoom((640, 480), (286, 232, 352, 247), (0, 0, 640, 480), 0.3)
    with Pause(0.3)
    n "I immediately jump back."
    n "I don't want any misunderstandings!"
    me "Sorry, I wasn't looking where I was going!"
    n "The girl lies there on the ground."
    me "Huh... She's not moving."
    me "Not... Moving..."
    me "Could she be..."
    me "No..."
    me "DEAD!?"
    me "Crap! I killed her!"
    me "What am I going to do!?"

    stop music fadeout 1
    $ sh_name = "Shurelia"
    show shurelia_sg frown behind bottom_engraving at shurcenter with dissolve
    sh "You..."
    n "She is right behind me."
    me "Shurelia! I didn't mean t-"
    play music "music/220 War of the Panties.ogg" fadeout 1
    $ renpy.sound.set_volume(2.0)
    play sound "sounds/punch.ogg"
    show bg school_stairs
    with hpunch

    n "{color=#f00}{b}WHAM!{/b}{/color}"
    n "I don't get to finish. Shurelia slaps me right across the face."
    n "A resounding smack echoes across the room."

    show shurelia_sg shout at shurcenter
    sh "Pervert! How dare you get your hands on Meimei-chan!"
    me "Meimei-chan!? We have more important things to worry about than tha-"
    show meimei smile at meiright behind shurelia_sg with moveinbottom

    mei "Ooh? Mornin' Shurelia."
    me "Huh? She was just... Sleeping?"
    show shurelia_sg frown at shurcenter
    sh "No mercy..."
    n "I can feel her death glare on me."
    n "Shurelia is not someone you want to make mad."
    me "Uh... I'm glad to see you're okay."
    me "Well... Gotta go!"
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/running3.ogg"
    hide shurelia_sg
    hide meimei
    with dissolve
    show bg school_stairs at Zoom((640, 480), (0, 0, 640, 480), (403, 3, 234, 137), 0.5)
    with Pause(0.5)
    n "I dash off as quickly as I can. My legs can't afford to give out this time."
    sh "Get back here! Sheesh..."
    $ renpy.sound.set_volume(1)

    play music "music/120 Leave It Be.ogg" fadeout 1
    scene bg school_stairs
    with ComposeTransition(Dissolve(0.5), after=ImageDissolve(im.Tile("dissolves/id_circlewipe.png"), 0.5, 8, reverse=True))
    call show_chat_border
    show shurelia_sgh frown behind bottom_engraving at shurleft
    show meimei smile behind bottom_engraving at meiright
    with dissolve
    mei "What happened, Shurelia?"
    show shurelia_sgh shout behind bottom_engraving at shurleft
    sh "That letch pushed you to the ground and took advantage of you."
    show meimei tired at meiright
    mei "Shurelia. You know he wouldn't do that. He's not that type of person."
    mei "You know it was an accident."
    show shurelia_sgh frown at shurleft
    sh "Well... Still."
    show meimei smile at meiright
    mei "Could it be that you're jealous?"
    show shurelia_sgh cryshout at shurleft
    sh "Do-do-don't say things like that!"
    show meimei yawn_hand at meiright with dissolve
    mei "*yawn* Okay, okay. I'm going back to class. So sleepy..."
    hide meimei
    with moveoutright
    show shurelia_sgh thoughtful1 at shurleft with dissolve
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/heartbeat.ogg"
    sh "Why does my heart keep beating so much..."

    play music "music/201 The Sound of the Bell, the Voice of Prayer.ogg" fadeout 1
    scene black with fade
    scene bg school_stairs at Zoom((640, 480), (0, 0, 640, 480), (403, 3, 234, 137), 0)
    with ComposeTransition(Dissolve(0.5), before=ImageDissolve(im.Tile("dissolves/id_circlewipe.png"), 0.5, 8))
    call show_chat_border
    n "Finally, I make it to the door."
    n "It's never locked. No one ever comes up here anyway."
    me "Well... She wouldn't know that."

    play music "music/121 Screaming Seagull.ogg" fadeout 1
    $ renpy.sound.set_volume(1.0)
    play sound "sounds/door2.ogg"
    $ renpy.music.set_volume(0.6, channel=6) # Make wind softer
    play music "sounds/wind3.ogg" channel 6 fadein 2
    scene black with fade
    scene bg school_rooftop
    with ComposeTransition(Dissolve(0.5), before=CropMove(0.5, "wipeleft"))
    call show_chat_border
    n "I open the door."
    $ renpy.music.set_volume(1.2, channel=6) # Make the wind louder
    n "The wind rushes in quickly and sweeps the hair off my face."
    show bg school_rooftop
    with Fade(0.5, 0.1, 2.0, color='#fff')
    n "I shield my eyes from the sudden glare of the sun as I step out."

    show bg sun_and_clouds
    with dissolve
    n "As my eyes adjust, I look into the sky."
    me "Wooow."
    # Fade out wind after the 5th text box screens, Dexas says it's distracting
    stop music fadeout 2 channel 6
    $ renpy.music.stop

    n "The sky is vast and huge."
    n "I never thought to look at the sky from here."
    n "There aren't as many trees in the way, so everything is clearly visible."
    n "The sun is surrounded by blue sky and circled by clusters of clouds."
    n "It's breathtaking."
    n "I admire the sight before turning to a voice from behind me."
    play music "music/110 Song For Counting the Memories.ogg" fadeout 1

    show bg school_rooftop
    with ComposeTransition(Dissolve(1.0), before=ImageDissolve(im.Tile("dissolves/zigzag.png"), 1.0, 8, reverse=True))
    show aurica_sg laugh behind bottom_engraving at auricacenter with dissolve
    $ au_name = "Aurica"
    au "Hey! I knew you'd find me!"
    n "She is sitting on the level above the entrance door."

    #--------

    show aurica_sg smile1 at auricacenter with dissolve
    au "So how did you find me?"
    me "I just remembered what you said earlier today. I figured you still wanted to see the world better."
    n "Aurica and I sit on the upper levels of the roof."
    n "We lean on the water tower and stare at the vast opening that is our school yard,"
    n "riddled with students playing with each other."
    n "Aurica is playing with Blackie."
    show aurica_sg smile2 at auricacenter
    au "I'm surprised you remembered."
    me "Guess Blackie really is a lucky cat."
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/meow.ogg"
    "Blackie" "Nya."
    me "So, what did you want to talk about?"
    $ renpy.sound.set_volume(1)
    show aurica_sg smile1 at auricacenter
    au "Nothing special. I just wanted to have some company."
    me "I see."
    n "A few silent moments pass."
    n "I don't know why, but the silence is fun."
    n "Guess it's because this is the first time I've ever hung out with someone without talking."
    n "I don't want even this to become boring, so I break the silence."

    me "So, what's the story with the uniform?"
    show aurica_sg thoughtful2 at auricacenter
    au "This?"
    me "Yeah. It's not this school's."
    me "Heck, it's not even from this country, from what I can tell."
    show aurica_sg sadsmile at auricacenter
    au "Well...."
    n "Silence again. Aurica looks kinda sad."
    n "I begin to regret asking her."
    play music "music/127 The Other Side of Memories.ogg" fadeout 1
    show aurica_sg thoughtful1 at auricacenter
    au "I'm a foster child."
    me "What?"
    show aurica_sg sad at auricacenter
    au "My parents died in an accident, so I've been moving from foster parent to foster parent. "
    show aurica_sg thoughtful1 at auricacenter
    au "That also means that I have to switch schools."
    show aurica_sg thoughtful2 at auricacenter
    au "I didn't want to trouble my foster parents to buy a new uniform for every school,"
    show aurica_sg thoughtful1 at auricacenter
    au "so I kept this one with me as I moved."
    me "Whoa... Wait, why do you keep switching foster parents?"
    show aurica_sg scared at auricacenter
    au "It's because I'm useless."
    me "What was that?"
    show aurica_sg dismay at auricacenter
    au "I'm useless. I can't do anything right."
    show aurica_sg scared at auricacenter
    au "I always cause trouble for my parents until they don't want me anymore."
    show aurica_sg sad at auricacenter
    au "The foster agency is also getting fed up with me."
    show aurica_sg dismay at auricacenter
    au "I... I can't do anything."

    me "You idiot!"
    show aurica_sg scared at auricacenter
    au "Huh?"
    me "Don't talk like that. You're not useless. You have a kind heart that wants to help others."
    me "You have a heart that craves help {i}from{/i} others. That is not useless at all. You are a person."
    me "Those foster parents don't know what they're thinking. They don't know anything."
    me "Aurica, if you just put your mind into it, you can do anything."
    me "If you feel down, just yell out to yourself, 'There is no one in this world who is useless'."
    me "If anyone else says otherwise, I'll send them to kingdom come!"

    play music "music/220 Cirrocumulus Clouds Flowing.ogg" fadeout 1
    show aurica_sg sadsmile at auricacenter
    au "T... Thank you..."
    show aurica_sg smile2 at auricacenter
    au "That was the kindest thing anyone has ever said to me."
    me "It better be."
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/thud.ogg"
    n "I sit back down. I don't even know when I got up. I was too in the zone."
    $ renpy.sound.set_volume(1)
    me "A smile is better on you, after all."

    show aurica_sg smile1 at auricacenter
    au "I can see why Shurelia and Misha like you so much."
    me "Huh? Speak up."

    play music "music/110 Song For Counting the Memories.ogg" fadeout 1
    show aurica_sg thoughtful2 at auricacenter
    au "Oh, that's right! Shurelia told me that you quit clubs after you join them. Why is that?"
    me "Oh, that? Well... Long story."
    show aurica_sg smile1 at auricacenter
    au "I have all the time in the world. Or until the bell rings, anyway."
    me "To make it simple, I'm bored of them. Every day's the same, you know?"
    me "I wake up, go to school, go home, eat, do homework, sleep, and then repeat."
    me "That's just boring, you know?"
    me "I like to try new things, so I decided to try out for some clubs."
    me "At first, I thought I'd try out the debate team to try and argue."
    me "But that got boring after a week since no one knew how to debate. Or maybe I just got too good."
    show aurica_sg smile2 at auricacenter
    n "Aurica smiles."
    me "I thought it was boring because all we did was stand there and yell big words in a fancy way."
    me "So I thought I'd go physical and joined the track team."
    me "But no one's been able to keep up with me since the beginning."
    me "To be honest, everything comes easily to me."
    show aurica_sg shout at auricacenter
    au "But that's amazing!"
    me "Not really."
    me "{size=-2}I don't get the satisfaction of finally being able to get something right after training if it just comes naturally.{/size}"
    me "It's boring."
    me "After the track team, I remembered the saying, 'no pain no gain',"
    me "and so I thought about joining something painful to gain excitement."
    me "I joined the wrestling team. That got easy too... But I did gain endurance like a madman."
    show aurica_sg thoughtful2 at auricacenter
    au "Really?"
    play music "music/111 Tick-Tock Man.ogg" fadeout 1

    me "Yeah. Even now, I'm still fit as a fiddle!"
    play sound "sounds/swishes.ogg"
    n "I jump up and throw a couple of punches."
    me "See?"
    play sound "sounds/swoosh.ogg"
    n "I kick the air."
    stop music
    $ renpy.sound.set_volume(1.0)
    show aurica_sg dismay at auricacenter
    play sound "sounds/thud5.ogg"

    show bg school_rooftop
    with vpunch
    n "Suddenly, a shock runs through my body."
    $ renpy.sound.set_volume(1)
    play music "music/210 Vidohunir.ogg" fadeout 1

    image bg school_rooftop_red = im.Recolor("backgrounds/school_rooftop.png", 255, 80, 80, 255)
    show bg school_rooftop_red
    with dissolve
    me "Ugh!"
    $ renpy.sound.set_volume(1)
    play sound "sounds/thud.ogg"

    hide aurica_sg
    show bg school_rooftop_red at ch4_floorz
    with dissolve
    n "I collapse to the ground and hold my chest in pain."
    $ renpy.sound.set_volume(1)
    me "Not now..."
    n "The pain is finally taking its toll."
    n "The adrenaline that was keeping me from feeling pain has finally worn off, and I'm now reaping what I sowed."
    play sound "sounds/heartbeat.ogg"

    # Flash a few times
    show bg school_rooftop_red at ch4_floorz with redflash
    show bg school_rooftop_red at ch4_floorz with redflash
    show bg school_rooftop_red at ch4_floorz with redflash

    n "My eyes begin to flash red."
    show bg school_rooftop_red
    show aurica_sg shout behind bottom_engraving at auricacenter
    with dissolve
    au "What's happening? Are you alright!?"

    hide aurica_sg
    show bg school_rooftop_red at ch4_floorz
    with dissolve
    me "It's... Nothing. Just a couple of broken ribs."

    play sound "sounds/heartbeat.ogg"
    # Flash a few times
    show bg school_rooftop_red at ch4_floorz with redflash
    show bg school_rooftop_red at ch4_floorz with redflash
    show bg school_rooftop_red at ch4_floorz with redflash
    n "The pain surges again."
    $ renpy.sound.set_volume(1)
    me "And maybe some internal bleeding."
    show bg school_rooftop_red at ch4_floorz
    with vpunch
    me "AAAAAAAARGH!"

    show aurica_sg dismay behind bottom_engraving at auricacenter
    show bg school_rooftop_red
    with dissolve
    au "No! Don't die! Please... don't leave me too!"
    n "{size=-6}{color=#0000ff}Sing!{/color}{/size}"
    show aurica_sg scared at auricacenter
    au "What? Who's there!?"
    n "{color=#00fffc}Sing!{/color}"
    show aurica_sg thoughtful1 at auricacenter
    au "... Okay."

    show aurica_sg thoughtful2 at auricacenter
    n "Aurica clasps her hands together."
    $ renpy.music.set_volume(1.0, channel=6)
    play music "sounds/wind3.ogg" channel 6
    n "The wind around us picks up force."
    n "For a moment, it seems as if it will whisk us away,"
    $ renpy.music.set_volume(0.5, channel=6)
    n "but the wind remains calm, as though it's supporting Aurica."
    play music "music/Ar Tonelico - EXEC_HYMME_MOISKYRIE.ogg" fadeout 1
    show aurica_sg smile2 at auricacenter

    $ hymmnos = "Wee quel ra yorr yehar mea vl zeeth ture fee"
    $ english = "I'm honestly relieved that you released me from\nthe chains that bound me to the wind"
    show hymmnos_song at truecenter with dissolve

    n "She begins to sing."
    hide aurica_sg
    hide hymmnos_song
    show bg school_rooftop_red at ch4_floorz
    with redflash

    me "Aurica?"
    show bg school_rooftop_red
    show aurica_sg smile2 behind bottom_engraving at auricacenter
    with redflash

    $ hymmnos = "Was yant ga suwant yor art noes van nedle swant"
    $ english = "I'm very afraid, but I'll save you even as I need to be saved"

    show aurica_blue behind aurica_sg at auricacenter
    show aurica_sprite behind bottom_engraving at ch4_sprite_pos
    show hymmnos_song at truecenter
    with dissolve

    show aurica_sprite_magic behind bottom_engraving at ch4_sprite_pos
    $ renpy.sound.set_volume(1.3)
    play sound "sounds/harp1.ogg"
    n "Aurica begins to glow a sudden blue. It is the most beautiful blue I have ever seen."
    $ renpy.sound.set_volume(1.0)

    hide hymmnos_song with dissolve
    $ hymmnos = "Tim gyas melifan na chs ides"
    $ english = "This misstep of history shall not become our past"
    show hymmnos_song at truecenter with dissolve

    n "Her clothes and hair flutter in the wind as though she were an angel,"
    n "descending to bestow a new life upon me."

    hide hymmnos_song with dissolve
    $ hymmnos = "Was ki ra gott faiy kierre gyaje sos yor"
    $ english = "I'll concentrate on removing this transient\nmoment of error, for your sake"
    show hymmnos_song at truecenter with dissolve

    n "The words that come from her are words that I do not recognize."
    show bg school_rooftop_red at ch4_floorz
    hide aurica_sg
    hide aurica_blue
    hide aurica_sprite
    hide aurica_sprite_magic
    hide hymmnos_song
    with redflash
    me "Is this... Hymmnos?"
    n "My body begins to feel fuzzy inside."

    show bg school_rooftop_red
    show aurica_sg smile1 behind bottom_engraving at auricacenter
    show aurica_blue behind aurica_sg at auricacenter
    show aurica_sprite behind bottom_engraving at ch4_sprite_pos
    show aurica_sprite_magic behind bottom_engraving at ch4_sprite_pos
    with redflash
    $ renpy.sound.set_volume(1.3)
    play sound "sounds/harp2.ogg"
    n "Little by little, my pain subsides as the red flashes gradually fade away."
    $ renpy.sound.set_volume(1.0)
    show bg school_rooftop
    hide aurica_sprite_magic
    with Dissolve(3.0)
    n "The sky becomes blue and white again."
    n "My body feels as good as new."

    play music "music/220 Cirrocumulus Clouds Flowing.ogg" fadeout 1

    # Dexas doesn't like long distracting wind noises, so stop the wind here,
    # instead of playing a more gentle noise
    stop music fadeout 2 channel 6
    hide aurica_blue
    hide aurica_sprite
    with dissolve
    n "Aurica finishes singing, and her hair falls back into place as the blue aura fades."
    me "Aurica... What did you do?"
    show aurica_sg thoughtful2 at auricacenter
    au "I... I don't know. The words just came into my head and I just felt I had to sing it. "
    show aurica_sg smile1 at auricacenter
    au "I... I thought it would save you. Did I do good?"
    me "Did good?! You did great! I feel like a new man!"
    $ renpy.sound.set_volume(0.8)
    play sound "sounds/capeflap.ogg"
    n "I jump up and down{nw}"
    queue sound "sounds/capeflap.ogg"
    extend " in great enthusiasm. It really is a great feeling."
    show aurica_sg laugh at auricacenter with dissolve
    n "Aurica and I laugh as I act silly."
    me "Hahahaha! I feel great! I can run another maratho-whoa!"

    play music "music/111 Tick-Tock Man.ogg" fadeout 1
    play sound "sounds/running_trip.ogg"
    n "I trip on my own feet. This pattern is becoming very familiar."

    show aurica_sg scared at auricacenter
    show bg school_rooftop at Zoom((640, 480), (0, 0, 640, 480), (76, 130, 498, 302), 0.4)
    with Pause(0.4)
    n "I fall toward Aurica."
    show aurica_sg dismay at auricacenter
    au "Eep!"
    $ renpy.sound.set_volume(1.2)
    play sound "sounds/thud.ogg"
    show bg school_rooftop at Zoom((640, 480), (76, 130, 498, 302), (218, 352, 180, 123), 0.2)
    show aurica_sg shout at auricacenter
    with vpunch
    n "{b}Crash!{/b}"
    $ renpy.sound.set_volume(1)

    n "Aaah. You oka-......"
    n "I find myself at a loss for words. By reflex I tried to grab something, but not this."
    stop music fadeout 1
    au "............"
    play music "music/120 Leave It Be.ogg" fadeout 1
    n "Aurica is on the ground, her leg wrapped around my neck, {nw}"
    $ renpy.sound.set_volume(1.0)
    play sound "sounds/heartbeat.ogg"
    extend "my face a breath away from what was between"
    n "her legs, my right hand still on her left breast."
    $ renpy.sound.set_volume(1)
    n "I don't need to look to know. Her breast is a familiar feeling."
    n "It is marshmallow hell, waiting to give me a trip from which I will never return."
    play sound "sounds/schoolbell.ogg"
    n "The bell rings, snapping me out of my state of shock."
    me "Ah-ah-ah"
    hide aurica_sg
    play sound "sounds/capeflap.ogg"
    show bg school_rooftop at Zoom((640, 480), (218, 352, 180, 123), (0, 0, 640, 480), 0.2)
    n "I sat straight up."
    me "I'm sorry! I didn't mean to! It was an honest mistake..."

    $ renpy.music.set_volume(0.4, channel=5)
    play music "sounds/footsteps3.ogg" channel 5
    show aurica_sg meansmile behind bottom_engraving at auricacenter with moveinbottom
    $renpy.pause(0.6)
    hide aurica_sg with easeoutleft
    n "She had already stood and was now walking toward the door."
    n "She picks up Blackie{nw}"
    $ renpy.sound.set_volume(0.5, channel=1)
    play sound "sounds/meow.ogg" channel 1
    extend " and starts down the stairs."

    me "Aurica!"
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/running3.ogg"
    show bg school_rooftop at Zoom((640, 480), (0, 0, 640, 480), (3, 117, 355, 249), 1.0)
    with Pause(1.0)
    show aurica_sg meansmile behind bottom_engraving at auricaleft with dissolve
    n "I run after her and {nw}"
    stop music channel 5 # Stop Aurica walking sound
    $ renpy.sound.set_volume(1)
    play sound "sounds/thud.ogg"
    extend "grab her shoulder. I don't want a misunderstanding."
    me "Aurica, please believe me-"
    $ renpy.sound.set_volume(2.0)
    stop music
    stop music channel 5
    play sound "sounds/punch.ogg"
    show bg school_rooftop at Zoom((640, 480), (0, 0, 640, 480), (3, 117, 355, 249), 0) with hpunch
    show aurica_sg meansmile behind bottom_engraving at auricaleft
    n "{b}SMACK!{/b}"
    play music "music/208 Heave-Ho Half-Off Sale!.ogg" fadeout 1
    n "She slaps me across the face."
    $ renpy.music.set_volume(0.4, channel=5)
    play music "sounds/footsteps3.ogg" channel 5
    hide aurica_sg with easeoutleft
    n "I am stunned again as she walks away."
    stop music fadeout 1 channel 5
    me "... Foul is fair, and fair is foul."
    n "I sigh to myself."

    #---------

    stop music fadeout 1 channel 6 # Stop the wind now
    play music "music/138 Pulse ~Elemia Arrange~.ogg" fadeout 1
    scene black with fade
    scene bg classroom
    with ComposeTransition(Dissolve(1.0), after=ImageDissolve(im.Tile("dissolves/twirl2.png"), 1.0, 8, reverse=True))
    call show_chat_border
    show aurica_sgh notimpressed behind bottom_engraving at auricacenter with dissolve
    n "We don't talk for the rest of the day."
    n "She just stares at the board, not even taking a single, sidelong glance in my direction."
    n "No notes, no glances. I feel really bad, but I don't want to make things worse."
    n "I just keep thinking that she'll cool down and realize it was an accident soon."
    n "Time heals all wounds, right? I really do hope so."

    #---------

    play sound "sounds/schoolbell.ogg"
    n "The last bell rings, and I try to talk to Aurica again."
    $ renpy.sound.set_volume(0.5, channel=1)
    play sound "sounds/desks.ogg" channel 1
    hide aurica_sgh with easeoutright
    n "She does not hesitate to get up and {nw}"
    $renpy.music.queue(["sounds/footsteps2.ogg"], channel=3, loop=False)
    extend "walk out the room without even looking my way."
    $ renpy.sound.set_volume(1, channel=1)

    play music "music/122 Filthy Shop Work.ogg" fadeout 1
    $ ja_name = "Jack"
    play sound "sounds/whistle.ogg"
    show jackh smile behind bottom_engraving at truecenter with dissolve
    ja "*whistles* You must have done something reaaaaally bad this time."
    me "... I'm going home."
    ja "Suit yourself."

    #----

    n "I don't care about detention today. I just want to get this day over with."
    scene black
    with ComposeTransition(Dissolve(1.0), after=ImageDissolve(im.Tile("dissolves/id_circlewipe.png"), 1.0, 8))
    play music "music/118 Airport Paradise.ogg" fadeout 1
    scene bg street
    with ComposeTransition(Dissolve(1.0), before=ImageDissolve(im.Tile("dissolves/id_circlewipe.png"), 1.0, 8))
    call show_chat_border
    $ renpy.music.set_volume(0.4, channel=6)
    play music "sounds/footsteps3.ogg" channel 6
    n "I walk home, thinking about today's events. It just keeps bothering me. Was it really my fault?"
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/heartbeat.ogg"
    n "Though I admit I did like that position..."
    stop sound
    $ renpy.sound.set_volume(1)
    n "What am I thinking!? This isn't right."
    play music "music/108 Peaceful Song.ogg" fadeout 1
    scene bg street2
    with ComposeTransition(Dissolve(1.0), before=blinds)
    call show_chat_border
    n "I sigh."
    stop music channel 6
    n "I snap out of my thoughts and look around. This area doesn't look familiar at all."
    me "Great... Getting lost on my own train of thought."

    scene black
    with ComposeTransition(Dissolve(0.5), after=ImageDissolve(im.Tile("dissolves/eyeopen.png"), 0.5, 8, reverse=True))
    centered "I close my eyes and sigh to myself, toward the sky."
    $ renpy.sound.set_volume(0.5)
    play sound "sounds/nature.ogg"
    centered "I decide to listen to the chirping birds to calm myself."
    centered "It is rather quiet."

    stop sound fadeout 1
    play music "music/10 EXEC_HARMONIUS_FYUSION.ogg" fadeout 1
    centered "Then I hear a sound. It is in a language I had not heard until today."
    centered "Hymmnos? Aurica?"

    scene bg street2
    with ComposeTransition(Dissolve(0.5), after=ImageDissolve(im.Tile("dissolves/eyeopen.png"), 0.5, 8))
    call show_chat_border
    n "Maybe I'm near her house. {nw}"
    $ renpy.music.set_volume(0.4, channel=6)
    play music "sounds/footsteps3.ogg" channel 6
    extend "I follow the sound."

    n "It sounds similar, but it's also slightly different from what I heard from Aurica."
    n "The song from earlier healed my wounds. This one makes me feel... Right, yet... Sad."

    n "I realize it is a song."
    scene bg shed
    with ComposeTransition(Dissolve(0.8), after=ImageDissolve(im.Tile("dissolves/id_circlewipe.png"), 0.8, 8))
    call show_chat_border
    stop music channel 6

    n "The song leads me to a backyard."
    show mir_at1 sad behind bottom_engraving at mir_at1_center with dissolve
    n "There is a girl there. She is singing with her eyes closed while facing the sky."

    $ mir_name = "Girl"

    $ hymmnos = "Was ki ra revatail whou Metafalica crushue, Mir"
    $ english = "Reyvateil who crafted the Song of Hope, Mir"
    show hymmnos_song at truecenter with dissolve
    mir "{i}{font=fonts/hymmnos.ttf}%(hymmnos)s{/font}{/i}"

    hide hymmnos_song with dissolve
    $ hymmnos = "Presia kiafa sarla mea"
    $ english = "Please hear my song."
    show hymmnos_song at truecenter with dissolve
    mir "{i}{font=fonts/hymmnos.ttf}%(hymmnos)s{/font}{/i}"

    hide hymmnos_song with dissolve
    $ hymmnos = "Faura yerwe murfan anw sol ciel"
    $ english = "Little birds chirp their feelings to the world"
    show hymmnos_song at truecenter with dissolve
    mir "{i}{font=fonts/hymmnos.ttf}%(hymmnos)s{/font}{/i}"

    hide hymmnos_song with dissolve
    $ hymmnos = "Faura sonwe murfan anw sol ciel ee...."
    $ english = "Little birds sing their feelings to the people."
    show hymmnos_song at truecenter with dissolve
    mir "{i}{font=fonts/hymmnos.ttf}%(hymmnos)s{/font}{/i}"
    hide hymmnos_song with dissolve

    n "It is so... Beautiful."
    hide hymmnos_song with dissolve
    n "I take another step to get closer, and my {nw}"
    play sound "sounds/splintering.ogg"
    extend "foot lands on a branch."
    stop music fadeout 1
    show mir_at1 angry at mir_at1_center with dissolve
    n "She stops singing and looks at me."
    me "Uhm... That was a beautiful song."
    play music "music/212 A Ruthless Blade.ogg" fadeout 1
    show mir_at1 smile at mir_at1_center with dissolve
    mir "... It was?"
    me "Did you make it yourself?"
    mir "I did."
    me "That's amazing. What's it called?"
    show mir_at1 sad at mir_at1_center with dissolve
    mir "... Exec...... Harmonious."
    me "It's a good song."
    me "Although it is kinda sad, I can feel that you put a lot of love into it."
    show mir_at1 smile at mir_at1_center with dissolve
    mir "You can tell?"
    me "Yeah."
    mir "..."

    me "What's your name?"
    mir "... Mir."
    me "Mir, huh? It suits you."
    $ mir_name = "Mir"
    show mir_at1 sad at mir_at1_center with dissolve
    mir "I don't like it."
    me "You don't? Why not?"
    mir "...... It sounds like mule."
    me "Ahahaha..."
    n "I laugh nervously. She's a unique kind of character."
    me "Well then... How about you get a nickname?"
    show mir_at1 angry at mir_at1_center with dissolve
    mir "A nickname?"
    me "Yeah. Hmm. A suitable nickname..."
    me "Say. How about Jakuri?"
    show mir_at1 sad at mir_at1_center with dissolve
    mir "... Jakuri."
    me "Yeah. That sounds about right."
    me "Jakuri. Yeah, that suits you perfectly."
    show mir_at1 smile at mir_at1_center with dissolve
    mir "... Okay. You can call me that."

    me "Really? That's great! Glad I could be of help."
    me "Oh yeah. By the way, I'm lost."
    me "Can you point the way to Ar Tonelico High School?"
    show mir_at1 sad at mir_at1_center with dissolve
    mir "Ar Tonelico High School?"
    me "Yeah. That's the school I go to."
    me "I got lost, and I only know how to get to my house from there."
    me "Do you know the way?"
    n "Jakuri looks to the ground."
    n "She shifts her head to the right and points in the direction of the school."
    me "Thanks! I hope we meet again!"

    $ renpy.music.set_volume(0.4, channel=6)
    play music "sounds/footsteps3.ogg" channel 6
    scene bg street2
    with ComposeTransition(Dissolve(0.8), after=ImageDissolve(im.Tile("dissolves/id_circlewipe.png"), 0.8, 8, reverse=True))
    call show_chat_border

    play music "music/108 Peaceful Song.ogg" fadeout 1
    n "I go in that direction."
    n "I think Jakuri is wearing a really really low shirt. I really do."
    n "I can't tell, since the hedges are in the way."
    n "But as I pass through the fence and the hedges go out of the way, I realize that she is indeed,"
    play music "music/120 Leave It Be.ogg" fadeout 1
    n "wearing no clothes."
    stop music channel 6
    $ renpy.sound.set_volume(2.0)
    play sound "sounds/thud_metal.ogg"
    show bg street2
    with vpunch
    n "{b}Crash!{/b}"

    scene black
    with ComposeTransition(Dissolve(0.2), after=ImageDissolve(im.Tile("dissolves/eyeopen.png"), 0.2, 8, reverse=True))
    centered "Distracted, I run into a telephone pole and black out."

    #-----------

    # Ren'Py doesn't support showing Zoomed images with alpha, so do it manually:
    image streetz1 = im.Scale(im.Crop("backgrounds/street2.png", 201, 5, 234, 178), 640, 480)
    image streetz2 = im.Alpha(ImageReference("streetz1"), 0.5)
    show streetz1
    show streetz2 at Position(xpos=50, ypos=0, xanchor=0, yanchor=0)
    with ComposeTransition(Dissolve(0.8), after=ImageDissolve(im.Tile("dissolves/eyeopen.png"), 0.8, 8))
    call show_chat_border
    n "I awaken soon after. My eyes are blurry."
    me "What happened?"

    show streetz2
    with moveinleft
    show mir_at1 smile behind bottom_engraving at mir_at1_center with dissolve
    n "My eyes clear up, and I see someone above me, leaning over me."
    play music "music/212 A Ruthless Blade.ogg" fadeout 1
    mir "Are you okay? Your nose is bleeding."

    $ renpy.sound.set_volume(1.0)
    play sound "sounds/capeflap.ogg"
    show bg street2 behind bottom_engraving
    with dissolve
    hide mir_at1
    show mir_at1 smile behind bottom_engraving at mir_at1_center with dissolve
    n "I leap back."
    me "Mir!"
    n "I notice that she is still wearing no clothes."

    hide mir_at1
    show bg street2 at Zoom((640, 480), (0, 0, 640, 480), (7, 220, 303, 244), 0)
    with dissolve
    n "I look the other way."
    play music "music/208 Heave-Ho Half-Off Sale!.ogg" fadeout 1
    me "Ahahahaha! Yeah! I'm fine! This is just from hitting the pole, that's all! Ahahaha!"
    me "Well then, I-I gotta go! See ya!"

    play music "sounds/running2.ogg" channel 6
    show bg street2 at Zoom((640, 480), (7, 220, 303, 244), (557, 294, 78, 55), 2.0)
    with Pause(2.0)
    n "I run off."
    n "\"Please put some clothes on!\" I shout as I run."

    $ renpy.sound.set_volume(1)
    play music "music/118 Airport Paradise.ogg" fadeout 1
    scene bg house_evening
    with ComposeTransition(Dissolve(0.4), after=ImageDissolve(im.Tile("dissolves/ltorsmooth.png"), 0.4, 8))
    call show_chat_border

    n "I keep running until I go around the block."
    n "I pass a house and stop{nw}"
    stop music channel 6
    extend ", then scoot back{nw}"
    $ renpy.sound.set_volume(0.4)
    play sound "sounds/running2.ogg"
    extend " and crank my head to the right{nw}"
    stop sound fadeout 0.5
    extend "."
    n "There it is... My house."

    play music "music/108 Peaceful Song.ogg" fadeout 1
    show bg street2
    show mir_at1 smile behind bottom_engraving at mir_at1_center
    with ComposeTransition(Dissolve(0.4), after=ImageDissolve(im.Tile("dissolves/ltorsmooth.png"), 0.4, 8, reverse=True))
    n "I look back and see that Mir is still there, just getting up."

    play music "music/118 Airport Paradise.ogg" fadeout 1
    hide mir_at1
    show bg house_evening
    with ComposeTransition(Dissolve(0.4), after=ImageDissolve(im.Tile("dissolves/ltorsmooth.png"), 0.4, 8))
    n "I look at my house,"

    show bg street2
    show mir_at1 smile behind bottom_engraving at mir_at1_center
    with ComposeTransition(Dissolve(0.4), after=ImageDissolve(im.Tile("dissolves/ltorsmooth.png"), 0.4, 8, reverse=True))
    play music "music/108 Peaceful Song.ogg" fadeout 1
    n "then back at Mir."
    n "It turns out... We're practically neighbors."

    #------------

    $ renpy.music.play("music/222 Good night!.ogg", loop=False, fadeout=1)
    scene black
    with fade
    centered "The next day comes. I didn't get any sleep."
    centered "This time, I want to go to school to make up with Aurica as soon as possible."

    play music "music/126 Expressive Girl.ogg"
    scene bg home
    with ComposeTransition(Dissolve(1.0), after=ImageDissolve(im.Tile("dissolves/shear.png"), 1.0, 8, reverse=2))
    call show_chat_border
    show misha_ws smile behind bottom_engraving at truecenter with dissolve
    n "I go and awaken Misha."
    n "She obeys this time. Rare, but it happens."
    $ renpy.sound.set_volume(0.2)
    play sound "sounds/cloth2.ogg"
    show misha_ws happyblush behind bottom_engraving at truecenter
    n "I give her a pat on the head to congratulate her. She seems to like that."

    scene black
    with fade
    $ renpy.music.set_volume(0.5, channel=6)
    play music "sounds/crowd.ogg" channel 6 fadein 1

    play music "music/stream.afs_00093.adx.ogg" fadeout 1
    scene bg classroom
    with squares
    call show_chat_border
    n "I finally arrive at school, where the class seems a little more crowded than usual."
    n "I just can't put my finger on it."
    n "Ah, maybe it's because sensei's here early. That must be it."
    play sound "sounds/woodsqueek.ogg"
    show aurica_sgh notimpressed behind bottom_engraving at auricacenter with dissolve
    n "I take my seat beside Aurica. She's still as silent as yesterday."
    n "I sigh. Maybe she'll calm down during lunch."

    play music "music/122 Filthy Shop Work.ogg" fadeout 1
    hide aurica_sgh
    show jackh smile behind bottom_engraving at truecenter
    with dissolve
    ja "Hey, kid."
    me "I still have a name."
    ja "Yeah yeah. Anyway, have you heard about the rampage of viruses out lately?"
    me "No. What about it?"
    show jackh frown at truecenter with dissolve
    ja "Don't you watch TV? It was all over the place yesterday."
    me "I'm sorry if I have my own problems."
    show jackh shock at truecenter with dissolve
    ja "Well, people are getting hurt nowadays by machines that are infected by viruses."
    show jackh frown at truecenter with dissolve
    ja "Turns out that the lab got blown up and they escaped."
    show jackh smile at truecenter with dissolve
    ja "If I were you, I'd be careful at night."
    me_soft "If you were me you'd be hitting on Aurica right now."
    show jackh frown at truecenter with dissolve
    ja "What was that?"
    me "If you were me, you'd be fitting an ocarina right now."
    show jackh smile at truecenter with dissolve
    ja "Why would I do that?"
    me "No reason."

    play music "music/138 Pulse ~Elemia Arrange~.ogg" fadeout 1
    stop music fadeout 1 channel 6
    $ renpy.sound.set_volume(1)
    play sound "sounds/schoolbell.ogg"
    n "The bell rings. We all snap to attention."
    show shurelia_sg shout at shurcenter behind bottom_engraving
    hide jackh
    with dissolve
    sh "Stand."
    $ renpy.sound.set_volume(0.5, channel=1)
    play sound "sounds/desks.ogg" channel 1
    show shurelia_sg smile1 at shurcenter
    n "We all stand up."
    show shurelia_sg shout at shurcenter
    sh "Bow."
    $ renpy.sound.set_volume(1)
    show shurelia_sg smile1 at shurcenter
    n "We bow."
    hide shurelia_sg
    show leardh smile at truecenter behind bottom_engraving
    with dissolve

    "Class" "Good morning sensei."
    me "I'm hungry. Feed me funbuns."
    hide leardh
    show shurelia_sg shout at shurcenter behind bottom_engraving
    with dissolve
    sh "Sit."

    $ renpy.sound.set_volume(0.5)
    play sound "sounds/desks.ogg"
    $ renpy.music.set_volume(1.0, channel=6)
    $ renpy.music.play(["sounds/wait1.ogg", "sounds/woodsqueek.ogg"], channel=6, loop=False)

    show shurelia_sg smile1 at shurcenter
    n "We all sit."
    hide shurelia_sg
    show leardh smile at truecenter behind bottom_engraving
    with dissolve
    n "Since Leard-sensei is here early, he takes attendance right away."
    $ renpy.sound.set_volume(1)
    le "Troublemaker."
    n "I know who he is referring to."
    me "Here."
    n "I don't care to correct him."
    le "Jack."
    hide leardh
    show jackh smile at truecenter behind bottom_engraving
    with dissolve
    ja "Hey."
    n "{i}\"Man... How am I supposed to apologize to Aurica if she won't listen to me?\"{/i} I think to myself."
    hide jackh
    show leardh smile at truecenter behind bottom_engraving
    with dissolve
    le "Shurelia."
    hide leardh
    show shurelia_sg smile1 at shurcenter behind bottom_engraving
    with dissolve
    sh "Present."
    n "I bang my head on the desk{nw}"
    $ renpy.sound.set_volume(2.0)
    play sound "sounds/woodknock.ogg"
    extend "."
    me "Man. I'm hungry too. Forgot to eat breakfast."
    $ renpy.sound.set_volume(1)
    hide shurelia_sg
    show leardh smile at truecenter behind bottom_engraving
    with dissolve
    le "Mir."
    me "Aaaaargh! This just couldn't get any...... Mir?"
    hide leardh
    show mir_sg faintsmile at truecenter behind bottom_engraving
    with dissolve
    play music "music/212 A Ruthless Blade.ogg" fadeout 1
    mir "Here."
    n "I turn around."
    me "Jakuri!?"
    hide mir_sg
    show leardh smile at truecenter behind bottom_engraving
    with dissolve
    le "Oh, Mir. You came?"
    le" It's nice to finally see you coming."
    hide leardh
    show jackh frown at truecenter behind bottom_engraving
    with dissolve
    ja "Jakuri?"
    show jackh shock at truecenter
    ja "Kid, you know her? Why didn't you tell me she was such a babe?"
    me "Well, you see, this happened and then that happened and she..."
    show jackh shock at jackleft
    show mir_sg frown at chatright behind bottom_engraving
    with moveinright
    mir "Don't call me Jakuri."
    show jackh frown at jackleft
    ja "Huh?"
    show mir_sg smile1 at chatright
    mir "Only he can."
    n "She points to me."
    show jackh smile at jackleft
    ja "Oh... sorry Mir."
    n "This is just weird!"
    n "Why is she at school!?"
    n "Is this really the same Jakuri I saw yesterday!?"
    n "What the hell's going o-..."
    n "Well, at least she's wearing clothes."

    # Reset volume levels, to be polite to the next chapter
    call reset_volumes

    return
