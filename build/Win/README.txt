An Ordinary Life - The Visual Novel (version 0.1)

This is a Ren'Py visual novel based on Danexedas' epic fan fiction,
"An Ordinary Life". You can read it here:

http://oldarm.isisview.org/ar-tonelico-an-ordinary-life-t395.html

This is the game for Chapters 1-5

CREDITS

Story, CG, images, and sfx- Danexedas
Proofreading and original Hymmnos Lyrics - Deciare
Programming - wizzardx
UI Design - Crosse

Sounds, music, and graphics - Ar Tonelico
Misc sounds - freesound.org, random places on the net
