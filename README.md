# README #

This text file lists various HOWTOs **wizzardx** put together while working on the VN. Hopefully they will be useful to anyone else who wants to work on this project.

### Foreword from Crosse ###

Welcome to the unofficial repository of the game **Ar Tonelico: An Ordinary Life**. This visual novel is based on a fanfic created by Dexas and created by WizzardX and posted on the old Ar Tonelico A Reyvateil's Melody forums (the one hosted in isisview.org). With the original forums down for some time, the information, game installers and source files have disappeared from the face of the internet.

Luckily, I found the source files, installer, and a build for Windows users. I now have re-uploaded them here in Github for everyone's access and enjoyment.

Check out the original fanfic in this forum thread. [Ar tonelico: An Ordinary Life](https://www.tapatalk.com/groups/revatail_hymmne/ar-tonelico-an-ordinary-life-t506.html).

Whether I can get to actually finish the game is something yet to be seen, but for now, enjoy the game!

-Crosse A.

### Installing the Game ###

To install the game, go to Releases and download the latest installer. Or, you can simply download the entire build folder for Windows at **build > Win**

## For Contributors ##

Make sure to clone the repository and check-in your work in your own branch before doing a merge request. The tips and content outlined below have been compiled by WizzardX and mostly written from his point of view. You can also find a copy of this in the HACKING.txt file found under the /dev/ folder in either Build or src directories.

### Making a new chapter ###

* Copy the previous chapter's folder over to a folder for the new chapter.

    - eg: Copy AnOrdinaryLifeCh2 to AnOrdinaryLifeCh3

* Remove .git version-tracking data, from the original chapter, if present:

    - eg: 

    ```mv ./.git /tmp/old_aol_git_data
    # The above is a bit safer than the below, in case I remove the wrong
    # chaper's git metadata:
    # mv ./.git rm ./.git -rfv
    ```

* Update README.txt:

    - Version number should go back to 0.1 (this is a new project)
    - Update the chapter number in the "This is the game for Chapter X" line
    - Make sure the Credits section is up to date
    - Remove RELEASE HISTORY details (to be filled in later)

* launcherinfo.py updates

    - Change distribution_base to "AnOrdinaryLifeChX" (where X is the chapter
      number)

* Go into the "game" sub-directory.

* Rename the previous chapter's chapter .rpy, eg:

    chapter2.rpy -> chapter3.rpy

* Update options.rpy:

    - Change config.window_title to "An Ordinary Life - Chapter X" (where X is
      the chapter number)
    - Change config.save_directory to """AnOrdinaryLifeChX-1236880333" (where X
      is the chapter number)

* script.rpy updates

    - Change the comment to something like "Call the 1st chapter"
    - Update the "call" line:
        - Remove the "from" bit, if present
        - Change the chapter name (eg: chapter2 -> chapter3)

    - Add a "to be continued..." line (might be changed later, depending on how
      the chapter ends), eg:

        ```
            # Fade out (music and display) and show "to be continued" message
            stop music fadeout 1
            scene black
            with fade
            centered "To be continued..."
            
        ```
* Remove all media (images, sound, music, etc) unrelated to the new chapter
  (we copy files from the An Ordinary Life media folder when needed).

Here is the current list of required media for a skeleton "An Ordinary Life"
chapter:

    backgrounds/title.png
    backgrounds/splash2.png
    backgrounds/splash1.png
    backgrounds/title_buttons.png
    backgrounds/title_buttons_selected.png
    backgrounds/splash3.png
    backgrounds/street2.png
    misc/chat_border.png
    misc/screen_bottom_engraving.png
    music/06 - EXEC_PHANTASMAGORIA.ogg
    sounds/menu1.ogg
    sounds/menu2.ogg

* Update the chapterX.rpy file:

    - Remove all things unrelated to the new chapter from the init section. We can borrow init stuff from previuos chapters as needed.

    - Change the number on the chapterX label appropriately

    - Remove everything between the start and end calls to "reset_volumes"

* Fire up the renpy launcher and test the new empty chapter

* Tidy up files in the main game dir

Remove *.rpyc, *.bak, and *.rpyb, eg:

    rm -v *.rpyc *.bak *.rpyb

* Add the new chapter to git revision control, eg:

    cd .. # Go to base chapter directory
    git init
    git add .
    git commit -m 'Initial commit for Chapter X'
    
    
### Work Flow ###

This section describes the basic workflow wizzardx uses for making a chapter.

1) Make a new skeleton chapter (see "Making a new chapter" guide)

2) Copy & paste the chapter from the forums into the chapter, and comment out
   the lines with # (my text editor makes this easy)

3) Convert the chapter into basic Ren'Py syntax (uncommenting as I go along)

4) Add font effects (colors, italics, etc) to the Ren'Py script.

5) Chat-box splitting & proof-reading

At this step, I start breaking the chapter into "chat boxes" where they read
more smoothly. At the same time, I proofread and fix typos, grammatical
problems, etc. See the "Proofreading" guide.

5) Add Ren'Py "characters" for the chapter

So that the names can get the correct colors, instead of being white.
See the "New character labels" guide.

6) Add backgrounds to the chapter (see "New background CG" guide)

7) Add music to the chapter (see "New music" guide)

8) Add sound effects to the chapter (see "New sound effects" guide)

If you change sound/music channel volumes during a chapter, remember to reset
them back to their original levels at the end of the chapter.

9) Add character graphics to the chapter.

- With appropriate expressions, positions, transitions, animations, etc.
- Animations also inspired by Princess Waltz :-)

10) Add Ren'Py special effects (flashes, dissolves, etc) to the chapter
- Play princess Waltz for an hour or so, to get ideas
- Nice flashy scene changes (inspired by Princess Waltz)
- Make scene changes fade to black for longer if some time has elapsed
- And various other things that will look cool

11) Add in any secret bonus stuffs

12) Check any remaining TODOs (in main AnOrdinaryLife TODO file, or in
    a chapterX.rpy comment)

13) Re-read HACKING.txt, to check if I've missed anything

14) Test, test, test

The config.debug and config.debug_sound options can be helpful here to catch
problems with missing media.

15) Forward to Dexas.

16) When Dexas replies (usually with a long list), fix them all, and forward
    the fixed version to him.

Usually the fixed version is fine with him, and that's what gets used in the end.

### Developer Options ###

Developer options.

For developing, you can change a few options in options.rpy:

 - set config.developer to True
 - set config.debug to True
 - set config.debug_sound to True

Set them back to False before releasing the game

### Developer Tips ###

While developing a section, use Shift+R to reload the script and jump to the
current position in the game.

To quickly skip to a section of the script, add a line like this just before
the area of interest:

    $ renpy.input('')

And then press ">" (with developer mode enabled), to skip to the $ renpy.input
line.

See this page for further information:

    http://www.renpy.org/wiki/renpy/doc/reference/Developer_Tools

### New character labels: ###

* Grab their hair and eye colors.
  - If their hair is covered, use their main clothing color
* Use the brighter color for the font, and the darker color for the outling

### New media ###

All new media (backgrounds, sounds, music, etc) should be added to a separate
project (AnOrdinaryLifeMedia). This helps me keep track of what has already
been used in previous chapters.

### New background CG ###

* Resize to 640 x 480, with smooth scaling

### New music ###

* Convert to ogg (I use Audacity -wizzardx)

* Normalise:

    vorbisgain music.ogg

    - Note: Vorbisgain doesn't seem to work with newer versions of Ren'Py, so
            we need to add some gain (preferably before converting to ogg).
    - I made this tool which can help: ~/dev/misc/normalize_ogg.py -wizzardx

### New Sound Effects ###

* Convert to ogg (I use Audacity)

* Normalise the OGG, eg:

    vorbisgain sound.ogg

    - Note: Vorbisgain doesn't seem to work with newer versions of Ren'Py, so
            we need to add some gain (preferably before converting to ogg).
    - I made this tool which can help: ~/dev/misc/normalize_ogg.py -wizzardx

### New character art ###

* Make sure that they look correct when animated, or shown at the far center,
far right, and far left.

A good way to do this is to show a few frames at the start of the chapter for
comparison

### Proofreading ###

Sometimes the fic has mixed tenses. Usually Dexas meant to use present tense,
so correct to present tense when in doubt.

Tip: Try reading the chapters out loud when proofreading. It's easier to catch mistakes that way, than when skimming over the text with just your eyes. And, it's more fun doing it this way :-)

## Dexas' Tips ##

A few of the many tips given to me by Dexas. Put here to help me remember.

* Music volume should softer than sound effects, to keep background music in
  the background. Save louder music for dramatic effect.

* Show black for longer during fades when 30 seconds or more elapse between
  scenes, to help portray the passage of time.

You can do this with a "scene black with fade" line just before the scene
change (or any music for the new scene).

* Don't show large character pics on the screen at the same time.

It makes it look like characters are standing close to each other. Similarly,
don't show characters unnecessarily. eg: Leard the entire time he is doing
rollcall, while main is talking to Aurica.

Also, it's probably better to completely switch characters onscreen, instead
of sliding in and out, if the characters aren't actually standing close to each
other in the story (eg: Leard and pupils doing rollcall).

* Don't use weird too-weird sound effects, eg:
  - Demonic-sounding cat meows for Main
  - Fatal-sounding bone-breaking sounds at bad times

* Split more sentences into separate chat boxes. Don't try to fit entire
  speeches into a single chat box.
  
## Releasing a new version of the game: ##

* Clean the project

 git clean -fxd

* Build the project for windows32

* Copy the zip file to a temporary directory, and unzip it

* Go into the temporary directory, and run this script to upload the files and
  make a zip remotely:

  dev/upload_updates.sh

* Test the updater, and the game, in a Windows XP virtual machine

  - Make sure that regular players won't be disturbed by stuff under
    development, unless they choose those chapters from the chapter menu.

* Players (or testers/fellow devs) can use the regular game updater to get new versions.

### Who do I talk to? ###

* Repo owner or admin - Crosse
* Other community or team contact